use core::alloc::{GlobalAlloc, Layout};
use core::intrinsics::volatile_set_memory;

use x86_64::instructions::interrupts::without_interrupts;
use x86_64::structures::paging::{FrameAllocator, Mapper, Page, PageSize, PageTableFlags, Size4KiB};
use x86_64::VirtAddr;

use crate::memory::{find_free_page, find_free_pages_to_fit, FRAME_ALLOCATOR, PAGE_TABLE};

use super::Locked;

// todo
// https://os.phil-opp.com/allocator-designs/#creating-new-blocks
// https://os.phil-opp.com/allocator-designs/#discussion-2
// could be upgraded with point of Creating new Blocks section

const BLOCK_SIZES: &[usize] = &[8, 16, 32, 64, 128, 256, 512, 1024, 2048];

struct ListNode {
    next: Option<&'static mut ListNode>,
}

pub struct FixedSizeBlockPagingAllocator {
    list_heads: [Option<&'static mut ListNode>; BLOCK_SIZES.len()],
}

// TODO return null pointer instead of crashing when alloc failure
impl FixedSizeBlockPagingAllocator {
    pub const fn new() -> Self {
        const EMPTY: Option<&'static mut ListNode> = None;
        Self {
            list_heads: [EMPTY; BLOCK_SIZES.len()],
        }
    }

    /// SAFETY: do not drop owned return value, instead use deallocate function
    /// this function could in the future return null pointer on alloc failure
    pub unsafe fn fallback_alloc(&mut self, layout: Layout, flags: PageTableFlags) -> *mut u8 {
        // TODO make this support align of values > Size4KiB::SIZE
        debug_assert!(layout.align() <= Size4KiB::SIZE as usize, "allocation with align > 4KiB not supported");
        // let size = layout.size().max(layout.align());
        let size = layout.size();

        let pages = find_free_pages_to_fit(size as u64);

        without_interrupts(|| {
            let mut mapper_lock = PAGE_TABLE.lock();
            let mapper = mapper_lock.as_mut().unwrap();
            let mut frame_alloc_lock = FRAME_ALLOCATOR.lock();
            let frame_allocator = frame_alloc_lock.as_mut().unwrap();

            for page in pages {
                let frame = frame_allocator.allocate_frame().expect("out of memory error");
                unsafe { mapper.map_to(page, frame, flags, frame_allocator) }.expect("mapping error").flush();
            }
            drop(mapper_lock);
            drop(frame_alloc_lock);
        });

        pages.start.start_address().as_mut_ptr()
    }

    /// SAFETY: do not drop owned return value, instead use deallocate function
    /// this function could in the future return null pointer on alloc failure
    pub unsafe fn fallback_alloc_zeroed_volatile(&mut self, layout: Layout, flags: PageTableFlags) -> *mut u8 {
        unsafe {
            let ptr = self.fallback_alloc(layout, flags);
            volatile_set_memory(ptr, 0, layout.size());
            ptr
        }
    }

    /// size must be one of the sizes from BLOCK_SIZES
    /// returns one address with size of wanter block and places rest that can fit in the page in
    /// list_heads
    fn fallback_alloc_block_size(&mut self, block_index: usize, flags: PageTableFlags) -> *mut u8 {
        let size = BLOCK_SIZES[block_index];
        let page = find_free_page();
        without_interrupts(|| {
            let mut mapper_lock = PAGE_TABLE.lock();
            let mapper = mapper_lock.as_mut().unwrap();
            let mut frame_alloc_lock = FRAME_ALLOCATOR.lock();
            let frame_allocator = frame_alloc_lock.as_mut().unwrap();
            let frame = frame_allocator.allocate_frame().expect("couldn't get frame from frame allocator - probably out of memory");
            unsafe { mapper.map_to(page, frame, flags, frame_allocator).expect("mapping error").flush() };
        });

        let amount_of_addresses = page.size() as usize / size;
        for i in 1..amount_of_addresses {
            let ptr_offset = size * i;
            let ptr = (page.start_address().as_u64() as usize + ptr_offset) as *const u8 as *mut u8;
            self.list_prepend(block_index, ptr);
        }

        page.start_address().as_mut_ptr()
    }

    // TODO maybe drop &mut self
    /// should only be called for memory allocated with fallback_alloc (not
    /// fallback_alloc_block_size)
    /// assumess that it is using Page<Size4KiB>
    pub unsafe fn deallocate(&mut self, ptr: *mut u8, layout: Layout) {
        let first_page: Page<Size4KiB> = {
            let virt_addr = VirtAddr::new(ptr as u64);
            Page::containing_address(virt_addr)
        };
        let last_page: Page<Size4KiB> = {
            let virt_addr = VirtAddr::new((ptr as usize + layout.size() - 1) as u64);
            Page::containing_address(virt_addr)
        };

        let range = Page::range_inclusive(first_page, last_page);

        let mut mapper_lock = PAGE_TABLE.lock();
        let mapper = mapper_lock.as_mut().unwrap();
        for page in range {
            // TODO reclaim this frame instead of leaking it
            mapper.unmap(page).expect("couldn't unmap a page").1.flush();
        }
        drop(mapper_lock);
    }

    fn list_prepend(&mut self, size_index: usize, ptr: *mut u8) {
        let new_node = ListNode {
            next: self.list_heads[size_index].take(),
        };

        let new_node_ptr = ptr as *mut ListNode;
        unsafe {
            new_node_ptr.write(new_node);
            self.list_heads[size_index] = Some(&mut *new_node_ptr);
        }
    }
}

fn list_index(layout: &Layout) -> Option<usize> {
    let required_block_size = layout.size().max(layout.align());
    BLOCK_SIZES.iter().position(|&s| s >= required_block_size)
}

unsafe impl GlobalAlloc for Locked<FixedSizeBlockPagingAllocator> {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE;
        without_interrupts(|| {
            let mut allocator = self.lock();
            match list_index(&layout) {
                Some(index) => match allocator.list_heads[index].take() {
                    Some(node) => {
                        allocator.list_heads[index] = node.next.take();
                        node as *mut ListNode as *mut u8
                    }
                    None => {
                        allocator.fallback_alloc_block_size(index, flags)
                    }
                },
                None => unsafe { allocator.fallback_alloc(layout, flags) },
            }
        })
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        without_interrupts(|| {
            let mut alloc = self.lock();
            match list_index(&layout) {
                Some(index) => {
                    // let new_node = ListNode {
                    //     next: alloc.list_heads[index].take(),
                    // };
                    //
                    // let new_node_ptr = ptr as *mut ListNode;
                    // unsafe {
                    //     new_node_ptr.write(new_node);
                    //     alloc.list_heads[index] = Some(&mut *new_node_ptr);
                    // }
                    alloc.list_prepend(index, ptr);
                }
                None => {
                    unsafe { alloc.deallocate(ptr, layout) }
                }
            }
        });
    }
}
