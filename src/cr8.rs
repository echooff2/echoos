use core::arch::asm;

#[inline]
pub unsafe fn write(value: u64) {
    unsafe {
        asm!("mov cr8, {}", in(reg) value, options(nostack, preserves_flags));
    }
}

#[inline]
pub fn read() -> u64 {
    let value: u64;

    unsafe {
        asm!("mov {}, cr8", out(reg) value, options(nomem, nostack, preserves_flags));
    }

    value
}
