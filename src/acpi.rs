use core::ptr::NonNull;

use acpi::{AcpiHandler, AcpiTables, HpetInfo, PhysicalMapping};
use spin::RwLock;
use x86_64::VirtAddr;

pub static ACPI_TABLE: RwLock<Option<AcpiTables<ACPI>>> = RwLock::new(None);

pub fn init(phys_mem_offset: VirtAddr, rsdp_addr: u64) {
    let acpi = ACPI {
        physical_to_virtual_mem_offset: phys_mem_offset.as_u64(),
    };

    let acpi_tables = unsafe { AcpiTables::from_rsdp(acpi, rsdp_addr as usize) }
        .expect("couldn't get acpi tables from rsdp");

    *ACPI_TABLE.write() = Some(acpi_tables);
}

/// None if acpi not initialized or hpet not supported
pub fn get_hpet_table() -> Option<HpetInfo> {
    let acpi_lock = ACPI_TABLE.read();
    let acpi = acpi_lock.as_ref()?;
    let hpet_info = HpetInfo::new(acpi).ok()?;
    Some(hpet_info)
}

#[derive(Debug, Clone)]
pub struct ACPI {
    physical_to_virtual_mem_offset: u64,
}

impl AcpiHandler for ACPI {
    unsafe fn map_physical_region<T>(
        &self,
        physical_address: usize,
        size: usize,
    ) -> acpi::PhysicalMapping<Self, T> {
        unsafe {
            PhysicalMapping::new(
                physical_address,
                NonNull::new_unchecked(
                    (physical_address + self.physical_to_virtual_mem_offset as usize) as *mut T,
                ),
                size,
                size,
                Self {
                    physical_to_virtual_mem_offset: self.physical_to_virtual_mem_offset,
                },
            )
        }
    }

    fn unmap_physical_region<T>(_region: &acpi::PhysicalMapping<Self, T>) {}
}

// impl AcpiTable for ACPI {
//     fn header(&self) -> &acpi::sdt::SdtHeader {

//     }
// }
