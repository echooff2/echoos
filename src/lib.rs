#![no_std]
#![no_main]
#![feature(
    custom_test_frameworks,
    alloc_error_handler,
    abi_x86_interrupt,
    asm_const,
    sync_unsafe_cell,
    error_in_core, // To be stablelized in rust 1.81
    core_intrinsics,
)]
#![test_runner(crate::test_runner)]
#![reexport_test_harness_main = "test_main"]
#![feature(const_mut_refs)]
#![deny(unsafe_op_in_unsafe_fn)]
#![allow(clippy::missing_safety_doc)]

extern crate alloc;

#[cfg(test)]
use bootloader::{entry_point, BootInfo};
use x86_64::VirtAddr;

use core::{alloc::Layout, panic::PanicInfo};

pub mod acpi;
pub mod allocator;
pub mod apic;
pub mod cr8;
pub mod display;
pub mod gdt;
pub mod hpet;
pub mod interrupts;
pub mod keyboard;
pub mod memory;
pub mod pci;
pub mod serial;
pub mod xhci;
pub mod rand;

/// value = 0 before initialization, so only makes sense to use after
pub static PHYS_MEM_OFFSET: &VirtAddr = unsafe { &PHYS_MEM_OFFSET_MUT };
static mut PHYS_MEM_OFFSET_MUT: VirtAddr = VirtAddr::zero();

pub fn init(phys_mem_offset: VirtAddr) {
    unsafe { PHYS_MEM_OFFSET_MUT = phys_mem_offset };
    gdt::init();
    interrupts::init_idt();
}

pub fn hlt_loop() -> ! {
    loop {
        x86_64::instructions::hlt()
    }
}

/// SAFETY: num can only have bit0 set
pub unsafe fn bool_from_u8(num: u8) -> bool {
    unsafe { core::mem::transmute::<u8, bool>(num) }
}

/// SAFETY: num can only have bit0 set
pub unsafe fn bool_from_u16(num: u16) -> bool {
    unsafe { bool_from_u8(num as u8) }
}

/// SAFETY: num can only have bit0 set
pub unsafe fn bool_from_u32(num: u32) -> bool {
    unsafe { bool_from_u8(num as u8) }
}

pub trait Testable {
    fn run(&self);
}

impl<T> Testable for T
where
    T: Fn(),
{
    fn run(&self) {
        serial_print!("{}...\t", core::any::type_name::<T>());
        self();
        serial_println!("[ok]");
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u32)]
pub enum QemuExitCode {
    Success = 0x10,
    Failed = 0x11,
}

#[allow(dead_code)]
pub fn exit_qemu(exit_code: QemuExitCode) -> ! {
    use x86_64::instructions::port::Port;

    let mut port = Port::new(0xf4);
    unsafe { port.write(exit_code as u32) };

    unreachable!()
}

pub fn test_runner(tests: &[&dyn Testable]) -> ! {
    serial_println!("Running {} tests", tests.len());

    for test in tests {
        test.run();
    }

    exit_qemu(QemuExitCode::Success)
}

pub fn test_panic_handler(info: &PanicInfo) -> ! {
    serial_println!("[failed]\n");
    serial_println!("Error: {}\n", info);

    exit_qemu(QemuExitCode::Failed)
}

#[cfg(test)]
entry_point!(test_kernel_main);

#[cfg(test)]
fn test_kernel_main(_boot_info: &mut BootInfo) -> ! {
    test_main();

    unreachable!()
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    test_panic_handler(info)
}

#[alloc_error_handler]
fn alloc_error_handler(layout: Layout) -> ! {
    panic!("allocation error: {:?}", layout);
}
