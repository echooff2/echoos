use alloc::vec::Vec;

// ehci manual: https://www.intel.com/content/www/us/en/products/docs/io/universal-serial-bus/ehci-specification-for-usb.html
#[derive(Debug, Clone)]
pub struct EhciCapability {
    pub cap_length: u8,
    pub hci_version: u16,
    pub hcs_params: u32,
    pub hcc_params: u32,
    // real size: 60 bits
    pub hcsp_portroute: u64,
}

#[derive(Debug, Clone)]
pub struct EhciOps {
    pub usb_cmd: u32,
    pub usb_sts: u32,
    pub usb_intr: u32,
    pub fr_index: u32,
    pub ctrls_segment: u32,
    pub periodic_list_base: u32,
    pub async_list_addr: u32,
    pub config_flag: u32,
    pub port_sc: Vec<u32>,
}
