use core::ops::{Deref, DerefMut};

use super::{TRB, TRB_LINK_TYPE};

#[derive(Debug, Copy, Clone)]
#[repr(transparent)]
pub struct LinkTRB(TRB);

impl LinkTRB {
    /// constructs new link trb and sets trb type to 6 (link trb)
    pub fn new() -> Self {
        let mut link_trb = LinkTRB(TRB::new());
        link_trb.set_trb_type(TRB_LINK_TYPE);
        link_trb
    }

    // has to be 16 byte aligned address to another trb ring
    pub fn set_ring_segment_ptr(&mut self, new_val: u64) {
        self.0.lower_u64 = new_val & (!0b1111)
    }

    pub fn ring_segment_ptr(&self) -> u64 {
        self.0.lower_u64
    }

    /// returns 1 bit value
    pub fn toggle_cycle(&self) -> u8 {
        (self.0.upper_u64 >> 33) as u8 & 1
    }

    pub fn set_toggle_cycle(&mut self, new_val: u8) {
        const CYCLE_BIT_MASK: u64 = !(1 << 33);
        debug_assert!(new_val < 2, "new_val must only be between 0 and 1");
        let read = self.lower_u64 & CYCLE_BIT_MASK;
        let new_val = (new_val as u64) << 33;
        self.lower_u64 = read | new_val;
    }
}

impl Into<TRB> for LinkTRB {
    fn into(self) -> TRB {
        self.0
    }
}

impl Deref for LinkTRB {
    type Target = TRB;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for LinkTRB {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
