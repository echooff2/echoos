use core::ops::{Deref, DerefMut};

use super::{TRB, TRB_COMMAND_NOOP_TYPE};

#[repr(transparent)]
#[derive(Debug,Copy, Clone)]
pub struct CommandTRB(TRB);

impl CommandTRB {
    fn new() -> Self {
        CommandTRB(TRB::new())
    }
}

impl Into<TRB> for CommandTRB {
    fn into(self) -> TRB {
        self.0
    }
}

impl Deref for CommandTRB {
    type Target = TRB;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for CommandTRB {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

#[repr(transparent)]
#[derive(Debug,Copy, Clone)]
pub struct NoOpCommandTRB(CommandTRB);

impl NoOpCommandTRB {
    pub fn new() -> Self {
        let mut command_trb = CommandTRB::new();
        command_trb.set_trb_type(TRB_COMMAND_NOOP_TYPE);
        NoOpCommandTRB(command_trb)
    }
}

impl Into<CommandTRB> for NoOpCommandTRB {
    fn into(self) -> CommandTRB {
        self.0
    }
}

impl Deref for NoOpCommandTRB {
    type Target = CommandTRB;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for NoOpCommandTRB {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
