use core::ptr::{read_volatile, write_volatile};

use x86_64::VirtAddr;

use crate::{memory::translate_addr_blocking, println};

pub mod command_trb;
pub mod link_trb;

pub const TRB_LINK_TYPE: u8 = 6;
pub const TRB_COMMAND_NOOP_TYPE: u8 = 23;

#[derive(Debug, Copy, Clone)]
pub struct TRB {
    /// has status, control, trb_type, flags
    upper_u64: u64,
    /// has parameter
    lower_u64: u64,
}

impl TRB {
    /// size in bytes
    pub const SIZE: u8 = 16;
    pub fn new() -> Self {
        Self {
            upper_u64: 0,
            lower_u64: 0,
        }
    }

    pub fn read(trb_ptr: *const u64) -> Self {
        unsafe {
            let lower_u64 = read_volatile(trb_ptr);
            let upper_u64 = read_volatile(trb_ptr.add(1));
            Self { upper_u64, lower_u64 }
        }
    }

    pub fn write(self, trb_ptr: *mut u64) {
        println!("trb write, ptr: {:?}", trb_ptr);
        println!("phys_ptr: {:?}", translate_addr_blocking(VirtAddr::from_ptr(trb_ptr)));
        unsafe { 
            let read1 = read_volatile(trb_ptr);
            let read2 = read_volatile(trb_ptr.add(1));
            println!("read1: {:#X}, read2: {:#X}", read1, read2);
        }
        unsafe {
            // order of write is important
            write_volatile(trb_ptr, self.lower_u64);
            write_volatile(trb_ptr.add(1), self.upper_u64);
        }
        // TMP - debug
        //
        unsafe { 
            let read1 = read_volatile(trb_ptr);
            let read2 = read_volatile(trb_ptr.add(1));
            println!("read1: {:#X}, read2: {:#X}", read1, read2);
        }
    }

    pub fn trb_type(&self) -> u8 {
        // ((self.lower_u64 >> 10) & 0x111111) as u8
        (self.upper_u64 >> 42) as u8 & 0b111111
    }

    pub fn set_trb_type(&mut self, new_val: u8) {
        const TRB_TYPE_MASK: u64 = !(0b111111 << 42);
        debug_assert!(new_val < 2_u8.pow(6), "new_val must only be 6 bits");
        let read = self.upper_u64 & TRB_TYPE_MASK;
        let new_val_shifted = (new_val as u64) << 42;
        self.upper_u64 = read | new_val_shifted;
    }

    pub fn cycle_bit(&self) -> u8 {
        (self.upper_u64 >> 32) as u8 & 1
    }

    pub fn set_cycle_bit(&mut self, new_val: u8) {
        const CYCLE_BIT_MASK: u64 = !(1 << 32);
        debug_assert!(new_val < 2, "new_val must only be between 0 and 1");
        let read = self.upper_u64 & CYCLE_BIT_MASK;
        let new_val = (new_val as u64) << 32;
        self.upper_u64 = read | new_val;
    }
}

