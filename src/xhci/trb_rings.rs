use core::{alloc::Layout, intrinsics::volatile_set_memory, marker::PhantomData, mem::{self, size_of}, ops::{Deref, DerefMut}, ptr::{read_volatile, write_volatile}};

use alloc::slice;
use x86_64::{structures::paging::{PageSize, PageTableFlags, Size4KiB, Translate}, VirtAddr};

use crate::{allocator::ALLOCATOR, memory::PAGE_TABLE, println, serial_println};

use super::trb::{command_trb::CommandTRB, link_trb::LinkTRB, TRB, TRB_LINK_TYPE};

#[derive(Debug)]
pub struct CommandRing {
    crcr_ptr: *mut u64,
    /// either 0 or 1
    pcs: u8,
    enqueue_ptr: EnqueuePtr,
}

impl CommandRing {
    pub fn new(crcr_ptr: *mut u64) -> Self {
        let ring = create_ring();
        let ring_virt_addr = VirtAddr::new(ring);
        let ring_phys_addr = {
            let lock = PAGE_TABLE.lock();
            let page_table = lock.as_ref().unwrap();
            page_table.translate_addr(ring_virt_addr)
        }.expect("just created ring should have phys addr");
        println!("ring created: {:#X}", ring);
        let enqueue_ptr = EnqueuePtr {
            trb_start_addr: ring,
            trb: TRB::new(),
        };

        unsafe { write_volatile(crcr_ptr, ring_phys_addr.as_u64() & (!0b111111)) };

        Self {
            crcr_ptr,
            pcs: 1,
            enqueue_ptr,
        }
    }

    pub fn crcr_ptr(&self) -> *const u64 {
        self.crcr_ptr
    }

    pub fn enqueue_trb(&mut self, trb: impl Into<CommandTRB>) -> Option<()> {
        if self.is_full() {
            return None;
        }
            
        self.enqueue_ptr.set_trb(trb.into().into());
        self.adnvance_enqueue_ptr();

        Some(())
    }

    pub fn is_full(&mut self) -> bool {
        // TODO when EventRing is implemented actually check if ring is full
        serial_println!("WARNING: is full returned false without actually checking whether it is full!!");
        println!("WARNING: is full returned false without actually checking whether it is full!!");
        false
    }

    /// advances and write trb
    /// after this it is guaranteed that enqueue_ptr trb is not link trb
    fn adnvance_enqueue_ptr(&mut self) {
        println!("adnvance_enqueue_ptr");
        self.enqueue_ptr.trb.set_cycle_bit(self.pcs);
        self.enqueue_ptr.flush();
        self.enqueue_ptr.trb_start_addr += 16;
        self.enqueue_ptr.read();

        let current_trb = self.enqueue_ptr.trb;
        if current_trb.trb_type() == TRB_LINK_TYPE {
            let current_trb = unsafe { mem::transmute::<TRB, LinkTRB>(current_trb) };
            if current_trb.toggle_cycle() == 1 {
                self.pcs = (!self.pcs) & 1;
            }

            self.enqueue_ptr.trb_start_addr = current_trb.ring_segment_ptr();
            self.enqueue_ptr.read();
        }
    }
}

#[derive(Debug)]
pub struct EventRing {
    erst_size: u16,
    erst_base: ERST,
    /// 1 bit field
    ccs: u8,
    pub dequeue_ptr: DequeuePtr,
}

impl EventRing {
    const ERSTSZ: u16 = 1;

    pub fn new() -> Self {
        let erst = ERST::new(Self::ERSTSZ);
        let dequeue_ptr = DequeuePtr {
            trb_start_addr: erst.inner[0].ring_segment_addr,
            trb: TRB::new(),
            current_erst_idx: 0,
            current_segment_idx: 0,
        };

        Self {
            erst_size: Self::ERSTSZ,
            erst_base: erst,
            ccs: 1,
            dequeue_ptr,
        }
    }

    /// will not advance if the next trb is empty
    pub fn advance_dequeue_ptr(&mut self) {
        self.dequeue_ptr.read();

        if self.is_empty_cached() {
            return;
        }
        
        let segment_size = self.erst_base[self.dequeue_ptr.current_erst_idx].size;
        if self.dequeue_ptr.current_segment_idx >= segment_size as usize {
            // go to next segment
            if self.dequeue_ptr.current_erst_idx >= self.erst_size as usize {
                // go back to first erst entry
                self.dequeue_ptr.current_erst_idx = 0;
            }
            else {
                // go to next erst entry
                self.dequeue_ptr.current_erst_idx += 1;
            }
            self.dequeue_ptr.current_segment_idx = 0;
            let next_addr = self.erst_base[self.dequeue_ptr.current_erst_idx].ring_segment_addr;
            self.dequeue_ptr.trb_start_addr = next_addr;
        }
        else {
            // go to next trb in the segment
            self.dequeue_ptr.current_segment_idx += 1;
            // because memory is continous we can just advance pointer by 16 bytes - TRB_SIZE
            self.dequeue_ptr.trb_start_addr += TRB::SIZE as u64;
        }

        self.dequeue_ptr.read();
    }

    /// works on cached trb, to check if currently it is empty use self.dequeue_ptr.read()
    pub fn is_empty_cached(&self) -> bool {
        self.dequeue_ptr.trb.cycle_bit() != self.ccs
    }

    pub fn erst_size(&self) -> u16 {
        self.erst_size
    }

    pub fn get_erdp(&self) -> u64 {
        self.dequeue_ptr.get_erdp()
    }

    pub fn erst_phys_addr(&self) -> u64 {
        let virt_addr = VirtAddr::from_ptr(self.erst_base.inner.as_ptr());
        let lock = PAGE_TABLE.lock();
        let page_table = lock.as_ref().unwrap();
        
        page_table.translate_addr(virt_addr).unwrap().as_u64()
    }
}

#[derive(Debug)]
struct ERST {
    inner: &'static [ERSTEntry],
}

impl ERST {
    fn new(size: u16) -> Self {
        let erst_slice = {
            let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE | PageTableFlags::NO_CACHE;
            let size_bytes = size_of::<ERSTEntry>() * size as usize;
            let layout = Layout::from_size_align(size_bytes, Size4KiB::SIZE as usize).unwrap();
            let mut alloc = ALLOCATOR.lock();
            let ptr = unsafe { alloc.fallback_alloc(layout, flags) };
            drop(alloc);
            unsafe { slice::from_raw_parts_mut::<ERSTEntry>(ptr as *mut ERSTEntry, size as usize) }
        };

        for i in 0..erst_slice.len() {
            let entry = ERSTEntry::new_page_sized();
            erst_slice[i] = entry;
        }

        Self {
            inner: erst_slice,
        }
    }
}

impl Deref for ERST {
    type Target = [ERSTEntry];

    fn deref(&self) -> &Self::Target {
        self.inner
    }
}

#[repr(packed)]
#[derive(Debug)]
struct ERSTEntry {
    ring_segment_addr: u64,
    /// The Ring Segment Size may be set to any value from 16 to 4096, however
    /// software shall allocate a buffer for the Event Ring Segment that rounds up its
    /// size to the nearest 64B boundary
    size: u16,
    reserved1: u16,
    reserved2: u32,
}

const _: () = {
    if size_of::<ERSTEntry>() != 16 {
        panic!("sizeof ERSTEntry != 16");
    }
};

impl ERSTEntry {
    const ERST_ENTRY_SIZE: u16 = 16;
    fn new_page_sized() -> ERSTEntry {
        let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE | PageTableFlags::NO_CACHE;
        let layout = Layout::from_size_align(Size4KiB::SIZE as usize, Size4KiB::SIZE as usize).unwrap();
        let ptr = {
            let mut alloc_lock = ALLOCATOR.lock();
            unsafe { alloc_lock.fallback_alloc_zeroed_volatile(layout, flags) }
        };

        Self {
            ring_segment_addr: ptr as u64,
            size: Size4KiB::SIZE as u16 / ERSTEntry::ERST_ENTRY_SIZE,
            reserved1: 0,
            reserved2: 0,
        }
    }
}

#[derive(Debug)]
pub struct EnqueuePtr {
    trb_start_addr: u64,
    trb: TRB,
}

impl EnqueuePtr {
    /// will not write to trb_start_addr. To do that use flush
    fn set_trb(&mut self, trb: TRB) {
        self.trb = trb;
    }

    /// writes set trb to the pointer
    fn flush(&mut self) {
        self.trb.write(self.trb_start_addr as *mut u64);
    }

    /// read trb from trb_start_addr and sets it to self.trb
    fn read(&mut self) {
        let trb = TRB::read(self.trb_start_addr as *const u64);
        self.trb = trb
    }
}

#[derive(Debug)]
pub struct DequeuePtr {
    trb_start_addr: u64,
    trb: TRB,
    current_erst_idx: usize,
    current_segment_idx: usize,
}

impl DequeuePtr {
    /// gets erdp with physical address of dequeue ptr
    pub fn get_erdp(&self) -> u64 {
        let virt_addr = VirtAddr::new(self.trb_start_addr);
        let lock = PAGE_TABLE.lock();
        let page_table = lock.as_ref().unwrap();
        
        let dequeue_ptr_phys_addr = page_table.translate_addr(virt_addr).unwrap().as_u64();
        drop(lock);

        let desi = self.current_erst_idx as u64 & 0b111;
        // zero does nothing, write one to clear. TODO determine what I should be with it
        let ehb: u64 = 0;
        desi | (ehb << 3) | (dequeue_ptr_phys_addr & (!0b1111) )
    }

    /// read trb from trb_start_addr and sets it to self.trb
    pub fn read(&mut self) {
        let trb = TRB::read(self.trb_start_addr as *const u64);
        self.trb = trb
    }
}

/// return address of the first TRB
/// created ring already has link TRB set with cycle bit set to 1
fn create_ring() -> u64 {
    // takes up PAGESIZE space
    const RING_SIZE: usize = 256;
    const TRB_SIZE: usize = 16;
    let ring_start_ptr = {
        let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE | PageTableFlags::NO_CACHE;
        let size = TRB_SIZE * RING_SIZE;
        let layout = Layout::from_size_align(size, Size4KiB::SIZE as usize).unwrap();
        let mut alloc_lock = ALLOCATOR.lock();
        unsafe { alloc_lock.fallback_alloc_zeroed_volatile(layout, flags) as *mut u64 }
    };
    println!("ring_start_ptr: {:?}", ring_start_ptr);
    let mut link_trb = LinkTRB::new();
    link_trb.set_ring_segment_ptr(ring_start_ptr as u64);
    link_trb.set_toggle_cycle(1);
    unsafe { link_trb.write(ring_start_ptr.add(RING_SIZE * 2 - 2)) };

    ring_start_ptr as u64
}
