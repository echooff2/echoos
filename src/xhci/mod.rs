use core::{alloc::Layout, intrinsics::volatile_set_memory, mem::{self, size_of}, ptr::{read_volatile, write_volatile}, slice};

use alloc::{alloc::handle_alloc_error, vec::Vec, vec};
use trb_rings::{CommandRing, EventRing};
use x86_64::{structures::paging::{PageSize, PageTableFlags, Size4KiB, Translate}, PhysAddr, VirtAddr};

use crate::{allocator::ALLOCATOR, bool_from_u32, memory::PAGE_TABLE, pci::PciGeneralDevice, println};

pub mod trb_rings;
pub mod trb;

#[derive(Debug)]
pub struct Xhci {
    pci: PciGeneralDevice,
    capability: XhciCapability,
    pub ops_registers: XhciOpsRegisters,
    pub runtime_registers: XhciRuntimeRegisters,
    /// is Some after init
    pub command_ring: Option<CommandRing>,
    pub doorbell_registers: DoorbellRegisters,
}

impl Xhci {
    pub unsafe fn new(pci: PciGeneralDevice, bar_addr: VirtAddr) -> Self {
        let capability = unsafe { XhciCapability::new(bar_addr) };
        println!("cap_length = {}", capability.cap_length);
        println!("hci_version = {}", capability.hci_version);
        let ops_registers_ptr = bar_addr.as_u64() + capability.cap_length as u64;
        let ops_registers = unsafe { XhciOpsRegisters::new(ops_registers_ptr) };
        let runtime_register_ptr = (bar_addr + capability.rts_off as u64).as_mut_ptr();
        let max_intrs = capability.max_intrs();
        let runtime_registers = unsafe {
            XhciRuntimeRegisters::new(runtime_register_ptr, max_intrs)
        };
        let doorbell_registers = DoorbellRegisters::new((bar_addr + capability.db_off as u64).as_mut_ptr());

        Self::new_debug_print(bar_addr);

        Self {
            pci,
            capability,
            ops_registers,
            runtime_registers,
            command_ring: None,
            doorbell_registers,
        }
    }

    /// SAFETY: should only be used when HCHalted == 1
    ///
    /// will spinlock until reset is complete
    pub unsafe fn reset(self, bar_addr: VirtAddr) -> Self {
        self.ops_registers.usb_cmd.set_hc_reset(true);
        while self.ops_registers.usb_cmd.get_hc_reset() == true {}
        unsafe { Self::new(self.pci, bar_addr) }
    }

    /// will spinlock until CNR flag == 0
    /// SAFETY: shouldn't be called if xhc is running (R/S = 1)
    pub unsafe fn init(&mut self) {
        assert!(self.capability.ac64() == true, "xhci doesn't support 64 bit addresses");
        assert_eq!(
            self.ops_registers.page_size(),
            0b1,
            "system with only xhci page size of 4KiB is supported"
        );
        println!("xhci R/S = {}", self.ops_registers.usb_cmd.rs());
        while self.ops_registers.usb_sts.cnr() == true {}
        while self.ops_registers.usb_cmd.get_hc_reset() == true {}

        println!("CNR = {}", self.ops_registers.usb_sts.cnr());

        println!("MaxSlots = {}", self.capability.get_max_slots());
        let max_slots = self.capability.get_max_slots();
        self.ops_registers.config.set_max_slots_en(max_slots);
        // on qemu = 0, on my laptop = 2
        let max_scratchpad_buffers = self.capability.max_scratchpad_buffers();
        println!("max scratchpad buffers: {}", max_scratchpad_buffers);
        let scratchpad_buffers_array_ptr = create_scratchpad_buffers(max_scratchpad_buffers);

        self.ops_registers.dcbaap.init(max_slots, scratchpad_buffers_array_ptr);
        println!("MaxSlotsEn = {}", self.ops_registers.config.max_slots_en());

        let command_ring = CommandRing::new(self.ops_registers.crcr_ptr());
        // it is initialized in CommandRing::new
        // let crcr_reg_val = command_ring.crcr_ptr() as u64 | 1;
        // unsafe { write_volatile(self.ops_registers.crcr_ptr(), crcr_reg_val) };
        self.command_ring = Some(command_ring);
        
        // TODO FIX THIS https://forum.osdev.org/viewtopic.php?t=37366
        let msix = self.pci.msix.as_mut().expect("msix is required for xhci driver");
        // TODO maybe shrink msix message table vec length to hcsparams1's MaxIntrs
        unsafe { msix.enable() };

        unsafe { self.runtime_registers.init() };
        self.ops_registers.usb_cmd.set_inte(true);
        self.runtime_registers.enable_interrupters();
        println!("imodi: {}", self.runtime_registers.irs[0].imodi());
        self.ops_registers.usb_cmd.set_rs(true);
        println!("xhci enabled");
    }

    pub fn is_running(&self) -> bool {
        self.ops_registers.usb_cmd.rs()
    }

    fn new_debug_print(bar_addr: VirtAddr) {
        let read: u32 = unsafe { read_volatile((bar_addr + 0x60_u64).as_ptr()) };
        let fladj = (read >> 8) as u8;
        println!("Fladj NFC: {}", (fladj >> 6) & 1);
        println!("FLADJ val: {}", fladj & 0b111111);
    }

    pub fn debug_print(&self) {
        let command_ring = self.command_ring.as_ref().unwrap();
        println!("crcr_ptr: {:?}", command_ring.crcr_ptr());
        let crcr = unsafe { read_volatile(command_ring.crcr_ptr()) };
        println!("CRR: {}", (crcr >> 3) & 1);
    }
}

/// return pointer to scratchpad_buffers_array as u64
/// or null if max_scratchpad_buffers == 0
fn create_scratchpad_buffers(max_scratchpad_buffers: u16) -> u64 {
    if max_scratchpad_buffers == 0 {
        return 0;
    }

    let array_size = max_scratchpad_buffers as usize * size_of::<u64>();
    let layout = Layout::from_size_align(array_size, Size4KiB::SIZE as usize).unwrap();
    let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE | PageTableFlags::NO_CACHE;
    let mut alloc_lock = ALLOCATOR.lock();
    let scratchpad_buffers_array_ptr = unsafe { alloc_lock.fallback_alloc(layout, flags) } as *mut u64;
    let scratchpad_buffers_array = unsafe { 
        slice::from_raw_parts_mut(scratchpad_buffers_array_ptr, max_scratchpad_buffers as usize) 
    };
    for buffer in scratchpad_buffers_array.iter_mut() {
        let layout = Layout::from_size_align(Size4KiB::SIZE as usize, Size4KiB::SIZE as usize).unwrap();
        let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE | PageTableFlags::NO_CACHE;
        let buffer_ptr = unsafe { alloc_lock.fallback_alloc(layout, flags) };
        let buffer_phys_addr = {
            let lock = PAGE_TABLE.lock();
            let page_table = lock.as_ref().unwrap();
            page_table.translate_addr(VirtAddr::from_ptr(buffer_ptr))
        };
        unsafe { write_volatile(buffer, buffer_phys_addr.unwrap().as_u64()) };
    }
    drop(alloc_lock);

    scratchpad_buffers_array_ptr as u64
}

// xhci manual: https://www.intel.com/content/dam/www/public/us/en/documents/technical-specifications/extensible-host-controler-interface-usb-xhci.pdf
#[derive(Debug, Clone)]
pub struct XhciCapability {
    pub cap_length: u8,
    pub hci_version: u16,
    pub hcs_params1: u32,
    pub hcs_params2: u32,
    pub hcs_params3: u32,
    pub hcc_params1: u32,
    pub db_off: u32,
    pub rts_off: u32,
    pub hcc_params2: u32,
}

impl XhciCapability {
    /// bar_addr must point to capabilities section offset = 0
    unsafe fn new(capability_bar_addr: VirtAddr) -> Self {
        let read: u32 = unsafe { read_volatile(capability_bar_addr.as_ptr()) };
        let cap_length = (read & 0xff) as u8;
        let hci_version = (read >> 16) as u16;
        let hcs_params1: u32 = unsafe { read_volatile((capability_bar_addr + 0x4_u64).as_ptr()) };
        let hcs_params2: u32 = unsafe { read_volatile((capability_bar_addr + 0x8_u64).as_ptr()) };
        let hcs_params3: u32 = unsafe { read_volatile((capability_bar_addr + 0xC_u64).as_ptr()) };
        let hcc_params1: u32 = unsafe { read_volatile((capability_bar_addr + 0x10_u64).as_ptr()) };
        let db_off: u32 = unsafe { read_volatile((capability_bar_addr + 0x14_u64).as_ptr()) };
        let rts_off: u32 = unsafe { read_volatile((capability_bar_addr + 0x18_u64).as_ptr()) };
        let hcc_params2: u32 = unsafe { read_volatile((capability_bar_addr + 0x1C_u64).as_ptr()) };

        Self {
            cap_length, hci_version, hcs_params1, hcs_params2, hcs_params3, hcc_params1, db_off, rts_off, hcc_params2
        }
    }

    fn get_max_slots(&self) -> u8 {
        self.hcs_params1 as u8
    }

    fn max_intrs(&self) -> u16 {
        ((self.hcs_params1 >> 8) & 2047) as u16
    }

    fn max_scratchpad_buffers(&self) -> u16 {
        let low_bits = (self.hcs_params2 >> 27) as u16;
        let high_bits = ((self.hcs_params2 >> 21) & 0b11111) as u16;
        low_bits | (high_bits << 5)
    }

    fn ac64(&self) -> bool {
        unsafe { bool_from_u32(self.hcc_params1 & 1) }
    }
}

#[derive(Debug)]
pub struct XhciOpsRegisters {
    pub usb_cmd: XhciUsbCMD,
    pub usb_sts: XhciUsbSTS,
    page_size: u32,
    // pub dnctrl: u32,
    /// pointer to crcr
    pub crcr_ptr: u64,
    // pub dcbaap: u64,
    pub dcbaap: XhciDCBAA,
    pub config: XhciConfigReg,
    // pub port_register_sets: Vec<u32>,
}

impl XhciOpsRegisters {
    /// SAFETY: address must be corrent pointer to OpsRegisters
    unsafe fn new(address: u64) -> Self {
        println!("address: {address}");
        Self {
            usb_cmd: XhciUsbCMD(address as *mut u32),
            usb_sts: XhciUsbSTS((address + 0x4) as *mut u32),
            page_size: unsafe { read_volatile((address + 0x8) as *const u32) },
            crcr_ptr: address + 0x18,
            dcbaap: XhciDCBAA::new((address + 0x30) as *mut u64),
            config: XhciConfigReg((address + 0x38) as *mut u32),
        }
    }

    pub fn page_size(&self) -> u32 {
        self.page_size
    }

    pub fn crcr_ptr(&mut self) -> *mut u64 {
        self.crcr_ptr as *mut u64
    }
}

#[derive(Debug)]
pub struct XhciUsbCMD(*mut u32);

impl XhciUsbCMD {
    pub fn rs(&self) -> bool {
        unsafe {
            let read = read_volatile(self.0);
            bool_from_u32(read & 1)
        }
    }

    pub fn set_rs(&mut self, new_val: bool) {
        unsafe { 
            let read = read_volatile(self.0);
            let read = read & (!1);
            write_volatile(self.0, read | new_val as u32);
        }
    }

    pub fn inte(&self) -> bool {
        unsafe {
            let read = read_volatile(self.0);
            bool_from_u32((read >> 2) & 1)
        }
    }

    pub fn set_inte(&mut self, new_val: bool) {
        unsafe {
            let read = read_volatile(self.0);
            let read = read & (!(1 << 2));
            let new_val = (new_val as u32) << 2;
            write_volatile(self.0, read | new_val);
        }
    }

    fn get_hc_reset(&self) -> bool {
        unsafe {
            let read = read_volatile(self.0);
            bool_from_u32((read >> 1) & 1)
        }
    }

    fn set_hc_reset(&self, new_val: bool) {
        unsafe {
            let read = read_volatile(self.0);
            let read = read & (!0b10);
            let new_val = (new_val as u32) << 1;
            write_volatile(self.0, read | new_val);
        }
    }

}

// writes to this has to be done in RW1C way
#[derive(Debug)]
pub struct XhciUsbSTS(*mut u32);

impl XhciUsbSTS {
    /// Controller not ready: true - Not ready for write, false - ready for writes
    pub fn cnr(&self) -> bool {
        unsafe {
            let read = read_volatile(self.0);
            bool_from_u32((read >> 11) & 1)
        }
    }

    /// HCHalted
    pub fn hch(&self) -> bool {
        unsafe { 
            let read = read_volatile(self.0);
            bool_from_u32(read & 1)
        }
    }

    /// Host System Error
    pub fn hse(&self) -> bool {
        unsafe {
            let read = read_volatile(self.0);
            bool_from_u32((read >> 2) & 1)
        }
    }

    /// Host Controller Error
    pub fn hce(&self) -> bool {
        unsafe {
            let read = read_volatile(self.0);
            bool_from_u32((read >> 12) & 1)
        }
    }
}

// TODO: should i implement Drop and drop inner manually?
#[derive(Debug)]
pub struct XhciDCBAA {
    dcbaap_ptr: *mut u64,
    /// do not use before calling init
    // inner: *mut u64,
    /// an array with inner_size size
    // inner: Option<NonNull<u64>>,
    inner: Option<&'static mut [u64]>,
    // max value = 256
    // inner_length: usize,
}

impl XhciDCBAA {
    fn new(dcbaap_ptr: *mut u64) -> Self {
        
        Self {
            dcbaap_ptr,
            inner: None,
            // inner_length: 0,
        }
    }

    /// create dcbaa and sets it's address to dcbaap operation register
    /// sets dcbaa first entry to scratchpad_buffers_array_ptr
    fn init(&mut self, max_slots_en: u8, scratchpad_buffers_array_ptr: u64) {
        // align required is only 64 bytes, but for the sake of keeping it withing one page it is
        // page aligned
        let elem_count = max_slots_en as usize + 1;
        let size = mem::size_of::<u64>() * elem_count;
        let inner_layout = Layout::from_size_align(size, Size4KiB::SIZE as usize).unwrap();
        let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE | PageTableFlags::NO_CACHE;
        let mut alloc = ALLOCATOR.lock();
        let inner_ptr = unsafe { alloc.fallback_alloc(inner_layout, flags) } as *mut u64;
        drop(alloc);
        if inner_ptr.is_null() {
            handle_alloc_error(inner_layout);
        }

        let (inner_phys_addr, scratchpad_phys_addr) = {
            let lock = PAGE_TABLE.lock();
            let page_table = lock.as_ref().unwrap();
            let inner_phys_addr = page_table.translate_addr(VirtAddr::from_ptr(inner_ptr)).unwrap();
            let scratchpad_phys_addr = if scratchpad_buffers_array_ptr == 0 { 
                PhysAddr::new(0)
            } 
            else {
                page_table.translate_addr(VirtAddr::new(scratchpad_buffers_array_ptr)).unwrap() 
            };
            (inner_phys_addr, scratchpad_phys_addr)
        };

        unsafe {
            volatile_set_memory(inner_ptr as *mut u8, 0_u8, size);
            inner_ptr.write_volatile(scratchpad_phys_addr.as_u64());
            let slice = slice::from_raw_parts_mut(inner_ptr, elem_count);

            self.inner = Some(slice);
            self.dcbaap_ptr.write_volatile(inner_phys_addr.as_u64());
        }

        // self.inner = inner_ptr as *mut u64;
        // self.inner = NonNull::new(inner_ptr);
        // self.inner_length = elem_count;

    }
}

#[derive(Debug)]
pub struct XhciConfigReg(*mut u32);

impl XhciConfigReg {
    fn max_slots_en(&self) -> u8 {
        let read = unsafe { read_volatile(self.0) };
        read as u8
    }

    fn set_max_slots_en(&mut self, new_val: u8) {
        let last = unsafe { read_volatile(self.0) };
        let last = last & (u32::MAX << 8);
        let new = last | (new_val as u32);
        unsafe { write_volatile(self.0, new) };
    }
}

#[derive(Debug)]
pub struct XhciRuntimeRegisters {
    inner_addr: u64,
    pub irs: Vec<XhciInterruptRegister>,
}

impl XhciRuntimeRegisters {
    pub unsafe fn new(runtime_register_ptr: *mut u64, max_intrs: u16) -> Self {
        println!("called XhciRuntimeRegisters::new with ptr: {:?}, max_intrs: {}", runtime_register_ptr, max_intrs);
        let mut ir_current_addr = runtime_register_ptr as u64 + 0x20;
        let mut irs = vec![];
        for _ in 0..max_intrs {
            let ir = XhciInterruptRegister::new(ir_current_addr as *mut u64);
            irs.push(ir);
            ir_current_addr += 32;
        }

        Self {
            inner_addr: runtime_register_ptr as u64,
            irs,
        }
    }

    pub unsafe fn init(&mut self) {
        for ir in self.irs.iter_mut() {
            unsafe { ir.init() };
        }
    }

    fn enable_interrupters(&mut self) {
        for ir in self.irs.iter_mut() {
            ir.set_enable(true);
        }
    }
}

#[derive(Debug)]
pub struct XhciInterruptRegister {
    inner: *mut u64,
    pub event_ring: EventRing,
}

impl XhciInterruptRegister {
    fn new(interrupter_ptr: *mut u64) -> Self {
        let event_ring = EventRing::new();

        Self {
            inner: interrupter_ptr,
            event_ring,
        }
    }

    /// SAFETY: must only be called once, interrupter_ptr passed in new must exists and be valid
    /// and RS must be 0 (xhci mustn't be run)
    unsafe fn init(&mut self) {
        unsafe {
            let first_read_ptr = (self.inner.add(1)) as *mut u32;
            let read = read_volatile(first_read_ptr);
            let read = read & (!(u16::MAX as u32));
            write_volatile(first_read_ptr, read | self.event_ring.erst_size() as u32);
            write_volatile(self.inner.add(3), self.event_ring.get_erdp());
            write_volatile(self.inner.add(2), self.event_ring.erst_phys_addr() & (!0b111111));
        }
    }

    pub fn imodi(&self) -> u16 {
        let u32_ptr = unsafe { (self.inner as *mut u32).add(1) };
        let read = unsafe { read_volatile(u32_ptr) };
        read as u16
    }

    pub fn ip(&self) -> bool {
        let read = unsafe { read_volatile(self.inner as *mut u32) };
        unsafe { bool_from_u32(read & 1) }
    }

    fn set_enable(&mut self, new_val: bool) {
        let read = unsafe { read_volatile(self.inner as *mut u32) };
        let read = read & (!0b10);
        unsafe { write_volatile(self.inner as *mut u32, read | new_val as u32) };
    }
}

#[derive(Debug)]
pub struct DoorbellRegisters {
    inner: *mut u32,
}

impl DoorbellRegisters {
    fn new(address: *mut u32) -> Self {
        Self { inner: address }
    }

    /// it is ub if arguments to this function are not correct
    pub unsafe fn ring(&mut self, doorbell_idx: u8, target: u8, task_id: u16) {
        let ptr = unsafe { self.inner.add(doorbell_idx as usize) };
        let payload = target as u32 | ((task_id as u32) << 16);
        unsafe { write_volatile(ptr, payload) };
    }
}
