use core::{error::Error, fmt::Display, ptr::{read_volatile, write_volatile}};

use alloc::vec::Vec;
use x86_64::VirtAddr;

use crate::{bool_from_u16, bool_from_u32, pci::BarSpace, println};

use super::{read_config_dword_address, write_config_dword_addr, PciBar, PciBars};

/// Remember to set enable bit to 1 in message control and unmask each table that is to be used
#[derive(Debug)]
pub struct MsiX {
    message_control: MsiXMessageControl,
    table: Vec<MsiXTable>,
    pbas: Vec<Pba>,
}

impl MsiX {
    /// SAFETY: address must point to the begining of msix_capability
    pub unsafe fn from_config_address(address: u32, bars: &PciBars) -> Self {
        let message_control = unsafe { MsiXMessageControl::read_from_address(address) };

        let table = read_config_dword_address(address + 0x4);
        let table_bir = (table & 0b111) as u8;
        let table_offset = table & (u32::MAX << 3);
        let pba = read_config_dword_address(address + 0x8);
        let pba_bir = (pba & 0b111 ) as u8;
        let pba_offset = pba & (u32::MAX << 3);

        let table_bar = bars.get_by_bir(table_bir).expect("msix specified bir, but that bar doesn't exists");
        debug_assert!(table_bar.get_space() == BarSpace::MemoryMapped, "MsiX bar must be memory mapped");
        let table_ptr = table_bar.get_address() + table_offset as u64;

        let pba_bar = bars.get_by_bir(pba_bir).expect("msix specified bir, but that bar doesn't exists");
        debug_assert!(pba_bar.get_space() == BarSpace::MemoryMapped, "MsiX bar must be memory mapped");
        let pba_ptr = pba_bar.get_address() + pba_offset as u64;

        let table_size = message_control.table_size();

        println!("msix table_size: {table_size}");

        let mut msix_table = Vec::with_capacity(table_size as usize);

        for i in 0..table_size {
            msix_table.push(MsiXTable(table_ptr + i as u64 * 16));
        }

        let mut pbas_count = table_size / 64;
        if pbas_count * 64 > table_size {
            pbas_count += 1;
        }

        let mut pbas = Vec::with_capacity(pbas_count as usize);

        for i in 0..pbas_count {
            pbas.push(Pba(pba_ptr + i as u64 * 8));
        }

        Self {
            message_control,
            table: msix_table,
            pbas,
        }
    }

    /// SAFETY: MSI has to be disabled and after this call interrupt based comunication is prohibited
    pub unsafe fn enable(&mut self) {
        self.message_control.set_msix_enable(true);
    }

    pub fn disable(&mut self) {
        self.message_control.set_msix_enable(false);
    }
}

/// these RW registers can only be written to by OS, hardware is prohibited
#[derive(Debug)]
pub struct MsiXMessageControl {
    address: u32,
    table_size: u16,
    function_mask: bool,
    msix_enable: bool,
}

impl MsiXMessageControl {
    /// SAFETY: address must point to the beggining of msix_capability
    unsafe fn read_from_address(address: u32) -> Self {
        let read = (read_config_dword_address(address) >> 16) as u16;
        let table_size = (read & (u16::MAX >> 5)) + 1;
        let function_mask = unsafe { bool_from_u16((read >> 14) & 1) };
        let msix_enable = unsafe { bool_from_u16((read >> 15) & 1) };

        Self { address, table_size, function_mask, msix_enable }
    }

    pub fn table_size(&self) -> u16 {
        self.table_size
    }

    pub fn function_mask(&self) -> bool {
        self.function_mask
    }

    pub fn set_function_mask(&mut self, new_function_mask: bool) {
        self.function_mask = new_function_mask;
        let read = read_config_dword_address(self.address);
        let read = read & (1 << 14);
        let new_val = read | ((new_function_mask as u32) << 14);
        write_config_dword_addr(self.address, new_val);
    }

    pub fn msix_enable(&self) -> bool {
        self.msix_enable
    }

    pub fn set_msix_enable(&mut self, new_msix_enable: bool) {
        self.msix_enable = new_msix_enable;
        let read = read_config_dword_address(self.address);
        let read = read & (1 << 15);
        let new_val = read | ((new_msix_enable as u32) << 15);
        write_config_dword_addr(self.address, new_val);
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct MsiXUnMaskedWriteErr;

impl Display for MsiXUnMaskedWriteErr {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "Error: tried to write to address/data field of msixtable without masking the table first")
    }
}

impl Error for MsiXUnMaskedWriteErr {}

/// Content of message address/data is defined in intel x86_64 manual volume 3 chapter 11.11
#[derive(Debug)]
pub struct MsiXTable(u64);

impl MsiXTable {
    /// table has to be masked
    pub fn write_address(&mut self, address: u64) -> Result<(), MsiXUnMaskedWriteErr> {
        if self.is_masked() == false {
            return Err(MsiXUnMaskedWriteErr);
        }

        unsafe { self.write_address_unchecked(address) };

        Ok(())
    }

    /// SAFETY: before using check if is_masked is true, otherwise DO NOT CALL
    pub unsafe fn write_address_unchecked(&mut self, address: u64) {
        debug_assert!(self.is_masked() == true, "Write to address field of msix entry is not permitted when entry is unmasked");
        let msg_addr_ptr = self.0 as *mut u64;

        unsafe { 
            // SAFETY: software is permitted to fill msg_addr with a single qword
            write_volatile(msg_addr_ptr, address & (!0b11));
        }
    }

    /// Panics if address is unmasked
    pub fn write_data(&mut self, data: u32) -> Result<(), MsiXUnMaskedWriteErr> {
        if self.is_masked() == false {
            return Err(MsiXUnMaskedWriteErr);
        }

        unsafe { self.write_data_unchecked(data) };

        Ok(())
    }

    /// SAFETY: before using check if is_masked is true, otherwise DO NOT CALL
    pub unsafe fn write_data_unchecked(&mut self, data: u32) {
        debug_assert!(self.is_masked() == true, "Write to address field of msix entry is not permitted when entry is unmasked");
        let msg_data_ptr = (self.0 + 8) as *mut u32;

        unsafe { 
            write_volatile(msg_data_ptr, data);
        }
    }

    fn is_masked(&self) -> bool {
        let vector_control_ptr = (self.0 + 12) as *const u32;
        let vector_control = unsafe { read_volatile(vector_control_ptr) };
        unsafe {bool_from_u32(vector_control & 1) }
    }

    fn set_masked(&mut self, mask: bool) {
        let vector_control_ptr = (self.0 + 12) as *mut u32;
        let read = unsafe { read_volatile(vector_control_ptr) };
        let read = read & (!0b1);
        let new_val = read | (mask as u32);
        unsafe { write_volatile(vector_control_ptr, new_val) };
    }
}

#[derive(Debug)]
pub struct Pba(u64);

impl Pba {
    fn read(&self) -> u64 {
        // TODO can I read 64 bit at a time?
        unsafe {
            let lower = read_volatile(self.0 as *const u32) as u64;
            let upper = read_volatile((self.0 + 4) as *const u32) as u64;
            lower | (upper << 32)
        }
    }

    fn write(&mut self, val: u64) {
        let lower_val = val as u32;
        let upper_val = (val >> 32) as u32;

        unsafe {
            write_volatile(self.0 as *mut u32, lower_val);
            write_volatile((self.0 + 4) as *mut u32, upper_val);
        }
    }
}
