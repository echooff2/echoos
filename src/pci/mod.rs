use core::{borrow::BorrowMut, cell::RefCell, fmt::{write, Display}, mem, ops::Deref};

use alloc::{boxed::Box, vec::Vec};
use alloc::vec;
use msix::MsiX;
use x86_64::{instructions::{interrupts::without_interrupts, port::{Port, PortWriteOnly}}, structures::paging::{mapper::MapToError, FrameAllocator, Mapper, PageTableFlags, PhysFrame, Size1GiB, Size4KiB}, PhysAddr, VirtAddr};

use crate::{bool_from_u16, bool_from_u32, bool_from_u8, memory::{self, find_free_p3_pages_to_fit}, println, serial_println};

pub mod msix;

// probably needs to be changed if try multithreading or even async
const CONFIG_ADDRESS_PORT: RefCell<PortWriteOnly<u32>> = RefCell::new(PortWriteOnly::new(0xCF8));
const CONFIG_DATA_PORT: RefCell<Port<u32>> = RefCell::new(Port::new(0xCFC));

#[derive(Debug)]
pub struct Pci {
    /// bus 0 is always on index 0
    /// vec always has at least one item 
    busses: Vec<PciBus>,
}

impl Pci {
    /// SAFETY: Pci has to be supported
    pub unsafe fn new() -> Self {
        let root_bus = PciBus::new(0);
        let busses = Self::discover_all_busses(root_bus);
        Self {
            busses
        }
    }

    pub fn root_device(&self) -> Option<&PciDevice> {
        self.busses[0].slots[0].functions[0].as_ref().map(|b| b.as_ref())
    }

    /// finds a device with class_code and subclass
    /// returned device might be PciDevice::JustHeader
    pub fn find_function_take(&mut self, class_code: u8, subclass: u8, prog_if: u8) -> Option<Box<PciDevice>> {
        self.busses.iter_mut().filter_map(|bus| bus.find_function_take(class_code, subclass, prog_if)).next()
    }

    pub fn busses(&self) -> &[PciBus] {
        &self.busses
    }
 
    fn discover_all_busses(root_bus: PciBus) -> Vec<PciBus> {
        let mut all_busses = vec![];
        let mut current_bus_idx = 0;

        // new_busses.push(root_bus.find_all_pci_to_pci_dev());
        all_busses.push(root_bus);

        while current_bus_idx < all_busses.len() {
            let mut new_busses = vec![];
            for pci_to_pci in all_busses[current_bus_idx].find_all_pci_to_pci_dev() {
                let secondary_bus = pci_to_pci.secondary_bus;
                let new_bus = PciBus::new(secondary_bus);
                new_busses.push(new_bus);
            }
            all_busses.append(&mut new_busses);
            current_bus_idx += 1;
        }

        all_busses
    }
}

#[derive(Debug)]
pub struct PciBus {
    bus_number: u8,
    /// slot 0 is always on index 0
    /// vec always has at least one item
    slots: Vec<PciSlot>,
}

impl PciBus {
    fn new(bus: u8) -> Self {
        let mut slots = vec![];
        for slot_idx in 0..32 {
            let slot = PciSlot::try_new(bus, slot_idx);
            match slot {
                Some(slot) => slots.push(slot),
                None => {}
            }
        }

        Self {
            bus_number: bus,
            slots,
        }
    }

    pub fn bus_number(&self) -> u8 {
        self.bus_number
    }

    pub fn find_function_take(&mut self, class_code: u8, subclass: u8, prog_if: u8) -> Option<Box<PciDevice>> {
        self.slots.iter_mut().filter_map(|slot| slot.find_function_take(class_code, subclass, prog_if)).next()
    }

    fn find_all_pci_to_pci_dev(&mut self) -> impl Iterator<Item = &PciToPciDevice> {
        self.slots.iter_mut().map(|slot| slot.find_all_pci_to_pci_dev()).flatten()
    }
}

#[derive(Debug)]
pub struct PciSlot {
    slot_number: u8,
    is_multi_function: bool,
    /// if is_multi_function == false this has only one item
    /// this vec always has at least one function
    /// item will be None when function is taken by a driver
    functions: Vec<Option<Box<PciDevice>>>,
}

impl PciSlot {
    fn try_new(bus: u8, slot: u8) -> Option<Self> {
        let mut functions = vec![];
        let header = PciDeviceHeader::new(bus, slot, 0).ok()?;
        let is_multi_function = header.is_multi_function;
        functions.push(Some(Box::new(PciDevice::JustHeader(header))));
        
        if is_multi_function {
            for func_idx in 1..8 {
                let func = PciDeviceHeader::new(bus, slot, func_idx);

                match func {
                    Ok(func) => functions.push(Some(Box::new(PciDevice::JustHeader(func)))),
                    Err(_) => {},
                }
            }
        }

        Some(Self {
            slot_number: slot,
            is_multi_function,
            functions,
        })
    }

    pub fn find_function_take(&mut self, class_code: u8, subclass: u8, prog_if: u8) -> Option<Box<PciDevice>> {
        for i in 0..self.functions.len() {
            if let Some(function) = self.functions[i].as_ref() {
                let header = function.get_header();
                if header.class_code == class_code && header.subclass == subclass && header.prog_if == prog_if {
                    return self.functions[i].take();
                }
            }
        }

        None
    }

    fn find_all_pci_to_pci_dev(&mut self) -> Vec<&PciToPciDevice> {
        let mut all_pci_to_pci = vec![];
        let functions_len = self.functions.len();

        for function_idx in 0..functions_len {
            // let mut function = &mut self.functions[function_idx];
            // let mut function = self.functions.get_mut(function_idx).unwrap();
            if let Some(function) = self.functions[function_idx].as_mut() {
                if function.get_header().header_type == PciHeaderType::PciToPci {
                    // 0x8080 is a random number
                    let dummy = unsafe { Box::from_raw(0x8080 as *mut PciDevice) };
                    without_interrupts(|| {
                        let inner_function = *mem::replace(function, dummy);
                        let pci_to_pci_device = inner_function.upgrade();
                        // box returned from this replace is invalid because it is dangling pointer
                        let dummy = mem::replace(function, Box::new(pci_to_pci_device));
                        // leak so that i don't try to dealocate dangling pointer
                        Box::leak(dummy);
                    });

                    let f_ptr = function.as_pci_to_pci() as *const PciToPciDevice;
                    all_pci_to_pci.push(unsafe { &*f_ptr });
                }
            }
        }

        all_pci_to_pci
    }
}

#[derive(Debug)]
pub struct PciDeviceHeader {
    pub device_id: u16,
    pub vendor_id: u16,
    pub status: PciStatus,
    pub command: PciCommand, // change type to PciCommand
    pub class_code: u8,
    pub subclass: u8,
    pub prog_if: u8,
    pub revision_id: u8,
    pub bist: u8,
    // pci-to-pci not supported - remember to panic at appriorate place TODO
    pub header_type: PciHeaderType,
    pub is_multi_function: bool,
    pub latency_timer: u8,
    pub cache_line_size: u8,
    bus: u8,
    slot: u8,
    func_number: u8,
    address: u32,
}

#[derive(Debug)]
pub enum PciDeviceError {
    NotFound,
}

impl Display for PciDeviceError {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            Self::NotFound => write!(f, "Couldn't find device"),
        }
    }
}

impl PciDeviceHeader {
    pub fn new(bus: u8, slot: u8, func_number: u8) -> Result<Self, PciDeviceError> {
        let first = read_config_dword(bus, slot, func_number, 0x0);

        let vendor_id = first & 0xffff;

        if vendor_id == 0xffff {
            return Err(PciDeviceError::NotFound);
        }

        let command_status_port_addr = get_config_port_address(bus, slot, func_number, 0x4);
        let second = read_config_dword_address(command_status_port_addr);
        let third = read_config_dword(bus, slot, func_number, 0x8);
        let fourth = read_config_dword(bus, slot, func_number, 0xC);

        let is_multi_function = unsafe { bool_from_u32((fourth >> 23) & 1) };

        Ok(Self {
            device_id: (first >> 16) as u16,
            vendor_id: (first & 0xffff) as u16,
            status: PciStatus::from_status_num((second >> 16) as u16),
            command: unsafe { PciCommand::from_address(command_status_port_addr) },
            class_code: (third >> 24) as u8,
            subclass: ((third >> 16) & 0xff) as u8,
            prog_if: ((third >> 8) & 0xff) as u8,
            revision_id: (third & 0xff) as u8,
            bist: (fourth >> 24) as u8,
            header_type: PciHeaderType::new((fourth >> 16) as u8),
            is_multi_function,
            latency_timer: ((fourth >> 8) & 0xff) as u8,
            cache_line_size: (fourth & 0xff) as u8,
            bus,
            slot,
            func_number,
            address: get_config_port_address(bus, slot, func_number, 0x0),
        })
    }

    // pub fn fetch_header(&mut self) {
    //     let reg3 = read_config_dword(self.bus, self.slot, self.func_number, 0xC);
    //     let (header_type, _) = PciHeaderType::from_header_type_num(
    //         ((reg3 >> 16) & 0xff) as u8,
    //         self,
    //     );
    //
    //     self.header_type = Some(header_type);
    // }

    pub fn into_pci_device(self) -> PciDevice {
        match self.header_type {
            PciHeaderType::GeneralDevice => {
                PciDevice::GeneralDevice(unsafe { self.into_general_device_unchecked() })
            },
            PciHeaderType::PciToPci => {
                PciDevice::PciToPciDevice(unsafe { self.into_pci_to_pci_device_unchecked() })
            },
            PciHeaderType::PciToCardBus => {
                panic!("Pci to cardbus pci device currently unsupported");
            }
        }
    }

    pub fn into_general_device(self) -> Option<PciGeneralDevice> {
        match self.header_type {
            PciHeaderType::GeneralDevice => unsafe {
                Some(self.into_general_device_unchecked())
            },
            _ => None
        }
    }

    pub unsafe fn into_general_device_unchecked(self) -> PciGeneralDevice {
        PciGeneralDevice::new(self)
    }

    pub fn into_pci_to_pci_device(self) -> Option<PciToPciDevice> {
        match self.header_type {
            PciHeaderType::PciToPci => unsafe {
                Some(self.into_pci_to_pci_device_unchecked())
            },
            _ => None
        }
    }

    pub unsafe fn into_pci_to_pci_device_unchecked(self) -> PciToPciDevice {
        PciToPciDevice::new(self)
    }

    pub fn get_status(&mut self) -> PciStatus {
        let status = read_config_dword(self.bus, self.slot, self.func_number, 0x4) >> 16;
        PciStatus::from_status_num(status as u16)
    }
}

#[derive(Debug)]
pub enum PciDevice {
    GeneralDevice(PciGeneralDevice),
    PciToPciDevice(PciToPciDevice),
    JustHeader(PciDeviceHeader),
}

impl PciDevice {
    fn get_header(&self) -> &PciDeviceHeader {
        match self {
            PciDevice::GeneralDevice(d) => &d.device,
            PciDevice::PciToPciDevice(d) => &d.pci_device_header,
            PciDevice::JustHeader(header) => &header,
        }
    }

    fn get_header_mut(&mut self) -> &mut PciDeviceHeader {
        match self {
            PciDevice::GeneralDevice(d) => &mut d.device,
            PciDevice::PciToPciDevice(d) => &mut d.pci_device_header,
            PciDevice::JustHeader(header) => header,
        }
    }

    /// consumes PciDevice::JustHeader and create either GeneralDevice or PciToPciDevice
    /// In case self is already GeneralDevice or PciToPciDevice return self
    pub fn upgrade(self) -> Self {
        match self {
            Self::JustHeader(header) => header.into_pci_device(),
            _=> self,
        }
    }

    fn as_pci_to_pci(&self) -> &PciToPciDevice {
        match self {
            PciDevice::PciToPciDevice(d) => d,
            _ => panic!("PciDevice is not a PciToPciDevice")
        }
    }
}

#[derive(Debug)]
pub struct PciStatus {
    pub detected_parity_error: bool,
    pub signaled_system_error: bool,
    pub received_master_abort: bool,
    pub received_target_abort: bool,
    pub signaled_target_abort: bool,
    pub devsel_timing: u8,
    pub master_data_parity_error: bool,
    pub fast_back_to_back_capable: bool,
    pub capable_66mhz: bool,
    pub capabillities_list: bool,
    pub interrupt_status: bool,
}

impl PciStatus {
    pub fn from_status_num(status: u16) -> Self {
        Self {
            detected_parity_error: (status >> 15) == 1,
            signaled_system_error: ((status >> 14) & 1) == 1,
            received_master_abort: ((status >> 13) & 1) == 1,
            received_target_abort: ((status >> 12) & 1) == 1,
            signaled_target_abort: ((status >> 11) & 1) == 1,
            devsel_timing: ((status >> 9) & 0b11) as u8,
            master_data_parity_error: ((status >> 8) & 1) == 1,
            fast_back_to_back_capable: ((status >> 7) & 1) == 1,
            capable_66mhz: ((status >> 5) & 1) == 1,
            capabillities_list: ((status >> 4) & 1) == 1,
            interrupt_status: ((status >> 3) & 1) == 1,
        }
    }

}


#[derive(Debug, Default)]
pub struct PciCommand {
    /// must remember that address points to both command and status
    address: u32,
    // TODO have all fields here
    /// RW
    io_space: bool,
    /// RW
    memory_space: bool,
}

impl PciCommand {
    unsafe fn from_address(address: u32) -> Self {
        let mut this = Self::default();
        this.address = address;
        this.refresh();
        this
    }

    fn get_val(&self) -> u16 {
        read_config_bottom_word_address(self.address)
    }

    /// refreshes all data, recommended before reading info of off struct
    fn refresh(&mut self) {
        let read = read_config_bottom_word_address(self.address);
        unsafe {
            self.io_space = bool_from_u16(read & 1);
            self.memory_space = bool_from_u16((read >> 1) & 1);
        }
    }

    fn set_memory_space(&mut self, new_val: bool) {
        const MEMORY_SPACE_MASK: u32 = !0b10;
        let read = read_config_dword_address(self.address);
        let val = read & MEMORY_SPACE_MASK;
        let new_val = (new_val as u32) << 1;
        let new_val = val | new_val;
        write_config_dword_addr(self.address, new_val);
    }

    fn set_io_space(&mut self, new_val: bool) {
        const IO_SPACE_MASK: u32 = !0b1;
        let read = read_config_dword_address(self.address);
        let val = read & IO_SPACE_MASK;
        let new_val = new_val as u32;
        let new_val = val | new_val;
        write_config_dword_addr(self.address, new_val);
    }
}

#[derive(Debug)]
pub struct PciGeneralDevice {
    pub device: PciDeviceHeader,
    pub bars: PciBars,
    pub cardbus_cis_pointer: u32,
    pub subsystem_id: u16,
    pub subsystem_vendor_id: u16,
    pub expansion_rom_base_address: u32,
    pub capabillities_pointer: u8,
    pub max_latency: u8,
    pub min_grant: u8,
    pub interrupt_pin: u8,
    pub interrupt_line: u8,
    pub msix: Option<MsiX>,
}

impl PciGeneralDevice {
    fn new(mut device: PciDeviceHeader) -> Self {
        let bars = PciBars(Self::get_bars(&mut device));
        let subsystem_info = read_config_dword(device.bus, device.slot, device.func_number, 0x2C);
        let last_info = read_config_dword(device.bus, device.slot, device.func_number, 0x3C);

        let cardbus_cis_pointer = read_config_dword(device.bus, device.slot, device.func_number, 0x28);
        let expansion_rom_base_address = read_config_dword(device.bus, device.slot, device.func_number, 0x30);
        let capabillities_pointer = (read_config_dword(device.bus, device.slot, device.func_number, 0x34) & 0xff) as u8;


        let mut gen_device = Self {
            device,
            bars,
            cardbus_cis_pointer,
            subsystem_id: (subsystem_info >> 16) as u16,
            subsystem_vendor_id: (subsystem_info & 0xffff) as u16,
            expansion_rom_base_address,
            capabillities_pointer,
            max_latency: (last_info >> 24) as u8,
            min_grant: ((last_info >> 16) & 0xff) as u8,
            interrupt_pin: ((last_info >> 8) & 0xff) as u8,
            interrupt_line: (last_info & 0xff) as u8,
            msix: None,
        };

        let msix = find_msix_capability(&mut gen_device);
        gen_device.msix = msix;

        gen_device
    }

    /// returns up to 6 bars, can be less if bars are 64 bit
    /// returns 6 if all bars are 32 bit
    fn get_bars(device: &mut PciDeviceHeader) -> Vec<PciBar> {
        let raw_bars = [
            read_config_dword(device.bus, device.slot, device.func_number, 0x10),
            read_config_dword(device.bus, device.slot, device.func_number, 0x14),
            read_config_dword(device.bus, device.slot, device.func_number, 0x18),
            read_config_dword(device.bus, device.slot, device.func_number, 0x1C),
            read_config_dword(device.bus, device.slot, device.func_number, 0x20),
            read_config_dword(device.bus, device.slot, device.func_number, 0x24),
        ];

        let mut bars = vec![];

        device.command.set_io_space(false);
        device.command.set_memory_space(false);

        let mut idx = 0;
        while idx < 6 {
            let mut bar = PciBar::new(raw_bars[idx] as u64, 0);
            // TODO i think it is better to crash but for testing lets just go to next bar
            // assert!(bar.get_space() == BarSpace::MemoryMapped, "Bar IO Space not supported");
            if bar.get_space() == BarSpace::IOSpace {
                serial_println!("WARNING: skipping iospace bar");
                println!("WARNING: skipping iospace bar");
                idx += 1;
                continue;
            }

            let size_info = get_size_info(device.bus, device.slot, device.func_number, 0x10_u8 + 4_u8 * idx as u8, bar.val as u32);
            println!("size_info: {}", size_info);
            serial_println!("size_info: {}", size_info);
            let size_bar = PciBar::new(size_info as u64, 0);
            let size_info = size_bar.get_address_bar0();
            // TODO was this correct?
            // bar.size = ((!size_info) + 1) as u64;
            bar.size = ((!size_info) as u64) + 1;

            if bar.get_type() == BarType::Bits64 {
                idx += 1;
                let second_bar = raw_bars[idx];
                let bar_addr = bar.get_address() + ((second_bar as u64) << 32);
                bar.set_address(bar_addr);

                let size_info = get_size_info(device.bus, device.slot, device.func_number, 0x10 + 4 * idx as u8, second_bar);
                let size = (((!size_info) + 1) as u64) << 32;
                bar.size |= size;
            }

            println!("this bar size: {}", bar.size);

            bars.push(bar);

            idx += 1;
        }

        device.command.set_io_space(true);
        device.command.set_memory_space(true);

        bars
    }

}

#[derive(Debug)]
pub struct PciToPciDevice {
    pci_device_header: PciDeviceHeader,
    primary_bus: u8,
    secondary_bus: u8,
}

impl PciToPciDevice {
    fn new(pci_device_header: PciDeviceHeader) -> Self {
        let read18 = read_config_dword_address(pci_device_header.address + 0x18);
        Self {
            pci_device_header,
            primary_bus: read18 as u8,
            secondary_bus: (read18 >> 8) as u8,
        }
    }
}

pub fn find_msix_capability(gen_device: &mut PciGeneralDevice) -> Option<MsiX> {
    if !gen_device.device.status.capabillities_list {
        return None;
    }

    let device = &gen_device.device;

    let mut capability_offset = read_config_dword(device.bus, device.slot, device.func_number, 0x34) as u8;

    while capability_offset != 0 {
        let capability_ptr = get_config_port_address(device.bus, device.slot, device.func_number, capability_offset);
        let capability = read_config_dword_address(capability_ptr);
        let id = capability & 0xff;

        println!("found capability: {}", id);
        if id == 0x11 {
            unsafe {
                return Some(MsiX::from_config_address(capability_ptr, &gen_device.bars));
            }
        }

        capability_offset = ((capability >> 8) & 0xfc) as u8;
    }

    None
}

#[derive(Debug)]
pub struct PciBars(Vec<PciBar>);

impl PciBars {
    pub fn get_by_bir(&self, bir: u8) -> Option<&PciBar> {
        let mut cur_bir = 0;
        for bar in self.0.iter() {
            if cur_bir == bir {
                return Some(bar);
            }

            cur_bir += 1;
            if bar.get_type() == BarType::Bits64 {
                cur_bir += 1;
            }
        }

        None
    }

    pub fn get_mut_by_bir(&mut self, bir: u8) -> Option<&mut PciBar> {
        let mut cur_bir = 0;
        for bar in self.0.iter_mut() {
            if cur_bir == bir {
                return Some(bar);
            }

            cur_bir += 1;
            if bar.get_type() == BarType::Bits64 {
                cur_bir += 1;
            }
        }

        None
    }
}

impl Deref for PciBars {
    type Target = [PciBar];
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

/// this then needs info bits masked and inverted all bits
fn get_size_info(bus: u8, slot: u8, func: u8, offset: u8, prev_val: u32) -> u32 {
    write_config_dword(bus, slot, func, offset, u32::MAX);
    let sizing_info = read_config_dword(bus, slot, func, offset);
    write_config_dword(bus, slot, func, offset, prev_val);
    sizing_info
}

#[derive(Debug, PartialEq, Eq)]
pub enum PciHeaderType {
    GeneralDevice,
    // TODO
    PciToPci,
    // TODO
    PciToCardBus,
}

impl PciHeaderType {
    pub fn new(header_type: u8) -> Self {
        match header_type & 0b11 {
            0x0 => Self::GeneralDevice,
            0x1 => Self::PciToPci,
            0x2 => {
                println!("Detected PciCardBus - Not yet implemented");
                Self::PciToCardBus
            },
            _ => panic!("pci header type exceeded 0x2, it shouldn't happen according to docs, header_type: {:#X}", header_type)
        }
    }
}

pub fn read_config_dword(bus: u8, slot: u8, func: u8, offset: u8) -> u32 {
    let addr = get_config_port_address(bus, slot, func, offset);
    read_config_dword_address(addr)
}

pub fn write_config_dword(bus: u8, slot: u8, func: u8, offset: u8, val: u32) {
    let addr = get_config_port_address(bus, slot, func, offset);
    write_config_dword_addr(addr, val);
}

fn get_config_port_address(bus: u8, slot: u8, func: u8, offset: u8) -> u32 {
    let bus = bus as u32;
    let slot = slot as u32;
    let func = func as u32;
    let offset = offset as u32;

    let addr: u32 = (bus << 16) | (slot << 11) | (func << 8) | (offset & 0xFC) | (1_u32 << 31);
    addr
}


fn read_config_dword_address(addr: u32) -> u32 {
    unsafe {
        without_interrupts(|| {
            CONFIG_ADDRESS_PORT.borrow_mut().write(addr);
            CONFIG_DATA_PORT.borrow_mut().read()
        })
    }
}

fn read_config_upper_word_address(addr: u32) -> u16 {
    unsafe {
        without_interrupts(|| {
            CONFIG_ADDRESS_PORT.borrow_mut().write(addr);
            (CONFIG_DATA_PORT.borrow_mut().read() >> 16) as u16
        })
    }
}

fn read_config_bottom_word_address(addr: u32) -> u16 {
    unsafe {
        without_interrupts(|| {
            CONFIG_ADDRESS_PORT.borrow_mut().write(addr);
            CONFIG_DATA_PORT.borrow_mut().read() as u16
        })
    }
}


fn write_config_dword_addr(addr: u32, val: u32) {
    unsafe {
        without_interrupts(|| {
            CONFIG_ADDRESS_PORT.borrow_mut().write(addr);
            CONFIG_DATA_PORT.borrow_mut().write(val);
        })
    }
}

fn disable_memory_space_and_io_space(command_status_port_addr: u32) {
    const DISABLE_MASK: u32 = u32::MAX << 2;
    let val = read_config_dword_address(command_status_port_addr);
    let new_val = val & DISABLE_MASK;
    write_config_dword_addr(command_status_port_addr, new_val);
}

fn enable_memory_space_and_io_space(command_status_port_addr: u32) {
    let val = read_config_dword_address(command_status_port_addr);
    let new_val = val | 0b11;
    write_config_dword_addr(command_status_port_addr, new_val);
}

#[derive(Debug, Clone)]
pub struct PciBar {
    val: u64,
    size: u64,
}

const BAR_ADDRESS_MASK: u64 = !0b1111;

impl PciBar {
    fn new(val: u64, size: u64) -> Self {
        Self {
            val,
            size,
        }
    }

    /// 0 - memory mapped, 1 - IO space
    pub fn get_space(&self) -> BarSpace {
        BarSpace::new((self.val as u8) & 0b1)
    }

    /// 0 - 32bit, 2 - 64bit
    pub fn get_type(&self) -> BarType {
        BarType::new(((self.val as u8) >> 1) & 0b11)
    }

    pub fn is_prefechable(&self) -> bool {
        unsafe { core::mem::transmute::<u8, bool>(((self.val >> 3) & 0b1) as u8) }
    }

    pub fn get_address(&self) -> u64 {
        self.val & BAR_ADDRESS_MASK
    }

    pub fn memory_map(&self) -> Result<VirtAddr, MapToError<Size1GiB>> {
        // allocate less for smaller bars
        let bar0_addr = self.get_address() as u64;
        let bar0_phys_addr = PhysAddr::new(bar0_addr);
        let bar0_frame: PhysFrame<Size1GiB> = PhysFrame::containing_address(bar0_phys_addr);
        let page_offset = bar0_phys_addr.as_u64() - bar0_frame.start_address().as_u64();
        println!("bar page_offset = {}", page_offset);
        let p2_pages = find_free_p3_pages_to_fit(self.size());
        let bar0_virt_addr = p2_pages[0].start_address() + page_offset;
        let mut bar0_page_flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE;
        match self.is_prefechable() {
            true => bar0_page_flags |= PageTableFlags::WRITE_THROUGH,
            false => bar0_page_flags |= PageTableFlags::NO_CACHE,
        }

        memory::map_p3s_to(&p2_pages, bar0_page_flags, bar0_phys_addr, self.size())?;

        Ok(bar0_virt_addr)
    }

    fn get_address_bar0(&self) -> u32 {
        const ADDRESS_BAR0_MASK: u32 = !0b1111;
        self.val as u32 & ADDRESS_BAR0_MASK
    }

    fn get_address_bar1(&self) -> u32 {
        (self.val >> 32) as u32
    }

    fn set_address(&mut self, new_val: u64) {
        let v = self.val & 0b1111;
        let v = v | (new_val & BAR_ADDRESS_MASK);
        self.val = v;
    }

    pub fn size(&self) -> u64 {
        self.size
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum BarType {
    Bits32 = 0,
    Bits64 = 2,
}

impl BarType {
    fn new(type_num: u8) -> Self {
        match type_num {
            0 => Self::Bits32,
            2 => Self::Bits64,
            _ => panic!("unimplemented or pci device malfunctioned, got type: {}", type_num)
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum BarSpace {
    MemoryMapped = 0,
    IOSpace = 1,
}

impl BarSpace {
    fn new(type_num: u8) -> Self {
        match type_num {
            0 => Self::MemoryMapped,
            1 => Self::IOSpace,
            _ => panic!("unimplemented or pci device malfunctioned, got type: {}", type_num)
        }
    }
}
