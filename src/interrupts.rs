use core::arch::x86_64::_rdtsc;
use core::cell::SyncUnsafeCell;

use lazy_static::lazy_static;
use x86_64::structures::idt::{
    InterruptDescriptorTable, InterruptStackFrame, PageFaultErrorCode,
};

use crate::apic::{APIC, TIMER_INTERRUPT_INDEX};
use crate::gdt::DOUBLE_FAULT_IST_INDEX;
use crate::hpet::HPET;
use crate::{hlt_loop, println, serial_println};

pub const PIC_1_OFFSET: u8 = 32;
pub const PIC_2_OFFSET: u8 = PIC_1_OFFSET + 8;

lazy_static! {
    static ref IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();
        idt.breakpoint.set_handler_fn(breakpoint_handler);
        unsafe {
            idt.double_fault
                .set_handler_fn(double_fault_handler)
                .set_stack_index(DOUBLE_FAULT_IST_INDEX);
        }
        idt.general_protection_fault
            .set_handler_fn(general_protection_fault_handler);
        idt.page_fault.set_handler_fn(page_fault_handler);
        idt.invalid_opcode.set_handler_fn(invalid_opcode_fault_handler);

        // idt[TIMER_INTERRUPT_INDEX].set_handler_fn(apic_timer_handler);
        idt[TIMER_INTERRUPT_INDEX].set_handler_fn(apic_timer_handler);

        idt[32].set_handler_fn(test_error_handler);

        idt
    };
}

pub fn init_idt() {
    IDT.load();
}

extern "x86-interrupt" fn test_error_handler(stack_frame: InterruptStackFrame) {
    serial_println!("test error interrupt, stack: {:?}", stack_frame);
    println!("test error interrupt, stack: {:?}", stack_frame);
}

/// SAFETY: DO NOT READ BEFORE APIC TIMER CALIBRATION ENDS
// static mut APIC_INTERVAL: u64 = 0;
//
// extern "x86-interrupt" fn apic_timer_calibration(_stack_frame: InterruptStackFrame) {
//     static mut TIME1: Option<u64> = None;
//
//     unsafe {
//         if TIME1.is_none() {
//             TIME1 = Some(_rdtsc());
//             APIC.lock().assume_init_mut().end_of_interrupt();
//             return;
//         }
//     };
//
//     let time2 = unsafe { _rdtsc() };
//
//     let interval = time2 - unsafe { TIME1.unwrap_unchecked() };
//
//     unsafe {
//         APIC_INTERVAL = interval;
//     };
//
//     // change idt to normal timer handler
//
//     unsafe {
//         let idt = &mut *IDT.get();
//         idt[TIMER_INTERRUPT_INDEX].set_handler_fn(apic_timer_handler);
//     };
//
//     unsafe {
//         APIC.lock().assume_init_mut().end_of_interrupt();
//     }
// }

extern "x86-interrupt" fn apic_timer_handler(_stack_frame: InterruptStackFrame) {
    // let hpet = HPET.lock();
    // let timer = unsafe { hpet.assume_init_ref().get_main_counter() };
    // drop(hpet);
    // serial_println!("{}", timer);
    unsafe {
        // possible deadlock if interrupt happens when some other piece of code has APIC locked
        APIC.lock().assume_init_mut().end_of_interrupt();
    }

    // unnecessary because of .end_of_interrupt();
    // let eoi_reg = (0x8000 + 0xb0) as *mut u32;
    // unsafe { 
    //     eoi_reg.write_volatile(0);
    // }
}

// extern "x86-interrupt" fn test_timer_handler(_stack_frame: InterruptStackFrame) {
//     // serial_println!("timer interrupt");
//     // println!("timer interrupt");
//     // unsafe {
//     //     APIC.lock().assume_init_mut().end_of_interrupt();
//     // }
//
//     let eoi_reg = (0x8000 + 0xb0) as *mut u32;
//     unsafe {
//         eoi_reg.write_volatile(0);
//     }
// }

extern "x86-interrupt" fn breakpoint_handler(stack_frame: InterruptStackFrame) {
    println!("EXCEPTION BREAKPOINT\n{:#?}", stack_frame);
}

extern "x86-interrupt" fn double_fault_handler(
    stack_frame: InterruptStackFrame,
    _error_code: u64,
) -> ! {
    // TEXT_DISPLAY.lock().as_mut().unwrap().clear();
    serial_println!("DOUBLE_FAULT\n{:#?}", stack_frame);
    println!("DOUBLE_FAULT\n{:#?}", stack_frame);
    panic!("EXCEPTION DOUBLE FAULT\n{:#?}", stack_frame);
}

extern "x86-interrupt" fn general_protection_fault_handler(
    stack_frame: InterruptStackFrame,
    error_code: u64,
) {
    serial_println!(
        "GENERAL PROTECTION FAULT, error_code: {}\n{:#?}",
        error_code, stack_frame
    );

    println!(
        "GENERAL PROTECTION FAULT, error_code: {}\n{:#?}",
        error_code, stack_frame
    );
}

extern "x86-interrupt" fn page_fault_handler(
    stack_frame: InterruptStackFrame,
    error_code: PageFaultErrorCode,
) {
    use x86_64::registers::control::Cr2;

    serial_println!("\nEXCEPTION PAGE FAULT");
    serial_println!("Accessed Address: {:?}", Cr2::read());
    serial_println!("Error Code: {:?}", error_code);
    serial_println!("{:#?}", stack_frame);

    println!("\nEXCEPTION PAGE FAULT");
    println!("Accessed Address: {:?}", Cr2::read());
    println!("Error Code: {:?}", error_code);
    println!("{:#?}", stack_frame);

    hlt_loop();
}

extern "x86-interrupt" fn invalid_opcode_fault_handler(stack_frame: InterruptStackFrame) {
    serial_println!("\nINVALID OPCODE FAULT");
    serial_println!("{:#?}", stack_frame);

    println!("\nINVALID OPCODE FAULT");
    println!("{:#?}", stack_frame);

    hlt_loop();
}
