use core::{mem::MaybeUninit, time::Duration};

use acpi::platform::interrupt::Apic;
use raw_cpuid::CpuId;
use spin::Mutex;
use volatile::Volatile;
use x2apic::lapic::{LocalApic, LocalApicBuilder, TimerDivide, TimerMode};
use x86_64::{instructions::interrupts::without_interrupts, VirtAddr};

use crate::{hpet::HPET, println, serial_println};

pub const TIMER_INTERRUPT_INDEX: usize = 34;

pub static APIC: Mutex<MaybeUninit<LocalApic>> = Mutex::new(MaybeUninit::uninit());

/// Safety: should be called only once
pub unsafe fn init(apic: Apic, phys_mem_offset: VirtAddr) {
    let cpuid = CpuId::new();
    let has_apic = cpuid
        .get_feature_info()
        .map_or(false, |features| features.has_apic());
    if !has_apic {
        panic!("This OS doesn't support systems without APIC");
    }
    println!("{:#x}", apic.local_apic_address + phys_mem_offset.as_u64());
    println!("phys_mem_offset_debug: {:?}", phys_mem_offset);
    let mut lapic = LocalApicBuilder::new()
        .error_vector(32)
        .spurious_vector(33)
        .timer_vector(TIMER_INTERRUPT_INDEX)
        // .set_xapic_base(phys_mem_offset.as_u64() + unsafe { xapic_base() })
        // TODO get this from acpi acpi::platform::interrupt::IoApic::address i think
        .set_xapic_base(0x8000)
        .timer_mode(TimerMode::Periodic)
        .timer_initial(u32::MAX)
        .timer_divide(TimerDivide::Div16)
        .build()
        .expect("couldn't build lapic/x2apic not supported");

    unsafe {
        // lapic.set_timer_initial(999_999);
        // lapic.set_timer_divide(TimerDivide::Div2);
        // lapic.set_timer_mode(TimerMode::Periodic);
        lapic.enable();
        lapic.enable_timer();
        println!("error flags: {:?}", lapic.error_flags());

        // calibrate lapic timer
        without_interrupts(|| {
            let hpet_lock = HPET.lock();
            let hpet = hpet_lock.assume_init_ref();
            let hpet_counter = hpet.get_main_counter();
            let apic_timer_current = lapic.timer_current();
            let mut dummy = Volatile::new(0_u64);
            for _ in 0..100000 {
                dummy.write(0);
            }
            let hpet_counter_elapsed = hpet.get_main_counter() - hpet_counter;
            let apic_timer_elapsed =  apic_timer_current - lapic.timer_current();

            let elapsed_time_femtosec = hpet_counter_elapsed * hpet.get_counter_clk_period() as u64;

            // 10^15 fs = 1 s
            let periods_in_1sec = 10_u64.pow(15) / elapsed_time_femtosec;
            let lapic_period_1sec = apic_timer_elapsed * periods_in_1sec as u32;
            println!("lapic period 1sec: {}", lapic_period_1sec);
            serial_println!("lapic period 1sec: {}", lapic_period_1sec);
            lapic.set_timer_initial(lapic_period_1sec);
        });

        x86_64::instructions::interrupts::enable();
        // set task prority to allow every priority interrupt
        // https://forum.osdev.org/viewtopic.php?f=1&t=12045&hilit=APIC+init
        crate::cr8::write(crate::cr8::read() & 0xffff_ffff_ffff_fff0);
        // lapic.disable_timer();
    }

    *APIC.lock() = MaybeUninit::new(lapic);

    println!("---Apic enabled!!!---");
}
