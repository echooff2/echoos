use alloc::vec::Vec;
use bootloader::boot_info::{MemoryRegion, MemoryRegionKind, MemoryRegions};
use spin::Mutex;
use spin::MutexGuard;
use x86_64::registers::control::Cr3;
use x86_64::structures::paging::mapper::MapToError;
use x86_64::structures::paging::mapper::MapperFlush;
use x86_64::structures::paging::page::PageRangeInclusive;
use x86_64::structures::paging::page_table::PageTableEntry;
use x86_64::structures::paging::FrameAllocator;
use x86_64::structures::paging::Mapper;
use x86_64::structures::paging::OffsetPageTable;
use x86_64::structures::paging::Page;
use x86_64::structures::paging::PageSize;
use x86_64::structures::paging::PageTable;
use x86_64::structures::paging::PageTableFlags;
use x86_64::structures::paging::PageTableIndex;
use x86_64::structures::paging::PhysFrame;
use x86_64::structures::paging::Size1GiB;
use x86_64::structures::paging::Size4KiB;
use x86_64::structures::paging::Translate;
use x86_64::PhysAddr;
use x86_64::VirtAddr;

use crate::println;
use crate::rand;
use crate::rand::rdrand::RDRAND;
use crate::PHYS_MEM_OFFSET;

/// lvl 4 page table - implements Mapper
pub static PAGE_TABLE: Mutex<Option<OffsetPageTable<'static>>> = Mutex::new(None);
pub static FRAME_ALLOCATOR: Mutex<Option<BootInfoFrameAllocator>> = Mutex::new(None);

/// Initialize a new OffsetPageTable.
///
/// # Safety
/// This function is unsafe because the caller must guarantee that the
/// complete physical memory is mapped to virtual memory at the passed
/// `physical_memory_offset`. Also, this function must be only called once
/// to avoid aliasing `&mut` references (which is undefined behavior).
pub unsafe fn init(physical_memory_offset: VirtAddr) {
    unsafe {
        let level_4_table = active_level_4_table(physical_memory_offset);
        let mut page_table_lock = PAGE_TABLE.lock();
        *page_table_lock = Some(OffsetPageTable::new(level_4_table, physical_memory_offset));
    }
}

trait FrameNumber {
    fn start_frame_number(&self) -> u64;
    fn end_frame_number(&self) -> u64;
}

impl FrameNumber for MemoryRegion {
    fn start_frame_number(&self) -> u64 {
        self.start / 4096
    }

    fn end_frame_number(&self) -> u64 {
        self.end / 4096
    }
}

/// TODO make sure to never give address that is 32 bit and starts with 0xfee because that is for
/// interrupts - msi data/address i think. Not sure if that region is in MemoryRegions or not. Not
/// sure if i actually need to make sure it is not given out. Intel manual volume 3 chapter 11.11.1
#[allow(dead_code)]
pub struct BootInfoFrameAllocator {
    region_in_use: u64,
    frame_in_use: u64,
    max_frames_in_region: u64,
    region_usable_entries: [Option<&'static MemoryRegion>; 2048],
}

impl BootInfoFrameAllocator {
    pub unsafe fn init(memory_map: &'static MemoryRegions) {
        // TODO I think this constant doesn't exists anymore so let's not depend on it
        // let mut region_usable_entries: [Option<&'static MemoryRegion>; 256] = [None; 256]; // 64 is taken from bootloader crate's private constant
        let mut region_usable_entries: [Option<&'static MemoryRegion>; 2048] = [None; 2048]; // 64 is taken from bootloader crate's private constatnt

        let mut next_usable_index = 0;

        for region in memory_map.iter() {
            if region.kind == MemoryRegionKind::Usable {
                region_usable_entries[next_usable_index] = Some(region);
                next_usable_index += 1;
            }
        }

        let mut frame_allocator_lock = FRAME_ALLOCATOR.lock();

        *frame_allocator_lock = Some(BootInfoFrameAllocator {
            region_in_use: 0,
            frame_in_use: 0,
            max_frames_in_region: region_usable_entries[0].unwrap().end_frame_number()
                - region_usable_entries[0].unwrap().start_frame_number(),
            region_usable_entries,
        });
    }
}

unsafe impl FrameAllocator<Size4KiB> for BootInfoFrameAllocator {
    fn allocate_frame(&mut self) -> Option<PhysFrame<Size4KiB>> {
        if self.frame_in_use >= self.max_frames_in_region {
            // region is full, use other region
            // select region that has space
            for region_index in self.region_in_use + 1..self.region_usable_entries.len() as u64 {
                let region = self.region_usable_entries[region_index as usize].unwrap();

                if region.end_frame_number() - region.start_frame_number() < 2 {
                    continue;
                }

                self.region_in_use = region_index;
                self.frame_in_use = 1;
                self.max_frames_in_region = region.end_frame_number() - region.start_frame_number();

                return Some(PhysFrame::containing_address(PhysAddr::new(region.start)));
            }
            //out of memory regions!
            return None;
        }

        let region = self.region_usable_entries[self.region_in_use as usize].unwrap();
        let frame_addr = PhysAddr::new((region.start_frame_number() + self.frame_in_use) * 4096);

        self.frame_in_use += 1;

        Some(PhysFrame::containing_address(frame_addr))
    }
}

pub struct EmptyFrameAllocator;

unsafe impl FrameAllocator<Size4KiB> for EmptyFrameAllocator {
    fn allocate_frame(&mut self) -> Option<PhysFrame<Size4KiB>> {
        None
    }
}

pub fn create_example_mapping(
    page: Page,
    mapper: &mut OffsetPageTable,
    frame_allocator: &mut impl FrameAllocator<Size4KiB>,
) {
    use x86_64::structures::paging::PageTableFlags as Flags;

    let frame = PhysFrame::containing_address(PhysAddr::new(0xb8000));
    let flags = Flags::PRESENT | Flags::WRITABLE;

    let map_to_result = unsafe {
        // FIXME: this is not safe, we do it only for testing
        mapper.map_to(page, frame, flags, frame_allocator)
    };

    map_to_result.expect("map_to failed").flush();
}

// TODO delete me
pub fn create_test_bar0_mapping(
    addr: u64,
    page: Page,
    mapper: &mut OffsetPageTable,
    frame_allocator: &mut impl FrameAllocator<Size4KiB>,
) {
    use x86_64::structures::paging::PageTableFlags as Flags;

    let frame = PhysFrame::containing_address(PhysAddr::new(addr));
    let flags = Flags::PRESENT | Flags::WRITABLE | Flags::NO_CACHE;

    let map_to_result = unsafe { mapper.map_to(page, frame, flags, frame_allocator) };

    map_to_result.expect("map_to failed").flush();
}

/// Returns a mutable reference to the active level 4 table.
///
/// This function is unsafe because the caller must guarantee that the
/// complete physical memory is mapped to virtual memory at the passed
/// `physical_memory_offset`. Also, this function must be only called once
/// to avoid aliasing `&mut` references (which is undefined behavior).
unsafe fn active_level_4_table(physical_memory_offset: VirtAddr) -> &'static mut PageTable {
    let (level_4_table_frame, _) = Cr3::read();

    let phys = level_4_table_frame.start_address();
    let virt = physical_memory_offset + phys.as_u64();
    let page_table_ptr: *mut PageTable = virt.as_mut_ptr();

    unsafe { &mut *page_table_ptr }
}

///  to get correct result must be called after PHYS_MEM_OFFSET initialization
pub fn is_page_mapped(page: &Page<Size4KiB>) -> bool {
    let phys_mem_offset = PHYS_MEM_OFFSET;
    match unsafe { translate_addr_old(page.start_address(), *phys_mem_offset) } {
        Some(_) => true,
        None => false,
    }
}

///  to get correct result must be called after PHYS_MEM_OFFSET initialization
// TODO maybe use software prng or cprng
pub fn find_free_page() -> Page<Size4KiB> {
    loop {
        const VIRT_ADDR_MASK: u64 = u64::MAX >> 16;
        let addr = RDRAND.get_u64() & VIRT_ADDR_MASK;
        let virt_addr = VirtAddr::new(addr);
        let page = Page::containing_address(virt_addr);
        if !is_page_mapped(&page) {
            return page;
        }
    }
}

/// size cannot be 0
pub fn find_free_pages_to_fit(size: u64) -> PageRangeInclusive<Size4KiB> {
    debug_assert_ne!(size, 0);
    loop {
        let first_page: Page<Size4KiB> = {
            let virt_addr = VirtAddr::new(RDRAND.get_u64() & (u64::MAX >> 16));
            Page::containing_address(virt_addr)
        };
        let last_page: Page<Size4KiB> = {
            let virt_addr = first_page.start_address() + size - 1_u64;
            Page::containing_address(virt_addr)
        };

        let page_range = Page::range_inclusive(first_page, last_page);

        let mut is_free = true;
        for page in page_range {
            if is_page_mapped(&page) {
                is_free = false;
                break;
            }
        }

        if !is_free {
            continue;
        }

        return page_range;
    }
}

// TODO make it work universally
/// supports up to 512GiB size
pub fn find_free_p3_pages_to_fit(size: u64) -> Vec<Page<Size1GiB>> {
    assert!(
        size <= Size1GiB::SIZE * 512,
        "find_free_p2_pages_to_fit supports size <= 512GiB"
    );
    let required_page_amount = (size / Size1GiB::SIZE) + 1;
    let mut pages: Vec<Page<Size1GiB>> = Vec::with_capacity(required_page_amount as usize);

    let p4_index = loop {
        const VIRT_ADDR_P3_P4_MASK: u64 = 0xFFFFC0000000;
        let addr = VirtAddr::new(RDRAND.get_u64() & VIRT_ADDR_P3_P4_MASK);

        // check if p3 is unused and if it is used retry
        let p4_index = addr.p4_index();

        let (lv4_table_frame, _) = Cr3::read();
        let lv4_table: *const PageTable =
            (*PHYS_MEM_OFFSET + lv4_table_frame.start_address().as_u64()).as_ptr();
        let lv4_table = unsafe { &*lv4_table };
        let lv4_entry = &lv4_table[p4_index];
        if lv4_entry.is_unused() {
            break p4_index;
        }
    };

    for p3_index in 0..required_page_amount {
        let p3_index = PageTableIndex::new_truncate(p3_index as u16);
        let page: Page<Size1GiB> = Page::from_page_table_indices_1gib(p4_index, p3_index);
        pages.push(page);
    }

    pages
}

pub unsafe fn translate_addr_old(addr: VirtAddr, phys_mem_offset: VirtAddr) -> Option<PhysAddr> {
    use x86_64::structures::paging::page_table::FrameError;

    // read the active level 4 frame from the CR3 register
    let (level_4_table_frame, _) = Cr3::read();

    let table_indexes = [
        addr.p4_index(),
        addr.p3_index(),
        addr.p2_index(),
        addr.p1_index(),
    ];
    let mut frame = level_4_table_frame;

    // traverse the multi-level page table
    for &index in &table_indexes {
        // convert the frame into a page table reference
        let virt = phys_mem_offset + frame.start_address().as_u64();
        let table_ptr: *const PageTable = virt.as_ptr();
        let table = unsafe { &*table_ptr };

        // read the page table entry and update `frame`
        let entry = &table[index];
        frame = match entry.frame() {
            Ok(frame) => frame,
            Err(FrameError::FrameNotPresent) => return None,
            Err(FrameError::HugeFrame) => panic!("huge pages not supported"),
        };
    }

    // calculate the physical address by adding the page offset
    Some(frame.start_address() + u64::from(addr.page_offset()))
}

pub fn translate_addr_blocking(virt_addr: VirtAddr) -> Option<PhysAddr> {
    let lock = PAGE_TABLE.lock();
    let page_table = lock.as_ref().unwrap();
    page_table.translate_addr(virt_addr)
}

pub fn map_p3s_to(
    pages: &[Page<Size1GiB>],
    page_flags: PageTableFlags,
    phys_addr_start: PhysAddr,
    size: u64,
) -> Result<(), MapToError<Size1GiB>> {
    let start_frame: PhysFrame<Size1GiB> = PhysFrame::containing_address(phys_addr_start);
    let phys_addr_end = phys_addr_start + size;
    let end_frame: PhysFrame<Size1GiB> = PhysFrame::containing_address(phys_addr_end);
    let frames = PhysFrame::range(start_frame, end_frame);

    let mut amount_mapped = 0;

    let mut last_map: Option<MapperFlush<Size1GiB>> = None;
    let mut mapper_lock = PAGE_TABLE.lock();
    let mapper = mapper_lock.as_mut().unwrap();
    let mut frame_alloc_lock = FRAME_ALLOCATOR.lock();
    let frame_allocator = frame_alloc_lock.as_mut().unwrap();

    for (page, frame) in pages.iter().zip(frames) {
        last_map = Some(unsafe { mapper.map_to(*page, frame, page_flags, frame_allocator)? });
        amount_mapped += 1;
    }
    drop(mapper_lock);
    drop(frame_alloc_lock);

    last_map.map(|map| map.flush());

    assert!(
        amount_mapped <= pages.len(),
        "there weren't enough free pages to map to frames"
    );

    Ok(())
}
