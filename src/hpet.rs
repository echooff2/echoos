use core::mem::MaybeUninit;

use acpi::HpetInfo;
use spin::Mutex;
use x86_64::{
    instructions::interrupts::without_interrupts, structures::paging::{
        mapper::MapToError, FrameAllocator, Mapper, Page, PageSize, PageTableFlags, PhysFrame, Size4KiB
    }, PhysAddr, VirtAddr
};

use crate::{memory::{self, FRAME_ALLOCATOR, PAGE_TABLE}, println};

/// SAFETY: only accessible after Hpet::init is called
pub static HPET: Mutex<MaybeUninit<Hpet>> = Mutex::new(MaybeUninit::uninit());

pub struct Hpet {
    hpet_registers: HpetRegisters,
}

impl Hpet {
    /// this method additionally enables cnf
    /// SAFETY: must only be called once and rdrand presence must be checked before calling
    pub unsafe fn init(
        hpet_info: HpetInfo,
    ) -> Result<(), MapToError<Size4KiB>> {
        println!("initializing hpet");
        // TODO make sure base_address is on virt memory
        println!("hpet adress: {:#X}", hpet_info.base_address);

        let page: Page<Size4KiB> = unsafe { memory::find_free_page() };
        println!("found free page: {page:?}");
        let hpet_phys_addr = PhysAddr::new(hpet_info.base_address as u64);
        println!("hpet_phys_addr is aligned == {}", hpet_phys_addr.is_aligned(Size4KiB::SIZE));
        let hpet_phys_addr_aligned = hpet_phys_addr.align_down(Size4KiB::SIZE);
        let offset = hpet_phys_addr - hpet_phys_addr_aligned;
        let hpet_reg_virt_addr = page.start_address() + offset;
        let hpet_phys_frame = unsafe { PhysFrame::from_start_address_unchecked(hpet_phys_addr_aligned) };
        let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE | PageTableFlags::NO_CACHE;

        let mut frame_alloc_lock = FRAME_ALLOCATOR.lock();
        let frame_allocator = frame_alloc_lock.as_mut().unwrap();
        let mut mapper_lock = PAGE_TABLE.lock();
        let mapper = mapper_lock.as_mut().unwrap();
        let mapped = unsafe { mapper.map_to(page, hpet_phys_frame, flags, frame_allocator)? };

        mapped.flush();

        println!("hpet page offset: {offset}");

        let hpet_registers = unsafe { HpetRegisters::new(hpet_reg_virt_addr) };
        let mut hpet = Self {
            hpet_registers,
        };

        // assert!(hpet.hpet_registers.general_capabilities_and_id_reg.count_size_cap() == 1, "hpet clock does not support 64 bit mode, which is required");

        hpet.hpet_registers.general_conf_reg.set_enable_cnf(true);
        println!("enabled cnf of hpet");

        println!("hpet enable_cnf: {}", hpet.hpet_registers.general_conf_reg.enable_cnf());
        println!("hpet test val: {}", hpet.get_main_counter());

        HPET.lock().write(hpet);

        println!("initialized hpet, HPET static var can be used now");

        Ok(())
    }

    pub fn get_main_counter(&self) -> u64 {
        let ptr = self.hpet_registers.main_counter_value_reg as *const u64;
        unsafe { ptr.read_volatile() }
    }

    pub fn get_counter_clk_period(&self) -> u32 {
        self.hpet_registers.general_capabilities_and_id_reg.counter_clk_period()
    }
}

struct HpetRegisters {
    general_capabilities_and_id_reg: GeneralCapabilitiesAndIdReg,
    pub general_conf_reg: GeneralConfigurationReg,
    general_interrupt_status_reg: u64,
    main_counter_value_reg: u64,
    // rest of timers if needed
}

impl HpetRegisters {
    /// SAFETY: hpet_base_addr must be pointer to a struct containing hpet registers
    /// https://www.intel.com/content/dam/www/public/us/en/documents/technical-specifications/software-developers-hpet-spec-1-0a.pdf
    unsafe fn new(hpet_base_addr: VirtAddr) -> Self {
        let hpet_base_ptr = hpet_base_addr.as_ptr() as *const [u64; 128];
        let hpet_base = unsafe { &*hpet_base_ptr };

        println!("main_counter_value_reg: {:#X}", hpet_base[0 / 8]);

        Self {
            general_capabilities_and_id_reg: GeneralCapabilitiesAndIdReg(hpet_base_addr.as_u64()),
            general_conf_reg: GeneralConfigurationReg(hpet_base_addr.as_u64() + 0x10),
            general_interrupt_status_reg: hpet_base_addr.as_u64() + 0x20,
            // main_counter_value_reg: unsafe { &*(hpet_base[0xf0 / 8] as *const u64) },
            main_counter_value_reg: hpet_base_addr.as_u64() + 0xf0
        }
    }
}

struct GeneralConfigurationReg(u64);

impl GeneralConfigurationReg {
    fn get_val(&self) -> u64 {
        let ptr = self.0 as *const u64;
        unsafe { ptr.read_volatile() }
    }

    fn set_val(&mut self, val: u64) {
        let ptr = self.0 as *const u64 as *mut u64;
        unsafe { ptr.write_volatile(val) };
    }

    pub fn leg_rt_cnf(&self) -> bool {
        let val = self.get_val();
        unsafe { core::mem::transmute::<u8, bool>(((val & 0b10) >> 1) as u8) }
    }

    pub fn set_leg_rt_cnf(&mut self, val: bool) {
        without_interrupts(|| {
            let curval = self.get_val();
            let unset_val = curval & (!0b10);
            let setval = unset_val | ((val as u64) << 1);
            self.set_val(setval);
        });
    }

    pub fn enable_cnf(&self) -> bool {
        let val = self.get_val();
        unsafe { core::mem::transmute::<u8, bool>(((val & 0b1)) as u8) }
    }

    pub fn set_enable_cnf(&mut self, val: bool) {
        without_interrupts(|| {
            let curval = self.get_val();
            let unset_val = curval & (!0b1);
            let setval = unset_val | (val as u64);
            self.set_val(setval);
        });
    }
}

struct GeneralCapabilitiesAndIdReg(u64);

impl GeneralCapabilitiesAndIdReg {
    fn get_val(&self) -> u64 {
        let ptr = self.0 as *const u64;
        unsafe { ptr.read_volatile() }
    }

    pub fn rev_id(&self) -> u8 {
        self.get_val() as u8 & 0xff
    }

    pub fn num_tim_cap(&self) -> u8 {
        (self.get_val() >> 8) as u8 & 0b11111
    }

    /// 0 - 32bits, 1 - 64bits
    pub fn count_size_cap(&self) -> u8 {
        (self.get_val() >> 13) as u8 & 0b1
    }

    /// 1 - supported, 0 - unsupported
    pub fn leg_rt_cap(&self) -> u8 {
        (self.get_val() >> 15) as u8 & 0b1
    }

    pub fn vendor_id(&self) -> u16 {
        (self.get_val() >> 16) as u16
    }

    /// in femptoseconds
    pub fn counter_clk_period(&self) -> u32 {
        (self.get_val() >> 32) as u32
    }
}
