use alloc::boxed::Box;

pub mod fonts;
pub mod text_display;

pub trait Display {
    /// this method will transfer framebuffer to another display
    fn switch_display_to(self) -> Box<dyn Display>;
}
