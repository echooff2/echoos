use hashbrown::HashMap;

pub mod uni3_terminus16;

#[derive(Clone)]
pub struct Font {
    pub name: &'static str,
    pub width: u16,
    pub height: u16,
    pub glypths: &'static [Glypth],
    pub glypths_count: u16,
    pub unicode_map: HashMap<u32, u16>, // HashMap<unicode, code>
}

impl Font {
    pub const fn new(
        name: &'static str,
        width: u16,
        height: u16,
        glypths: &'static [Glypth],
        glypths_count: u16,
        unicode_map: HashMap<u32, u16>,
    ) -> Self {
        Self {
            name,
            width,
            height,
            glypths,
            glypths_count,
            unicode_map,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Glypth {
    pub code: u16,
    pub pixels: [[bool; 8]; 16],
}

impl Glypth {
    pub const fn new(code: u16, pixels: [[bool; 8]; 16]) -> Self {
        Self { code, pixels }
    }
}
