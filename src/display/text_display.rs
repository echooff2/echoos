use core::fmt::{self, Write};
use core::num::ParseIntError;

use alloc::vec;
use alloc::vec::Vec;
use alloc::{boxed::Box, string::String};
use bootloader::boot_info::{FrameBuffer, PixelFormat};
use core::ops::{Deref, DerefMut};
use spin::Mutex;
use x86_64::instructions::interrupts::without_interrupts;

use crate::serial_println;

use super::{fonts::Font, Display};

static TEXT_DISPLAY: Mutex<Option<TextDisplay>> = Mutex::new(None);

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Color {
    // independent of pixelformat
    WHITE = 0xffffff,
    BLACK = 0x000000,
    RED = 0xff0000,
    GREEN = 0x00ff00,
    BLUE = 0x0000ff,
}

impl Color {
    pub fn to_rgb_color(self) -> RGBColor {
        RGBColor::from_rgb_hex(self as u32)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct RGBColor(u32); // independent of pixelformat

impl RGBColor {
    pub fn from_rgb(r: u8, g: u8, b: u8) -> Self {
        RGBColor(((r as u32) << 16) + ((g as u32) << 8) + (b as u32))
    }

    pub fn from_rgb_hex(rgb: u32) -> Self {
        RGBColor(rgb)
    }

    pub fn from_rgb_str(str: &str) -> Result<Self, ParseIntError> {
        u32::from_str_radix(&str[1..], 16).map(RGBColor)
    }

    fn to_color_code(self, pixel_format: &PixelFormat) -> ColorCode {
        match pixel_format {
            PixelFormat::RGB => u32::reverse_bits(*self),
            PixelFormat::BGR => *self,
            PixelFormat::U8 => unimplemented!("u8 pixel format not supported"),
            _ => unimplemented!("only RGB and BGR is supported"),
        }
    }
}

impl Deref for RGBColor {
    type Target = u32;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for RGBColor {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

type ColorCode = u32; // dependent on pixelformat

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct Cursor {
    x: u32,
    y: u32,
}

impl Cursor {
    pub fn reset(&mut self) {
        self.x = 0;
        self.y = 0;
    }
}

pub struct TextDisplay {
    font: &'static Font,
    fg: ColorCode,
    bg: ColorCode,
    framebuffer: &'static mut FrameBuffer,
    write_read_buffer: Vec<u8>,
    cursor: Cursor,
    char_width: u32,  // max chars in one line
    char_height: u32, // max chars in one line
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TextDisplayWriteError {
    UnsupportedGlypth,
    UnsupportedUnicode,
}

impl TextDisplay {
    pub fn new(
        framebuffer: &'static mut FrameBuffer,
        fg: RGBColor,
        bg: RGBColor,
        font: &'static Font,
    ) -> Self {
        Self {
            char_width: (framebuffer.info().horizontal_resolution / font.width as usize) as u32,
            char_height: (framebuffer.info().vertical_resolution / font.height as usize) as u32,
            font,
            fg: fg.to_color_code(&framebuffer.info().pixel_format),
            bg: bg.to_color_code(&framebuffer.info().pixel_format),
            write_read_buffer: vec![0; framebuffer.info().byte_len],
            framebuffer,
            cursor: Cursor::default(),
        }
    }

    pub fn flash_on_ln(&mut self, chr: char) {
        if chr == '\n' {
            self.flush();
        }
    }

    pub fn flush(&mut self) {
        self.framebuffer
            .buffer_mut()
            .copy_from_slice(self.write_read_buffer.as_mut_slice());
    }

    /// after this call, to display text call self.flush();
    pub fn write_byte(&mut self, byte: u16) -> Result<(), TextDisplayWriteError> {
        if byte > self.font.glypths_count {
            return Err(TextDisplayWriteError::UnsupportedGlypth);
        }

        if self.cursor.x >= self.char_width {
            self.new_line();
        }

        let stride = self.framebuffer.info().stride;
        let bytes_per_pixel = self.framebuffer.info().bytes_per_pixel;
        let glypth_pixels = &self.font.glypths[byte as usize].pixels;

        let buffer = &mut self.write_read_buffer;
        for y in 0..self.font.height {
            for x in 0..self.font.width {
                let offset = (((self.cursor.y as usize * self.font.height as usize + y as usize)
                    * stride)
                    + (self.cursor.x as usize * self.font.width as usize + x as usize))
                    * bytes_per_pixel;

                // let buffer_offset = unsafe { buffer.as_mut_ptr().add(offset) };
                if glypth_pixels[y as usize][x as usize] {
                    // unsafe {
                    //     buffer_offset.write((self.fg & 0xff) as u8);
                    //     buffer_offset.add(1).write(((self.fg >> 8) & 0xff) as u8);
                    //     buffer_offset.add(2).write(((self.fg >> 16) & 0xff) as u8);
                    // }    maybe that is faster? idk. I know that it doesn't work???
                    buffer[offset] = (self.fg & 0xff) as u8;
                    buffer[offset + 1] = ((self.fg >> 8) & 0xff) as u8;
                    buffer[offset + 2] = ((self.fg >> 16) & 0xff) as u8;
                } else {
                    // unsafe {
                    //     buffer_offset.write((self.fg & 0xff) as u8);
                    //     buffer_offset.add(1).write(((self.fg >> 8) & 0xff) as u8);
                    //     buffer_offset.add(2).write(((self.fg >> 16) & 0xff) as u8);
                    // }
                    buffer[offset] = (self.bg & 0xff) as u8;
                    buffer[offset + 1] = ((self.bg >> 8) & 0xff) as u8;
                    buffer[offset + 2] = ((self.bg >> 16) & 0xff) as u8;
                }
            }
        }

        self.cursor.x += 1;

        Ok(())
    }

    pub fn new_line(&mut self) {
        self.cursor.x = 0;
        if self.cursor.y >= self.char_height - 1 {
            // copy one up
            let stride = self.framebuffer.info().stride;
            let bytes_per_pixel = self.framebuffer.info().bytes_per_pixel;
            let stride_bytes = stride * bytes_per_pixel;
            let char_row_bytes = stride_bytes * self.font.height as usize;
            unsafe {
                let write_read_buffer_ptr = self.write_read_buffer.as_mut_ptr();
                write_read_buffer_ptr.copy_from(
                    (write_read_buffer_ptr as *const u8).add(char_row_bytes),
                    self.framebuffer.info().byte_len - char_row_bytes,
                );
                write_read_buffer_ptr
                    .add(self.framebuffer.info().byte_len - char_row_bytes)
                    .write_bytes(0, char_row_bytes);
            }
        } else {
            self.cursor.y += 1;
        }
    }

    pub fn clear(&mut self) {
        let framebuffer_info = self.framebuffer.info();
        let bytes_per_pixel = framebuffer_info.bytes_per_pixel;
        let write_read_buffer_ptr = self.write_read_buffer.as_mut_ptr();
        for y in 0..framebuffer_info.vertical_resolution {
            for x in 0..framebuffer_info.horizontal_resolution {
                let pixel = (y * framebuffer_info.stride + x) * bytes_per_pixel;
                unsafe {
                    write_read_buffer_ptr
                        .add(pixel)
                        .write((self.bg & 0xff) as u8);
                    write_read_buffer_ptr
                        .add(pixel + 1)
                        .write(((self.bg >> 8) & 0xff) as u8);
                    write_read_buffer_ptr
                        .add(pixel + 2)
                        .write(((self.bg >> 16) & 0xff) as u8);
                }
            }
        }

        self.cursor.reset();
        self.flush();
    }

    fn write_char_not_flush(&mut self, c: char) -> fmt::Result {
        if c == '\n' {
            self.new_line();
            return Ok(());
        }

        let unicode = c as u32;

        let code = *self.font.unicode_map.get(&unicode).ok_or(fmt::Error)?;

        self.write_byte(code).map_err(|_| fmt::Error)?;

        Ok(())
    }

    pub fn set_fg_color(&mut self, color: RGBColor) {
        self.fg = color.to_color_code(&self.framebuffer.info().pixel_format);
    }

    pub fn set_bg_color(&mut self, color: RGBColor) {
        self.bg = color.to_color_code(&self.framebuffer.info().pixel_format);
    }

    pub fn get_framebuffer(&mut self) -> &mut FrameBuffer {
        self.framebuffer
    }
}

impl Display for TextDisplay {
    fn switch_display_to(self) -> Box<dyn Display> {
        unimplemented!()
    }
}

impl fmt::Write for TextDisplay {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for c in s.chars() {
            self.write_char(c)?;
        }

        // self.flush();

        Ok(())
    }

    fn write_char(&mut self, c: char) -> fmt::Result {
        self.write_char_not_flush(c)?;
        // self.flush();
        self.flash_on_ln(c);
        Ok(())
    }
}

pub fn init(
    framebuffer: &'static mut FrameBuffer,
    fg: RGBColor,
    bg: RGBColor,
    font: &'static Font,
) {
    without_interrupts(|| {
        let text_display = TextDisplay::new(framebuffer, fg, bg, font);
        *TEXT_DISPLAY.lock() = Some(text_display);
    });
}

pub fn clear() {
    without_interrupts(|| {
        TEXT_DISPLAY.lock().as_mut().unwrap().clear();
    });
}

pub unsafe fn get_text_display() -> &'static Mutex<Option<TextDisplay>> {
    &TEXT_DISPLAY
}

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::display::text_display::_print(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}

#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
    without_interrupts(|| {
        let error = TEXT_DISPLAY.lock().as_mut().unwrap().write_fmt(args);

        if let Err(err) = error {
            serial_println!("Error while writing to display: {:?}", err);
            panic!("Error while writing to display: {:?}", err);
        }
    });
}

// pub fn switch_display_to(self) {
// unimplemented!()
// }

#[cfg(test)]
mod tests {
    #[test_case]
    fn from_rgb_test() {
        let color = super::RGBColor::from_rgb(32, 22, 69);
        assert_eq!(color.0, 0x201645);
    }
}
