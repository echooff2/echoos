use hashbrown::HashMap;
use lazy_static::lazy_static;

use super::{Font, Glypth};

const WIDTH: u16 = 8;
const HEIGHT: u16 = 16;
const GLYPTH_COUNT: usize = 512;

static GLYPTHS: [Glypth; GLYPTH_COUNT] = {
    let glypth0: Glypth = Glypth::new(
        0,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [true, false, false, false, false, false, false, true],
            [true, false, false, true, true, false, false, true],
            [true, false, true, false, false, true, false, true],
            [true, false, true, false, false, false, false, true],
            [true, false, true, false, false, true, false, true],
            [true, false, false, true, true, false, false, true],
            [true, false, false, false, false, false, false, true],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth1: Glypth = Glypth::new(
        1,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [true, false, false, false, false, false, false, true],
            [true, false, true, true, true, false, false, true],
            [true, false, true, false, false, true, false, true],
            [true, false, true, true, true, false, false, true],
            [true, false, true, false, true, false, false, true],
            [true, false, true, false, false, true, false, true],
            [true, false, false, false, false, false, false, true],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth2: Glypth = Glypth::new(
        2,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, true],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, true, false],
            [false, true, false, false, true, false, true, false],
            [false, true, false, true, false, false, true, false],
            [false, true, true, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [true, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth3: Glypth = Glypth::new(
        3,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, true, true, true, false, true, true],
            [false, true, false, true, false, true, false, true],
            [false, true, false, true, false, true, false, true],
            [false, true, false, true, false, false, false, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth4: Glypth = Glypth::new(
        4,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, true, true, true, true, true, false, false],
            [true, true, true, true, true, true, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth5: Glypth = Glypth::new(
        5,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, false, true, true],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth6: Glypth = Glypth::new(
        6,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, false, true, true],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth7: Glypth = Glypth::new(
        7,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth8: Glypth = Glypth::new(
        8,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, true, true, true, true],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth9: Glypth = Glypth::new(
        9,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, false, true, true],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth10: Glypth = Glypth::new(
        10,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [true, true, true, true, true, true, true, true],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth11: Glypth = Glypth::new(
        11,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, false, true, true],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth12: Glypth = Glypth::new(
        12,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth13: Glypth = Glypth::new(
        13,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, true, true, true, true, true, true, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth14: Glypth = Glypth::new(
        14,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth15: Glypth = Glypth::new(
        15,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, true, true, true, true, true, true, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth16: Glypth = Glypth::new(
        16,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, true, false, true, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, true, false, true, false, true, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth17: Glypth = Glypth::new(
        17,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, true, false, true, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, true, false, true, false, true, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth18: Glypth = Glypth::new(
        18,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, true, false, false, false, false],
            [false, true, true, false, false, false, false, false],
            [false, true, true, false, false, false, false, false],
            [false, true, false, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth19: Glypth = Glypth::new(
        19,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, true, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth20: Glypth = Glypth::new(
        20,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, true, true, true, false, false, true, false],
            [false, false, false, true, false, false, true, false],
            [false, false, false, true, false, false, true, false],
            [false, false, false, true, false, false, true, false],
            [false, false, false, true, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth21: Glypth = Glypth::new(
        21,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth22: Glypth = Glypth::new(
        22,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth23: Glypth = Glypth::new(
        23,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth24: Glypth = Glypth::new(
        24,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, true, false, true, false, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth25: Glypth = Glypth::new(
        25,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, false, true, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth26: Glypth = Glypth::new(
        26,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, true, false, false],
            [true, true, true, true, true, true, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth27: Glypth = Glypth::new(
        27,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [true, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth28: Glypth = Glypth::new(
        28,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, true, true, true],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth29: Glypth = Glypth::new(
        29,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, true, true, true],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth30: Glypth = Glypth::new(
        30,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth31: Glypth = Glypth::new(
        31,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth32: Glypth = Glypth::new(
        32,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth33: Glypth = Glypth::new(
        33,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth34: Glypth = Glypth::new(
        34,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth35: Glypth = Glypth::new(
        35,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth36: Glypth = Glypth::new(
        36,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, true, false],
            [false, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth37: Glypth = Glypth::new(
        37,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, false, false, true, false, false],
            [true, false, false, true, false, true, false, false],
            [false, true, true, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, true, true, false, false],
            [false, true, false, true, false, false, true, false],
            [false, true, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth38: Glypth = Glypth::new(
        38,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, true, false, false, true, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, true, true, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth39: Glypth = Glypth::new(
        39,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth40: Glypth = Glypth::new(
        40,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth41: Glypth = Glypth::new(
        41,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth42: Glypth = Glypth::new(
        42,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth43: Glypth = Glypth::new(
        43,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth44: Glypth = Glypth::new(
        44,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth45: Glypth = Glypth::new(
        45,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth46: Glypth = Glypth::new(
        46,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth47: Glypth = Glypth::new(
        47,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth48: Glypth = Glypth::new(
        48,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, true, false],
            [false, true, false, false, true, false, true, false],
            [false, true, false, true, false, false, true, false],
            [false, true, true, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth49: Glypth = Glypth::new(
        49,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth50: Glypth = Glypth::new(
        50,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth51: Glypth = Glypth::new(
        51,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth52: Glypth = Glypth::new(
        52,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, true, true, false],
            [false, false, false, false, true, false, true, false],
            [false, false, false, true, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth53: Glypth = Glypth::new(
        53,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth54: Glypth = Glypth::new(
        54,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, true, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth55: Glypth = Glypth::new(
        55,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth56: Glypth = Glypth::new(
        56,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth57: Glypth = Glypth::new(
        57,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth58: Glypth = Glypth::new(
        58,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth59: Glypth = Glypth::new(
        59,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth60: Glypth = Glypth::new(
        60,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth61: Glypth = Glypth::new(
        61,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth62: Glypth = Glypth::new(
        62,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth63: Glypth = Glypth::new(
        63,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth64: Glypth = Glypth::new(
        64,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, true, true, true, true, false],
            [true, false, true, false, false, false, true, false],
            [true, false, true, false, false, false, true, false],
            [true, false, true, false, false, false, true, false],
            [true, false, true, false, false, true, true, false],
            [true, false, false, true, true, false, true, false],
            [true, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth65: Glypth = Glypth::new(
        65,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth66: Glypth = Glypth::new(
        66,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth67: Glypth = Glypth::new(
        67,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth68: Glypth = Glypth::new(
        68,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth69: Glypth = Glypth::new(
        69,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth70: Glypth = Glypth::new(
        70,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth71: Glypth = Glypth::new(
        71,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth72: Glypth = Glypth::new(
        72,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth73: Glypth = Glypth::new(
        73,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth74: Glypth = Glypth::new(
        74,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, true, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth75: Glypth = Glypth::new(
        75,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, true, false, false, false, false],
            [false, true, true, false, false, false, false, false],
            [false, true, true, false, false, false, false, false],
            [false, true, false, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth76: Glypth = Glypth::new(
        76,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth77: Glypth = Glypth::new(
        77,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, true, false, false, false, true, true, false],
            [true, false, true, false, true, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth78: Glypth = Glypth::new(
        78,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, false, false, false, true, false],
            [false, true, false, true, false, false, true, false],
            [false, true, false, false, true, false, true, false],
            [false, true, false, false, false, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth79: Glypth = Glypth::new(
        79,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth80: Glypth = Glypth::new(
        80,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth81: Glypth = Glypth::new(
        81,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, true, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth82: Glypth = Glypth::new(
        82,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth83: Glypth = Glypth::new(
        83,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth84: Glypth = Glypth::new(
        84,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, true, true, true, true, true, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth85: Glypth = Glypth::new(
        85,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth86: Glypth = Glypth::new(
        86,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth87: Glypth = Glypth::new(
        87,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, true, false, true, false, true, false],
            [true, true, false, false, false, true, true, false],
            [true, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth88: Glypth = Glypth::new(
        88,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth89: Glypth = Glypth::new(
        89,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth90: Glypth = Glypth::new(
        90,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth91: Glypth = Glypth::new(
        91,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth92: Glypth = Glypth::new(
        92,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth93: Glypth = Glypth::new(
        93,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth94: Glypth = Glypth::new(
        94,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth95: Glypth = Glypth::new(
        95,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth96: Glypth = Glypth::new(
        96,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth97: Glypth = Glypth::new(
        97,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth98: Glypth = Glypth::new(
        98,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth99: Glypth = Glypth::new(
        99,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth100: Glypth = Glypth::new(
        100,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth101: Glypth = Glypth::new(
        101,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth102: Glypth = Glypth::new(
        102,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, true, true, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth103: Glypth = Glypth::new(
        103,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth104: Glypth = Glypth::new(
        104,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth105: Glypth = Glypth::new(
        105,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth106: Glypth = Glypth::new(
        106,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth107: Glypth = Glypth::new(
        107,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, true, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth108: Glypth = Glypth::new(
        108,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth109: Glypth = Glypth::new(
        109,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, true, true, true, true, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth110: Glypth = Glypth::new(
        110,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth111: Glypth = Glypth::new(
        111,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth112: Glypth = Glypth::new(
        112,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth113: Glypth = Glypth::new(
        113,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth114: Glypth = Glypth::new(
        114,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, true, true, true, true, false],
            [false, true, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth115: Glypth = Glypth::new(
        115,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth116: Glypth = Glypth::new(
        116,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth117: Glypth = Glypth::new(
        117,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth118: Glypth = Glypth::new(
        118,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth119: Glypth = Glypth::new(
        119,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth120: Glypth = Glypth::new(
        120,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth121: Glypth = Glypth::new(
        121,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth122: Glypth = Glypth::new(
        122,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth123: Glypth = Glypth::new(
        123,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth124: Glypth = Glypth::new(
        124,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth125: Glypth = Glypth::new(
        125,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth126: Glypth = Glypth::new(
        126,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, false, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth127: Glypth = Glypth::new(
        127,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth128: Glypth = Glypth::new(
        128,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth129: Glypth = Glypth::new(
        129,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth130: Glypth = Glypth::new(
        130,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth131: Glypth = Glypth::new(
        131,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth132: Glypth = Glypth::new(
        132,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth133: Glypth = Glypth::new(
        133,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth134: Glypth = Glypth::new(
        134,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth135: Glypth = Glypth::new(
        135,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth136: Glypth = Glypth::new(
        136,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth137: Glypth = Glypth::new(
        137,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth138: Glypth = Glypth::new(
        138,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth139: Glypth = Glypth::new(
        139,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth140: Glypth = Glypth::new(
        140,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth141: Glypth = Glypth::new(
        141,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth142: Glypth = Glypth::new(
        142,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth143: Glypth = Glypth::new(
        143,
        [
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth144: Glypth = Glypth::new(
        144,
        [
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth145: Glypth = Glypth::new(
        145,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, false, true, true, false, false],
            [false, false, false, true, false, false, true, false],
            [false, true, true, true, false, false, true, false],
            [true, false, false, true, true, true, true, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [false, true, true, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth146: Glypth = Glypth::new(
        146,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, true, true, true, true, true, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth147: Glypth = Glypth::new(
        147,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth148: Glypth = Glypth::new(
        148,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth149: Glypth = Glypth::new(
        149,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth150: Glypth = Glypth::new(
        150,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth151: Glypth = Glypth::new(
        151,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth152: Glypth = Glypth::new(
        152,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth153: Glypth = Glypth::new(
        153,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth154: Glypth = Glypth::new(
        154,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth155: Glypth = Glypth::new(
        155,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth156: Glypth = Glypth::new(
        156,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth157: Glypth = Glypth::new(
        157,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth158: Glypth = Glypth::new(
        158,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth159: Glypth = Glypth::new(
        159,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, true, false, false],
            [false, false, false, true, false, false, true, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [false, true, true, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth160: Glypth = Glypth::new(
        160,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth161: Glypth = Glypth::new(
        161,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth162: Glypth = Glypth::new(
        162,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth163: Glypth = Glypth::new(
        163,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth164: Glypth = Glypth::new(
        164,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, true, false],
            [false, true, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth165: Glypth = Glypth::new(
        165,
        [
            [false, false, true, true, false, false, true, false],
            [false, true, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, false, false, false, true, false],
            [false, true, false, true, false, false, true, false],
            [false, true, false, false, true, false, true, false],
            [false, true, false, false, false, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth166: Glypth = Glypth::new(
        166,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth167: Glypth = Glypth::new(
        167,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth168: Glypth = Glypth::new(
        168,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth169: Glypth = Glypth::new(
        169,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, true, true, false, false, false, false],
            [true, false, false, false, true, false, false, false],
            [true, false, false, false, true, false, false, false],
            [true, false, false, false, true, false, false, false],
            [true, true, true, true, false, true, false, false],
            [true, false, false, false, false, true, false, false],
            [true, false, false, false, true, true, true, false],
            [true, false, false, false, false, true, false, false],
            [true, false, false, false, false, true, false, false],
            [true, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth170: Glypth = Glypth::new(
        170,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth171: Glypth = Glypth::new(
        171,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, true, true, false, false],
            [true, false, false, true, false, false, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth172: Glypth = Glypth::new(
        172,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, true, false],
            [false, true, false, false, false, true, true, false],
            [true, false, false, false, true, false, true, false],
            [false, false, false, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth173: Glypth = Glypth::new(
        173,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth174: Glypth = Glypth::new(
        174,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, true, false, false, true, false, false, false],
            [true, false, false, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth175: Glypth = Glypth::new(
        175,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, true, false, false, true, false, false, false],
            [true, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth176: Glypth = Glypth::new(
        176,
        [
            [true, false, false, false, true, false, false, false],
            [false, false, true, false, false, false, true, false],
            [true, false, false, false, true, false, false, false],
            [false, false, true, false, false, false, true, false],
            [true, false, false, false, true, false, false, false],
            [false, false, true, false, false, false, true, false],
            [true, false, false, false, true, false, false, false],
            [false, false, true, false, false, false, true, false],
            [true, false, false, false, true, false, false, false],
            [false, false, true, false, false, false, true, false],
            [true, false, false, false, true, false, false, false],
            [false, false, true, false, false, false, true, false],
            [true, false, false, false, true, false, false, false],
            [false, false, true, false, false, false, true, false],
            [true, false, false, false, true, false, false, false],
            [false, false, true, false, false, false, true, false],
        ],
    );
    let glypth177: Glypth = Glypth::new(
        177,
        [
            [true, false, true, false, true, false, true, false],
            [false, true, false, true, false, true, false, true],
            [true, false, true, false, true, false, true, false],
            [false, true, false, true, false, true, false, true],
            [true, false, true, false, true, false, true, false],
            [false, true, false, true, false, true, false, true],
            [true, false, true, false, true, false, true, false],
            [false, true, false, true, false, true, false, true],
            [true, false, true, false, true, false, true, false],
            [false, true, false, true, false, true, false, true],
            [true, false, true, false, true, false, true, false],
            [false, true, false, true, false, true, false, true],
            [true, false, true, false, true, false, true, false],
            [false, true, false, true, false, true, false, true],
            [true, false, true, false, true, false, true, false],
            [false, true, false, true, false, true, false, true],
        ],
    );
    let glypth178: Glypth = Glypth::new(
        178,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth179: Glypth = Glypth::new(
        179,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
        ],
    );
    let glypth180: Glypth = Glypth::new(
        180,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [true, true, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
        ],
    );
    let glypth181: Glypth = Glypth::new(
        181,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth182: Glypth = Glypth::new(
        182,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [true, true, true, true, true, true, true, true],
            [false, true, false, false, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth183: Glypth = Glypth::new(
        183,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, true, false, true, false, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, false, true, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth184: Glypth = Glypth::new(
        184,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, true, false, true, false, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, false, true, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth185: Glypth = Glypth::new(
        185,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, true, true, true, true, true, true, false],
            [true, true, false, false, false, false, true, true],
            [false, true, true, true, true, true, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth186: Glypth = Glypth::new(
        186,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth187: Glypth = Glypth::new(
        187,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, true, false, true, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, true, true, true, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth188: Glypth = Glypth::new(
        188,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [true, true, true, true, true, true, true, false],
            [true, true, false, true, false, true, true, false],
            [true, true, true, true, true, true, true, false],
            [true, true, true, true, true, true, true, false],
            [true, true, false, false, false, true, true, false],
            [true, true, true, false, true, true, true, false],
            [true, true, true, true, true, true, true, false],
            [true, true, true, true, true, true, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth189: Glypth = Glypth::new(
        189,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, true, false],
            [false, true, false, true, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [true, true, true, false, true, true, true, false],
            [false, false, true, true, true, false, false, false],
            [false, true, false, true, false, true, false, false],
            [true, false, false, true, false, false, true, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth190: Glypth = Glypth::new(
        190,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth191: Glypth = Glypth::new(
        191,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
        ],
    );
    let glypth192: Glypth = Glypth::new(
        192,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, true, true, true, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth193: Glypth = Glypth::new(
        193,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [true, true, true, true, true, true, true, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth194: Glypth = Glypth::new(
        194,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, true, true, true, true, true, true],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
        ],
    );
    let glypth195: Glypth = Glypth::new(
        195,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, true, true, true, true],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
        ],
    );
    let glypth196: Glypth = Glypth::new(
        196,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, true, true, true, true, true, true],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth197: Glypth = Glypth::new(
        197,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [true, true, true, true, true, true, true, true],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
        ],
    );
    let glypth198: Glypth = Glypth::new(
        198,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, true, true, false],
            [false, false, false, false, false, true, true, false],
            [false, false, false, false, true, false, true, false],
            [false, false, false, true, false, false, true, false],
            [false, false, true, true, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth199: Glypth = Glypth::new(
        199,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, true, true, true, true, true, false, false],
            [true, true, true, true, true, true, true, false],
            [true, true, true, true, true, true, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth200: Glypth = Glypth::new(
        200,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, false, true, false, true, false, false],
            [true, true, true, true, true, true, true, false],
            [true, true, true, true, true, true, true, false],
            [false, true, false, true, false, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth201: Glypth = Glypth::new(
        201,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth202: Glypth = Glypth::new(
        202,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth203: Glypth = Glypth::new(
        203,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth204: Glypth = Glypth::new(
        204,
        [
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth205: Glypth = Glypth::new(
        205,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth206: Glypth = Glypth::new(
        206,
        [
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth207: Glypth = Glypth::new(
        207,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth208: Glypth = Glypth::new(
        208,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth209: Glypth = Glypth::new(
        209,
        [
            [false, false, false, false, false, false, false, false],
            [true, true, true, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, false, false, false, false, false],
            [false, false, false, true, false, false, true, false],
            [true, true, true, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, true, false],
            [false, true, false, false, false, true, true, false],
            [true, false, false, false, true, false, true, false],
            [false, false, false, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth210: Glypth = Glypth::new(
        210,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth211: Glypth = Glypth::new(
        211,
        [
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth212: Glypth = Glypth::new(
        212,
        [
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth213: Glypth = Glypth::new(
        213,
        [
            [false, false, true, true, false, false, true, false],
            [false, true, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth214: Glypth = Glypth::new(
        214,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth215: Glypth = Glypth::new(
        215,
        [
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth216: Glypth = Glypth::new(
        216,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth217: Glypth = Glypth::new(
        217,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [true, true, true, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth218: Glypth = Glypth::new(
        218,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, true, true, true],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
        ],
    );
    let glypth219: Glypth = Glypth::new(
        219,
        [
            [true, true, true, true, true, true, true, true],
            [true, true, true, true, true, true, true, true],
            [true, true, true, true, true, true, true, true],
            [true, true, true, true, true, true, true, true],
            [true, true, true, true, true, true, true, true],
            [true, true, true, true, true, true, true, true],
            [true, true, true, true, true, true, true, true],
            [true, true, true, true, true, true, true, true],
            [true, true, true, true, true, true, true, true],
            [true, true, true, true, true, true, true, true],
            [true, true, true, true, true, true, true, true],
            [true, true, true, true, true, true, true, true],
            [true, true, true, true, true, true, true, true],
            [true, true, true, true, true, true, true, true],
            [true, true, true, true, true, true, true, true],
            [true, true, true, true, true, true, true, true],
        ],
    );
    let glypth220: Glypth = Glypth::new(
        220,
        [
            [false, false, true, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth221: Glypth = Glypth::new(
        221,
        [
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth222: Glypth = Glypth::new(
        222,
        [
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth223: Glypth = Glypth::new(
        223,
        [
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth224: Glypth = Glypth::new(
        224,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [true, true, true, true, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth225: Glypth = Glypth::new(
        225,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, false, false, false, true, false],
            [false, true, false, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth226: Glypth = Glypth::new(
        226,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth227: Glypth = Glypth::new(
        227,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth228: Glypth = Glypth::new(
        228,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth229: Glypth = Glypth::new(
        229,
        [
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth230: Glypth = Glypth::new(
        230,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, true, false],
            [false, true, true, true, true, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth231: Glypth = Glypth::new(
        231,
        [
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth232: Glypth = Glypth::new(
        232,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth233: Glypth = Glypth::new(
        233,
        [
            [false, false, true, true, false, false, true, false],
            [false, true, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth234: Glypth = Glypth::new(
        234,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, true, true, false, false, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth235: Glypth = Glypth::new(
        235,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth236: Glypth = Glypth::new(
        236,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth237: Glypth = Glypth::new(
        237,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth238: Glypth = Glypth::new(
        238,
        [
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth239: Glypth = Glypth::new(
        239,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth240: Glypth = Glypth::new(
        240,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth241: Glypth = Glypth::new(
        241,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth242: Glypth = Glypth::new(
        242,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth243: Glypth = Glypth::new(
        243,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth244: Glypth = Glypth::new(
        244,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, true, false, false],
            [false, false, false, true, false, false, true, false],
            [false, false, false, true, false, false, true, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
        ],
    );
    let glypth245: Glypth = Glypth::new(
        245,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [false, true, true, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth246: Glypth = Glypth::new(
        246,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth247: Glypth = Glypth::new(
        247,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, true, false],
            [false, true, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, true, false],
            [false, true, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth248: Glypth = Glypth::new(
        248,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth249: Glypth = Glypth::new(
        249,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth250: Glypth = Glypth::new(
        250,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth251: Glypth = Glypth::new(
        251,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, true, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, false, true, false, false],
            [false, false, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth252: Glypth = Glypth::new(
        252,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth253: Glypth = Glypth::new(
        253,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth254: Glypth = Glypth::new(
        254,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth255: Glypth = Glypth::new(
        255,
        [
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth256: Glypth = Glypth::new(
        256,
        [
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth257: Glypth = Glypth::new(
        257,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth258: Glypth = Glypth::new(
        258,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, true, false],
            [false, true, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth259: Glypth = Glypth::new(
        259,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth260: Glypth = Glypth::new(
        260,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, true, false],
            [false, true, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth261: Glypth = Glypth::new(
        261,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, true, true, false],
            [false, true, false, false, true, false, true, false],
            [false, true, false, true, false, false, true, false],
            [false, true, true, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [true, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth262: Glypth = Glypth::new(
        262,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth263: Glypth = Glypth::new(
        263,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth264: Glypth = Glypth::new(
        264,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth265: Glypth = Glypth::new(
        265,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth266: Glypth = Glypth::new(
        266,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth267: Glypth = Glypth::new(
        267,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth268: Glypth = Glypth::new(
        268,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, true, true, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth269: Glypth = Glypth::new(
        269,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, true, true, true, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth270: Glypth = Glypth::new(
        270,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth271: Glypth = Glypth::new(
        271,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth272: Glypth = Glypth::new(
        272,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth273: Glypth = Glypth::new(
        273,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth274: Glypth = Glypth::new(
        274,
        [
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth275: Glypth = Glypth::new(
        275,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth276: Glypth = Glypth::new(
        276,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth277: Glypth = Glypth::new(
        277,
        [
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth278: Glypth = Glypth::new(
        278,
        [
            [false, false, true, true, false, false, true, false],
            [false, true, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth279: Glypth = Glypth::new(
        279,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth280: Glypth = Glypth::new(
        280,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth281: Glypth = Glypth::new(
        281,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth282: Glypth = Glypth::new(
        282,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth283: Glypth = Glypth::new(
        283,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth284: Glypth = Glypth::new(
        284,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, true, false],
            [false, false, false, true, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth285: Glypth = Glypth::new(
        285,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, true, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth286: Glypth = Glypth::new(
        286,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth287: Glypth = Glypth::new(
        287,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth288: Glypth = Glypth::new(
        288,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth289: Glypth = Glypth::new(
        289,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [true, false, true, false, true, false, false, false],
            [false, true, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, true, false, true, false, false],
            [true, false, true, false, true, false, true, false],
            [true, false, false, true, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth290: Glypth = Glypth::new(
        290,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth291: Glypth = Glypth::new(
        291,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth292: Glypth = Glypth::new(
        292,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, true, false, false],
            [false, false, true, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [true, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [true, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, false, false, false, true, false],
            [false, false, false, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth293: Glypth = Glypth::new(
        293,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth294: Glypth = Glypth::new(
        294,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth295: Glypth = Glypth::new(
        295,
        [
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth296: Glypth = Glypth::new(
        296,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth297: Glypth = Glypth::new(
        297,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth298: Glypth = Glypth::new(
        298,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth299: Glypth = Glypth::new(
        299,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth300: Glypth = Glypth::new(
        300,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth301: Glypth = Glypth::new(
        301,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth302: Glypth = Glypth::new(
        302,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth303: Glypth = Glypth::new(
        303,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth304: Glypth = Glypth::new(
        304,
        [
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth305: Glypth = Glypth::new(
        305,
        [
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth306: Glypth = Glypth::new(
        306,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth307: Glypth = Glypth::new(
        307,
        [
            [false, true, false, false, true, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth308: Glypth = Glypth::new(
        308,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, false, false, false, false, false],
            [true, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth309: Glypth = Glypth::new(
        309,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth310: Glypth = Glypth::new(
        310,
        [
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, false, false, false, true, false],
            [false, true, false, true, false, false, true, false],
            [false, true, false, false, true, false, true, false],
            [false, true, false, false, false, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth311: Glypth = Glypth::new(
        311,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth312: Glypth = Glypth::new(
        312,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, false, false, false, true, false],
            [false, true, false, true, false, false, true, false],
            [false, true, false, false, true, false, true, false],
            [false, true, false, false, false, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth313: Glypth = Glypth::new(
        313,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth314: Glypth = Glypth::new(
        314,
        [
            [false, false, false, true, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth315: Glypth = Glypth::new(
        315,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth316: Glypth = Glypth::new(
        316,
        [
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth317: Glypth = Glypth::new(
        317,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, true, true, true, true, false],
            [false, true, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth318: Glypth = Glypth::new(
        318,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth319: Glypth = Glypth::new(
        319,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, true, true, true, true, false],
            [false, true, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth320: Glypth = Glypth::new(
        320,
        [
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth321: Glypth = Glypth::new(
        321,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth322: Glypth = Glypth::new(
        322,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, true, true, true, true, true, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth323: Glypth = Glypth::new(
        323,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, true, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth324: Glypth = Glypth::new(
        324,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, true, true, true, true, true, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth325: Glypth = Glypth::new(
        325,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth326: Glypth = Glypth::new(
        326,
        [
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth327: Glypth = Glypth::new(
        327,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth328: Glypth = Glypth::new(
        328,
        [
            [false, false, false, true, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth329: Glypth = Glypth::new(
        329,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth330: Glypth = Glypth::new(
        330,
        [
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth331: Glypth = Glypth::new(
        331,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth332: Glypth = Glypth::new(
        332,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth333: Glypth = Glypth::new(
        333,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth334: Glypth = Glypth::new(
        334,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
        ],
    );
    let glypth335: Glypth = Glypth::new(
        335,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
        ],
    );
    let glypth336: Glypth = Glypth::new(
        336,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, true, true, true, true, true, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
        ],
    );
    let glypth337: Glypth = Glypth::new(
        337,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
        ],
    );
    let glypth338: Glypth = Glypth::new(
        338,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth339: Glypth = Glypth::new(
        339,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth340: Glypth = Glypth::new(
        340,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth341: Glypth = Glypth::new(
        341,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth342: Glypth = Glypth::new(
        342,
        [
            [false, false, false, true, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth343: Glypth = Glypth::new(
        343,
        [
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth344: Glypth = Glypth::new(
        344,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth345: Glypth = Glypth::new(
        345,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth346: Glypth = Glypth::new(
        346,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth347: Glypth = Glypth::new(
        347,
        [
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth348: Glypth = Glypth::new(
        348,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth349: Glypth = Glypth::new(
        349,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth350: Glypth = Glypth::new(
        350,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth351: Glypth = Glypth::new(
        351,
        [
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth352: Glypth = Glypth::new(
        352,
        [
            [false, false, false, false, true, true, false, false],
            [false, false, false, true, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth353: Glypth = Glypth::new(
        353,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [true, true, true, true, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth354: Glypth = Glypth::new(
        354,
        [
            [false, false, false, false, true, true, false, false],
            [false, false, false, true, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, true, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth355: Glypth = Glypth::new(
        355,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, true, false, false],
            [false, false, false, true, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth356: Glypth = Glypth::new(
        356,
        [
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth357: Glypth = Glypth::new(
        357,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth358: Glypth = Glypth::new(
        358,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth359: Glypth = Glypth::new(
        359,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth360: Glypth = Glypth::new(
        360,
        [
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, true, false, true, false, true, false],
            [true, true, false, false, false, true, true, false],
            [true, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth361: Glypth = Glypth::new(
        361,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth362: Glypth = Glypth::new(
        362,
        [
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth363: Glypth = Glypth::new(
        363,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth364: Glypth = Glypth::new(
        364,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, true, false, false, false, true, true, false],
            [true, false, true, false, true, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth365: Glypth = Glypth::new(
        365,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, true, true, true, true, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth366: Glypth = Glypth::new(
        366,
        [
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth367: Glypth = Glypth::new(
        367,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth368: Glypth = Glypth::new(
        368,
        [
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth369: Glypth = Glypth::new(
        369,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth370: Glypth = Glypth::new(
        370,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth371: Glypth = Glypth::new(
        371,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth372: Glypth = Glypth::new(
        372,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
        ],
    );
    let glypth373: Glypth = Glypth::new(
        373,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth374: Glypth = Glypth::new(
        374,
        [
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth375: Glypth = Glypth::new(
        375,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth376: Glypth = Glypth::new(
        376,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth377: Glypth = Glypth::new(
        377,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth378: Glypth = Glypth::new(
        378,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, true, false, false, false, false],
            [false, true, true, false, false, false, false, false],
            [false, true, true, false, false, false, false, false],
            [false, true, false, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
        ],
    );
    let glypth379: Glypth = Glypth::new(
        379,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, true, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
        ],
    );
    let glypth380: Glypth = Glypth::new(
        380,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
        ],
    );
    let glypth381: Glypth = Glypth::new(
        381,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
        ],
    );
    let glypth382: Glypth = Glypth::new(
        382,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, false, false, false, true, false],
            [false, true, false, true, false, false, true, false],
            [false, true, false, false, true, false, true, false],
            [false, true, false, false, false, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
        ],
    );
    let glypth383: Glypth = Glypth::new(
        383,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
        ],
    );
    let glypth384: Glypth = Glypth::new(
        384,
        [
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth385: Glypth = Glypth::new(
        385,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth386: Glypth = Glypth::new(
        386,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, false, false, false, false],
        ],
    );
    let glypth387: Glypth = Glypth::new(
        387,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, true, true, true, true, false],
            [false, true, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [true, false, false, false, false, false, false, false],
        ],
    );
    let glypth388: Glypth = Glypth::new(
        388,
        [
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth389: Glypth = Glypth::new(
        389,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth390: Glypth = Glypth::new(
        390,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth391: Glypth = Glypth::new(
        391,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth392: Glypth = Glypth::new(
        392,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, true, true, true, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth393: Glypth = Glypth::new(
        393,
        [
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth394: Glypth = Glypth::new(
        394,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth395: Glypth = Glypth::new(
        395,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, false, false, false, false],
            [false, true, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, true, true, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth396: Glypth = Glypth::new(
        396,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, true, true, false, false],
            [true, true, true, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth397: Glypth = Glypth::new(
        397,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, true, true, true, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth398: Glypth = Glypth::new(
        398,
        [
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, true, false, false, false, false],
            [false, true, true, false, false, false, false, false],
            [false, true, true, false, false, false, false, false],
            [false, true, false, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth399: Glypth = Glypth::new(
        399,
        [
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, true, false],
            [false, true, false, false, true, false, true, false],
            [false, true, false, true, false, false, true, false],
            [false, true, true, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth400: Glypth = Glypth::new(
        400,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth401: Glypth = Glypth::new(
        401,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth402: Glypth = Glypth::new(
        402,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth403: Glypth = Glypth::new(
        403,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [true, true, true, true, true, true, true, false],
            [true, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth404: Glypth = Glypth::new(
        404,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, true, false, true, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, true, false, true, false, true, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth405: Glypth = Glypth::new(
        405,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth406: Glypth = Glypth::new(
        406,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, true, false],
            [false, true, false, false, true, false, true, false],
            [false, true, false, true, false, false, true, false],
            [false, true, true, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth407: Glypth = Glypth::new(
        407,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, true, false],
            [false, true, false, false, true, false, true, false],
            [false, true, false, true, false, false, true, false],
            [false, true, true, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth408: Glypth = Glypth::new(
        408,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, true, true, false],
            [false, false, false, true, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth409: Glypth = Glypth::new(
        409,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth410: Glypth = Glypth::new(
        410,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth411: Glypth = Glypth::new(
        411,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth412: Glypth = Glypth::new(
        412,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth413: Glypth = Glypth::new(
        413,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth414: Glypth = Glypth::new(
        414,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, true, true, true, false, false, true, false],
            [true, false, false, false, true, false, true, false],
            [true, false, false, false, true, false, true, false],
            [true, false, false, false, true, false, true, false],
            [true, false, false, false, true, false, true, false],
            [true, false, false, false, true, false, true, false],
            [true, true, true, true, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth415: Glypth = Glypth::new(
        415,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth416: Glypth = Glypth::new(
        416,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth417: Glypth = Glypth::new(
        417,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, true, true, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, true, true, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth418: Glypth = Glypth::new(
        418,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, true, false, true, false],
            [false, false, false, true, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth419: Glypth = Glypth::new(
        419,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth420: Glypth = Glypth::new(
        420,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth421: Glypth = Glypth::new(
        421,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth422: Glypth = Glypth::new(
        422,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [true, true, true, true, true, true, true, false],
            [true, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth423: Glypth = Glypth::new(
        423,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, true, false, true, false, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, true, false, true, false, true, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth424: Glypth = Glypth::new(
        424,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth425: Glypth = Glypth::new(
        425,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth426: Glypth = Glypth::new(
        426,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth427: Glypth = Glypth::new(
        427,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, true, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth428: Glypth = Glypth::new(
        428,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, true, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth429: Glypth = Glypth::new(
        429,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, true, false, false, false, true, true, false],
            [true, false, true, false, true, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth430: Glypth = Glypth::new(
        430,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth431: Glypth = Glypth::new(
        431,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth432: Glypth = Glypth::new(
        432,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, true, true, true, true, true, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth433: Glypth = Glypth::new(
        433,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth434: Glypth = Glypth::new(
        434,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth435: Glypth = Glypth::new(
        435,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth436: Glypth = Glypth::new(
        436,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth437: Glypth = Glypth::new(
        437,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, true, true, true, false, false, true, false],
            [true, false, false, false, true, false, true, false],
            [true, false, false, false, true, false, true, false],
            [true, false, false, false, true, false, true, false],
            [true, true, true, true, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth438: Glypth = Glypth::new(
        438,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, true, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth439: Glypth = Glypth::new(
        439,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth440: Glypth = Glypth::new(
        440,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, true, true, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, true, true, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth441: Glypth = Glypth::new(
        441,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, true, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth442: Glypth = Glypth::new(
        442,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth443: Glypth = Glypth::new(
        443,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [true, true, true, true, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, true, true, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth444: Glypth = Glypth::new(
        444,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth445: Glypth = Glypth::new(
        445,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth446: Glypth = Glypth::new(
        446,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, true, true, false, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth447: Glypth = Glypth::new(
        447,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, true, true, false, false],
            [true, true, true, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth448: Glypth = Glypth::new(
        448,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [true, true, true, true, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth449: Glypth = Glypth::new(
        449,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, true, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth450: Glypth = Glypth::new(
        450,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth451: Glypth = Glypth::new(
        451,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth452: Glypth = Glypth::new(
        452,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth453: Glypth = Glypth::new(
        453,
        [
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth454: Glypth = Glypth::new(
        454,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth455: Glypth = Glypth::new(
        455,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, true, false, true, true, false],
            [true, false, false, true, false, true, true, false],
            [true, false, false, true, false, true, true, false],
            [true, true, false, true, false, false, false, false],
            [true, true, true, true, false, false, false, false],
            [true, true, true, true, false, false, false, false],
            [true, false, true, true, false, false, false, false],
            [true, false, false, true, false, true, true, false],
            [true, false, false, true, false, false, false, false],
            [true, false, false, true, false, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth456: Glypth = Glypth::new(
        456,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [true, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth457: Glypth = Glypth::new(
        457,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [true, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth458: Glypth = Glypth::new(
        458,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth459: Glypth = Glypth::new(
        459,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, false, false, false, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth460: Glypth = Glypth::new(
        460,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth461: Glypth = Glypth::new(
        461,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth462: Glypth = Glypth::new(
        462,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, true, false, true, false, false],
            [false, true, false, true, true, false, false, false],
            [false, true, true, true, false, false, false, false],
            [false, true, true, true, false, false, false, false],
            [false, true, false, true, true, false, false, false],
            [false, true, false, true, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth463: Glypth = Glypth::new(
        463,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, true, false, true, false, false],
            [false, true, false, true, true, false, false, false],
            [false, true, true, true, false, false, false, false],
            [false, true, false, true, true, false, false, false],
            [false, true, false, true, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth464: Glypth = Glypth::new(
        464,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, true, false, false, false, false],
            [false, true, true, false, false, false, false, false],
            [false, true, true, false, false, false, false, false],
            [false, true, false, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth465: Glypth = Glypth::new(
        465,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, true, true, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth466: Glypth = Glypth::new(
        466,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth467: Glypth = Glypth::new(
        467,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth468: Glypth = Glypth::new(
        468,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth469: Glypth = Glypth::new(
        469,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, true, false, true, false],
            [false, true, false, false, true, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, true, false, true, false],
            [false, false, false, false, true, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth470: Glypth = Glypth::new(
        470,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, true, false, true, false],
            [false, true, false, false, true, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, true, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth471: Glypth = Glypth::new(
        471,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth472: Glypth = Glypth::new(
        472,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth473: Glypth = Glypth::new(
        473,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth474: Glypth = Glypth::new(
        474,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth475: Glypth = Glypth::new(
        475,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth476: Glypth = Glypth::new(
        476,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, true, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth477: Glypth = Glypth::new(
        477,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth478: Glypth = Glypth::new(
        478,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth479: Glypth = Glypth::new(
        479,
        [
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, true, true, false],
            [false, true, false, false, true, false, true, false],
            [false, true, false, true, false, false, true, false],
            [false, true, true, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth480: Glypth = Glypth::new(
        480,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth481: Glypth = Glypth::new(
        481,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth482: Glypth = Glypth::new(
        482,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth483: Glypth = Glypth::new(
        483,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth484: Glypth = Glypth::new(
        484,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth485: Glypth = Glypth::new(
        485,
        [
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth486: Glypth = Glypth::new(
        486,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth487: Glypth = Glypth::new(
        487,
        [
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth488: Glypth = Glypth::new(
        488,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth489: Glypth = Glypth::new(
        489,
        [
            [false, false, false, true, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth490: Glypth = Glypth::new(
        490,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, true, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth491: Glypth = Glypth::new(
        491,
        [
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, true, true, true, false, false, true, false],
            [true, false, false, false, true, false, true, false],
            [true, false, false, false, true, false, true, false],
            [true, false, false, false, true, false, true, false],
            [true, false, false, false, true, false, true, false],
            [true, true, true, true, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth492: Glypth = Glypth::new(
        492,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, true, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, true, true, true, false, false, true, false],
            [true, false, false, false, true, false, true, false],
            [true, false, false, false, true, false, true, false],
            [true, false, false, false, true, false, true, false],
            [true, true, true, true, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth493: Glypth = Glypth::new(
        493,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, true, true, true, true, true, true, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, true, true, false, false],
            [false, true, true, true, false, false, false, false],
            [false, false, false, true, true, true, false, false],
            [false, true, true, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth494: Glypth = Glypth::new(
        494,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, true, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth495: Glypth = Glypth::new(
        495,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, true, true, true, false],
            [false, false, true, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth496: Glypth = Glypth::new(
        496,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, true, false, false],
            [false, true, true, true, true, true, false, false],
            [true, false, false, false, true, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, false, true, false, false, true, false],
            [true, false, true, false, false, false, true, false],
            [false, true, true, true, true, true, false, false],
            [false, true, false, false, false, false, false, false],
            [true, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth497: Glypth = Glypth::new(
        497,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth498: Glypth = Glypth::new(
        498,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
        ],
    );
    let glypth499: Glypth = Glypth::new(
        499,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth500: Glypth = Glypth::new(
        500,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, true, false, false, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth501: Glypth = Glypth::new(
        501,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, true, false],
            [false, false, true, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [true, true, true, true, true, true, true, false],
            [false, true, false, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth502: Glypth = Glypth::new(
        502,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [true, true, false, false, false, false, false, false],
            [false, true, true, true, true, true, true, false],
            [false, false, true, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth503: Glypth = Glypth::new(
        503,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, true, true, false, true, true, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth504: Glypth = Glypth::new(
        504,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, true, false, false, false],
            [true, true, true, true, true, true, false, false],
            [false, false, false, false, false, true, true, false],
            [true, true, true, true, true, true, false, false],
            [false, false, false, false, true, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth505: Glypth = Glypth::new(
        505,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, true, true, false, true, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth506: Glypth = Glypth::new(
        506,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, true, true, false, false, false],
            [false, true, true, false, true, true, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, true, true, false, true, true, false, false],
            [false, false, true, true, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth507: Glypth = Glypth::new(
        507,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth508: Glypth = Glypth::new(
        508,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth509: Glypth = Glypth::new(
        509,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [false, true, false, false, false, true, false, false],
            [false, true, false, false, false, true, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth510: Glypth = Glypth::new(
        510,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, true, false, false, false, false, true, false],
            [false, false, true, true, true, true, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );
    let glypth511: Glypth = Glypth::new(
        511,
        [
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, true, false, false, false, false],
            [false, false, true, false, true, false, false, false],
            [false, true, false, false, false, true, false, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, false, false, false, false, false, true, false],
            [true, true, true, true, true, true, true, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false],
        ],
    );

    let glypths: [Glypth; GLYPTH_COUNT] = [
        glypth0, glypth1, glypth2, glypth3, glypth4, glypth5, glypth6, glypth7, glypth8, glypth9,
        glypth10, glypth11, glypth12, glypth13, glypth14, glypth15, glypth16, glypth17, glypth18,
        glypth19, glypth20, glypth21, glypth22, glypth23, glypth24, glypth25, glypth26, glypth27,
        glypth28, glypth29, glypth30, glypth31, glypth32, glypth33, glypth34, glypth35, glypth36,
        glypth37, glypth38, glypth39, glypth40, glypth41, glypth42, glypth43, glypth44, glypth45,
        glypth46, glypth47, glypth48, glypth49, glypth50, glypth51, glypth52, glypth53, glypth54,
        glypth55, glypth56, glypth57, glypth58, glypth59, glypth60, glypth61, glypth62, glypth63,
        glypth64, glypth65, glypth66, glypth67, glypth68, glypth69, glypth70, glypth71, glypth72,
        glypth73, glypth74, glypth75, glypth76, glypth77, glypth78, glypth79, glypth80, glypth81,
        glypth82, glypth83, glypth84, glypth85, glypth86, glypth87, glypth88, glypth89, glypth90,
        glypth91, glypth92, glypth93, glypth94, glypth95, glypth96, glypth97, glypth98, glypth99,
        glypth100, glypth101, glypth102, glypth103, glypth104, glypth105, glypth106, glypth107,
        glypth108, glypth109, glypth110, glypth111, glypth112, glypth113, glypth114, glypth115,
        glypth116, glypth117, glypth118, glypth119, glypth120, glypth121, glypth122, glypth123,
        glypth124, glypth125, glypth126, glypth127, glypth128, glypth129, glypth130, glypth131,
        glypth132, glypth133, glypth134, glypth135, glypth136, glypth137, glypth138, glypth139,
        glypth140, glypth141, glypth142, glypth143, glypth144, glypth145, glypth146, glypth147,
        glypth148, glypth149, glypth150, glypth151, glypth152, glypth153, glypth154, glypth155,
        glypth156, glypth157, glypth158, glypth159, glypth160, glypth161, glypth162, glypth163,
        glypth164, glypth165, glypth166, glypth167, glypth168, glypth169, glypth170, glypth171,
        glypth172, glypth173, glypth174, glypth175, glypth176, glypth177, glypth178, glypth179,
        glypth180, glypth181, glypth182, glypth183, glypth184, glypth185, glypth186, glypth187,
        glypth188, glypth189, glypth190, glypth191, glypth192, glypth193, glypth194, glypth195,
        glypth196, glypth197, glypth198, glypth199, glypth200, glypth201, glypth202, glypth203,
        glypth204, glypth205, glypth206, glypth207, glypth208, glypth209, glypth210, glypth211,
        glypth212, glypth213, glypth214, glypth215, glypth216, glypth217, glypth218, glypth219,
        glypth220, glypth221, glypth222, glypth223, glypth224, glypth225, glypth226, glypth227,
        glypth228, glypth229, glypth230, glypth231, glypth232, glypth233, glypth234, glypth235,
        glypth236, glypth237, glypth238, glypth239, glypth240, glypth241, glypth242, glypth243,
        glypth244, glypth245, glypth246, glypth247, glypth248, glypth249, glypth250, glypth251,
        glypth252, glypth253, glypth254, glypth255, glypth256, glypth257, glypth258, glypth259,
        glypth260, glypth261, glypth262, glypth263, glypth264, glypth265, glypth266, glypth267,
        glypth268, glypth269, glypth270, glypth271, glypth272, glypth273, glypth274, glypth275,
        glypth276, glypth277, glypth278, glypth279, glypth280, glypth281, glypth282, glypth283,
        glypth284, glypth285, glypth286, glypth287, glypth288, glypth289, glypth290, glypth291,
        glypth292, glypth293, glypth294, glypth295, glypth296, glypth297, glypth298, glypth299,
        glypth300, glypth301, glypth302, glypth303, glypth304, glypth305, glypth306, glypth307,
        glypth308, glypth309, glypth310, glypth311, glypth312, glypth313, glypth314, glypth315,
        glypth316, glypth317, glypth318, glypth319, glypth320, glypth321, glypth322, glypth323,
        glypth324, glypth325, glypth326, glypth327, glypth328, glypth329, glypth330, glypth331,
        glypth332, glypth333, glypth334, glypth335, glypth336, glypth337, glypth338, glypth339,
        glypth340, glypth341, glypth342, glypth343, glypth344, glypth345, glypth346, glypth347,
        glypth348, glypth349, glypth350, glypth351, glypth352, glypth353, glypth354, glypth355,
        glypth356, glypth357, glypth358, glypth359, glypth360, glypth361, glypth362, glypth363,
        glypth364, glypth365, glypth366, glypth367, glypth368, glypth369, glypth370, glypth371,
        glypth372, glypth373, glypth374, glypth375, glypth376, glypth377, glypth378, glypth379,
        glypth380, glypth381, glypth382, glypth383, glypth384, glypth385, glypth386, glypth387,
        glypth388, glypth389, glypth390, glypth391, glypth392, glypth393, glypth394, glypth395,
        glypth396, glypth397, glypth398, glypth399, glypth400, glypth401, glypth402, glypth403,
        glypth404, glypth405, glypth406, glypth407, glypth408, glypth409, glypth410, glypth411,
        glypth412, glypth413, glypth414, glypth415, glypth416, glypth417, glypth418, glypth419,
        glypth420, glypth421, glypth422, glypth423, glypth424, glypth425, glypth426, glypth427,
        glypth428, glypth429, glypth430, glypth431, glypth432, glypth433, glypth434, glypth435,
        glypth436, glypth437, glypth438, glypth439, glypth440, glypth441, glypth442, glypth443,
        glypth444, glypth445, glypth446, glypth447, glypth448, glypth449, glypth450, glypth451,
        glypth452, glypth453, glypth454, glypth455, glypth456, glypth457, glypth458, glypth459,
        glypth460, glypth461, glypth462, glypth463, glypth464, glypth465, glypth466, glypth467,
        glypth468, glypth469, glypth470, glypth471, glypth472, glypth473, glypth474, glypth475,
        glypth476, glypth477, glypth478, glypth479, glypth480, glypth481, glypth482, glypth483,
        glypth484, glypth485, glypth486, glypth487, glypth488, glypth489, glypth490, glypth491,
        glypth492, glypth493, glypth494, glypth495, glypth496, glypth497, glypth498, glypth499,
        glypth500, glypth501, glypth502, glypth503, glypth504, glypth505, glypth506, glypth507,
        glypth508, glypth509, glypth510, glypth511,
    ];

    glypths
};

lazy_static! {
    pub static ref UNI3_TERMINUS16: Font = {
        let mut unicode_map = HashMap::new();
        unicode_map.insert(169, 0);
        unicode_map.insert(174, 1);
        unicode_map.insert(216, 2);
        unicode_map.insert(8482, 3);
        unicode_map.insert(9830, 4);
        unicode_map.insert(9672, 4);
        unicode_map.insert(65533, 4);
        unicode_map.insert(260, 5);
        unicode_map.insert(261, 6);
        unicode_map.insert(8226, 7);
        unicode_map.insert(9679, 7);
        unicode_map.insert(273, 8);
        unicode_map.insert(280, 9);
        unicode_map.insert(294, 10);
        unicode_map.insert(371, 11);
        unicode_map.insert(1062, 12);
        unicode_map.insert(1065, 13);
        unicode_map.insert(1094, 14);
        unicode_map.insert(1097, 15);
        unicode_map.insert(1174, 16);
        unicode_map.insert(1175, 17);
        unicode_map.insert(1178, 18);
        unicode_map.insert(1179, 19);
        unicode_map.insert(182, 20);
        unicode_map.insert(167, 21);
        unicode_map.insert(1186, 22);
        unicode_map.insert(1187, 23);
        unicode_map.insert(8593, 24);
        unicode_map.insert(9650, 24);
        unicode_map.insert(9652, 24);
        unicode_map.insert(8595, 25);
        unicode_map.insert(9660, 25);
        unicode_map.insert(9662, 25);
        unicode_map.insert(8594, 26);
        unicode_map.insert(9654, 26);
        unicode_map.insert(9656, 26);
        unicode_map.insert(8592, 27);
        unicode_map.insert(9664, 27);
        unicode_map.insert(9666, 27);
        unicode_map.insert(1188, 28);
        unicode_map.insert(1189, 29);
        unicode_map.insert(1202, 30);
        unicode_map.insert(1203, 31);
        unicode_map.insert(32, 32);
        unicode_map.insert(160, 32);
        unicode_map.insert(8192, 32);
        unicode_map.insert(8193, 32);
        unicode_map.insert(8194, 32);
        unicode_map.insert(8195, 32);
        unicode_map.insert(8196, 32);
        unicode_map.insert(8197, 32);
        unicode_map.insert(8198, 32);
        unicode_map.insert(8199, 32);
        unicode_map.insert(8200, 32);
        unicode_map.insert(8201, 32);
        unicode_map.insert(8202, 32);
        unicode_map.insert(8239, 32);
        unicode_map.insert(33, 33);
        unicode_map.insert(34, 34);
        unicode_map.insert(35, 35);
        unicode_map.insert(36, 36);
        unicode_map.insert(37, 37);
        unicode_map.insert(38, 38);
        unicode_map.insert(39, 39);
        unicode_map.insert(40, 40);
        unicode_map.insert(41, 41);
        unicode_map.insert(42, 42);
        unicode_map.insert(8859, 42);
        unicode_map.insert(43, 43);
        unicode_map.insert(8853, 43);
        unicode_map.insert(44, 44);
        unicode_map.insert(45, 45);
        unicode_map.insert(8210, 45);
        unicode_map.insert(8211, 45);
        unicode_map.insert(8722, 45);
        unicode_map.insert(173, 45);
        unicode_map.insert(8208, 45);
        unicode_map.insert(8209, 45);
        unicode_map.insert(8854, 45);
        unicode_map.insert(46, 46);
        unicode_map.insert(8857, 46);
        unicode_map.insert(9744, 46);
        unicode_map.insert(47, 47);
        unicode_map.insert(8856, 47);
        unicode_map.insert(9585, 47);
        unicode_map.insert(48, 48);
        unicode_map.insert(9450, 48);
        unicode_map.insert(49, 49);
        unicode_map.insert(9312, 49);
        unicode_map.insert(50, 50);
        unicode_map.insert(9313, 50);
        unicode_map.insert(51, 51);
        unicode_map.insert(9314, 51);
        unicode_map.insert(52, 52);
        unicode_map.insert(9315, 52);
        unicode_map.insert(53, 53);
        unicode_map.insert(9316, 53);
        unicode_map.insert(54, 54);
        unicode_map.insert(9317, 54);
        unicode_map.insert(55, 55);
        unicode_map.insert(9318, 55);
        unicode_map.insert(56, 56);
        unicode_map.insert(9319, 56);
        unicode_map.insert(57, 57);
        unicode_map.insert(9320, 57);
        unicode_map.insert(58, 58);
        unicode_map.insert(59, 59);
        unicode_map.insert(60, 60);
        unicode_map.insert(61, 61);
        unicode_map.insert(8860, 61);
        unicode_map.insert(62, 62);
        unicode_map.insert(63, 63);
        unicode_map.insert(64, 64);
        unicode_map.insert(65, 65);
        unicode_map.insert(1040, 65);
        unicode_map.insert(913, 65);
        unicode_map.insert(9398, 65);
        unicode_map.insert(66, 66);
        unicode_map.insert(1042, 66);
        unicode_map.insert(914, 66);
        unicode_map.insert(9399, 66);
        unicode_map.insert(67, 67);
        unicode_map.insert(1057, 67);
        unicode_map.insert(9400, 67);
        unicode_map.insert(68, 68);
        unicode_map.insert(9401, 68);
        unicode_map.insert(69, 69);
        unicode_map.insert(1045, 69);
        unicode_map.insert(917, 69);
        unicode_map.insert(9402, 69);
        unicode_map.insert(70, 70);
        unicode_map.insert(9403, 70);
        unicode_map.insert(71, 71);
        unicode_map.insert(9404, 71);
        unicode_map.insert(72, 72);
        unicode_map.insert(1053, 72);
        unicode_map.insert(919, 72);
        unicode_map.insert(9405, 72);
        unicode_map.insert(73, 73);
        unicode_map.insert(1030, 73);
        unicode_map.insert(921, 73);
        unicode_map.insert(9406, 73);
        unicode_map.insert(74, 74);
        unicode_map.insert(1032, 74);
        unicode_map.insert(9407, 74);
        unicode_map.insert(75, 75);
        unicode_map.insert(1050, 75);
        unicode_map.insert(922, 75);
        unicode_map.insert(8490, 75);
        unicode_map.insert(9408, 75);
        unicode_map.insert(76, 76);
        unicode_map.insert(9409, 76);
        unicode_map.insert(77, 77);
        unicode_map.insert(1052, 77);
        unicode_map.insert(924, 77);
        unicode_map.insert(9410, 77);
        unicode_map.insert(78, 78);
        unicode_map.insert(925, 78);
        unicode_map.insert(9411, 78);
        unicode_map.insert(79, 79);
        unicode_map.insert(1054, 79);
        unicode_map.insert(927, 79);
        unicode_map.insert(9412, 79);
        unicode_map.insert(80, 80);
        unicode_map.insert(1056, 80);
        unicode_map.insert(929, 80);
        unicode_map.insert(9413, 80);
        unicode_map.insert(81, 81);
        unicode_map.insert(9414, 81);
        unicode_map.insert(82, 82);
        unicode_map.insert(9415, 82);
        unicode_map.insert(83, 83);
        unicode_map.insert(1029, 83);
        unicode_map.insert(9416, 83);
        unicode_map.insert(84, 84);
        unicode_map.insert(1058, 84);
        unicode_map.insert(932, 84);
        unicode_map.insert(9417, 84);
        unicode_map.insert(85, 85);
        unicode_map.insert(9418, 85);
        unicode_map.insert(86, 86);
        unicode_map.insert(9419, 86);
        unicode_map.insert(87, 87);
        unicode_map.insert(9420, 87);
        unicode_map.insert(88, 88);
        unicode_map.insert(1061, 88);
        unicode_map.insert(935, 88);
        unicode_map.insert(9421, 88);
        unicode_map.insert(9587, 88);
        unicode_map.insert(89, 89);
        unicode_map.insert(1198, 89);
        unicode_map.insert(9422, 89);
        unicode_map.insert(90, 90);
        unicode_map.insert(918, 90);
        unicode_map.insert(9423, 90);
        unicode_map.insert(91, 91);
        unicode_map.insert(92, 92);
        unicode_map.insert(9586, 92);
        unicode_map.insert(93, 93);
        unicode_map.insert(94, 94);
        unicode_map.insert(95, 95);
        unicode_map.insert(96, 96);
        unicode_map.insert(97, 97);
        unicode_map.insert(1072, 97);
        unicode_map.insert(9424, 97);
        unicode_map.insert(98, 98);
        unicode_map.insert(9425, 98);
        unicode_map.insert(99, 99);
        unicode_map.insert(1089, 99);
        unicode_map.insert(9426, 99);
        unicode_map.insert(100, 100);
        unicode_map.insert(9427, 100);
        unicode_map.insert(101, 101);
        unicode_map.insert(1077, 101);
        unicode_map.insert(9428, 101);
        unicode_map.insert(102, 102);
        unicode_map.insert(9429, 102);
        unicode_map.insert(103, 103);
        unicode_map.insert(9430, 103);
        unicode_map.insert(104, 104);
        unicode_map.insert(9431, 104);
        unicode_map.insert(105, 105);
        unicode_map.insert(1110, 105);
        unicode_map.insert(9432, 105);
        unicode_map.insert(106, 106);
        unicode_map.insert(1112, 106);
        unicode_map.insert(9433, 106);
        unicode_map.insert(107, 107);
        unicode_map.insert(9434, 107);
        unicode_map.insert(108, 108);
        unicode_map.insert(9435, 108);
        unicode_map.insert(109, 109);
        unicode_map.insert(9436, 109);
        unicode_map.insert(110, 110);
        unicode_map.insert(9437, 110);
        unicode_map.insert(111, 111);
        unicode_map.insert(1086, 111);
        unicode_map.insert(9438, 111);
        unicode_map.insert(112, 112);
        unicode_map.insert(1088, 112);
        unicode_map.insert(9439, 112);
        unicode_map.insert(113, 113);
        unicode_map.insert(9440, 113);
        unicode_map.insert(114, 114);
        unicode_map.insert(9441, 114);
        unicode_map.insert(115, 115);
        unicode_map.insert(1109, 115);
        unicode_map.insert(115, 115);
        unicode_map.insert(9442, 115);
        unicode_map.insert(116, 116);
        unicode_map.insert(9443, 116);
        unicode_map.insert(117, 117);
        unicode_map.insert(9444, 117);
        unicode_map.insert(118, 118);
        unicode_map.insert(9445, 118);
        unicode_map.insert(9745, 118);
        unicode_map.insert(119, 119);
        unicode_map.insert(9446, 119);
        unicode_map.insert(120, 120);
        unicode_map.insert(1093, 120);
        unicode_map.insert(9447, 120);
        unicode_map.insert(9746, 120);
        unicode_map.insert(10007, 120);
        unicode_map.insert(10008, 120);
        unicode_map.insert(11197, 120);
        unicode_map.insert(121, 121);
        unicode_map.insert(1091, 121);
        unicode_map.insert(9448, 121);
        unicode_map.insert(122, 122);
        unicode_map.insert(9449, 122);
        unicode_map.insert(123, 123);
        unicode_map.insert(124, 124);
        unicode_map.insert(125, 125);
        unicode_map.insert(126, 126);
        unicode_map.insert(1206, 127);
        unicode_map.insert(199, 128);
        unicode_map.insert(252, 129);
        unicode_map.insert(233, 130);
        unicode_map.insert(226, 131);
        unicode_map.insert(228, 132);
        unicode_map.insert(224, 133);
        unicode_map.insert(229, 134);
        unicode_map.insert(231, 135);
        unicode_map.insert(234, 136);
        unicode_map.insert(235, 137);
        unicode_map.insert(1105, 137);
        unicode_map.insert(232, 138);
        unicode_map.insert(239, 139);
        unicode_map.insert(1111, 139);
        unicode_map.insert(238, 140);
        unicode_map.insert(236, 141);
        unicode_map.insert(196, 142);
        unicode_map.insert(197, 143);
        unicode_map.insert(8491, 143);
        unicode_map.insert(201, 144);
        unicode_map.insert(230, 145);
        unicode_map.insert(198, 146);
        unicode_map.insert(244, 147);
        unicode_map.insert(246, 148);
        unicode_map.insert(242, 149);
        unicode_map.insert(251, 150);
        unicode_map.insert(249, 151);
        unicode_map.insert(255, 152);
        unicode_map.insert(214, 153);
        unicode_map.insert(220, 154);
        unicode_map.insert(162, 155);
        unicode_map.insert(163, 156);
        unicode_map.insert(165, 157);
        unicode_map.insert(1207, 158);
        unicode_map.insert(402, 159);
        unicode_map.insert(225, 160);
        unicode_map.insert(237, 161);
        unicode_map.insert(243, 162);
        unicode_map.insert(250, 163);
        unicode_map.insert(241, 164);
        unicode_map.insert(209, 165);
        unicode_map.insert(170, 166);
        unicode_map.insert(186, 167);
        unicode_map.insert(191, 168);
        unicode_map.insert(8359, 169);
        unicode_map.insert(172, 170);
        unicode_map.insert(189, 171);
        unicode_map.insert(188, 172);
        unicode_map.insert(161, 173);
        unicode_map.insert(171, 174);
        unicode_map.insert(8810, 174);
        unicode_map.insert(187, 175);
        unicode_map.insert(8811, 175);
        unicode_map.insert(9617, 176);
        unicode_map.insert(9618, 177);
        unicode_map.insert(8252, 178);
        unicode_map.insert(9474, 179);
        unicode_map.insert(9475, 179);
        unicode_map.insert(9599, 179);
        unicode_map.insert(9597, 179);
        unicode_map.insert(9595, 179);
        unicode_map.insert(9591, 179);
        unicode_map.insert(9593, 179);
        unicode_map.insert(9589, 179);
        unicode_map.insert(9550, 179);
        unicode_map.insert(9551, 179);
        unicode_map.insert(9478, 179);
        unicode_map.insert(9479, 179);
        unicode_map.insert(9482, 179);
        unicode_map.insert(9483, 179);
        unicode_map.insert(9553, 179);
        unicode_map.insert(9508, 180);
        unicode_map.insert(9515, 180);
        unicode_map.insert(9514, 180);
        unicode_map.insert(9513, 180);
        unicode_map.insert(9512, 180);
        unicode_map.insert(9511, 180);
        unicode_map.insert(9510, 180);
        unicode_map.insert(9509, 180);
        unicode_map.insert(9569, 180);
        unicode_map.insert(9570, 180);
        unicode_map.insert(9571, 180);
        unicode_map.insert(8976, 181);
        unicode_map.insert(8596, 182);
        unicode_map.insert(8597, 183);
        unicode_map.insert(8616, 184);
        unicode_map.insert(8660, 185);
        unicode_map.insert(8735, 186);
        unicode_map.insert(9786, 187);
        unicode_map.insert(9787, 188);
        unicode_map.insert(9788, 189);
        unicode_map.insert(9792, 190);
        unicode_map.insert(9488, 191);
        unicode_map.insert(9491, 191);
        unicode_map.insert(9490, 191);
        unicode_map.insert(9489, 191);
        unicode_map.insert(9557, 191);
        unicode_map.insert(9558, 191);
        unicode_map.insert(9559, 191);
        unicode_map.insert(9582, 191);
        unicode_map.insert(9492, 192);
        unicode_map.insert(9495, 192);
        unicode_map.insert(9494, 192);
        unicode_map.insert(9493, 192);
        unicode_map.insert(9560, 192);
        unicode_map.insert(9561, 192);
        unicode_map.insert(9562, 192);
        unicode_map.insert(9584, 192);
        unicode_map.insert(9524, 193);
        unicode_map.insert(9531, 193);
        unicode_map.insert(9530, 193);
        unicode_map.insert(9529, 193);
        unicode_map.insert(9528, 193);
        unicode_map.insert(9527, 193);
        unicode_map.insert(9526, 193);
        unicode_map.insert(9525, 193);
        unicode_map.insert(9575, 193);
        unicode_map.insert(9576, 193);
        unicode_map.insert(9577, 193);
        unicode_map.insert(9516, 194);
        unicode_map.insert(9523, 194);
        unicode_map.insert(9522, 194);
        unicode_map.insert(9521, 194);
        unicode_map.insert(9520, 194);
        unicode_map.insert(9519, 194);
        unicode_map.insert(9518, 194);
        unicode_map.insert(9517, 194);
        unicode_map.insert(9572, 194);
        unicode_map.insert(9573, 194);
        unicode_map.insert(9574, 194);
        unicode_map.insert(9500, 195);
        unicode_map.insert(9507, 195);
        unicode_map.insert(9506, 195);
        unicode_map.insert(9505, 195);
        unicode_map.insert(9504, 195);
        unicode_map.insert(9503, 195);
        unicode_map.insert(9502, 195);
        unicode_map.insert(9501, 195);
        unicode_map.insert(9566, 195);
        unicode_map.insert(9567, 195);
        unicode_map.insert(9568, 195);
        unicode_map.insert(9472, 196);
        unicode_map.insert(9473, 196);
        unicode_map.insert(9598, 196);
        unicode_map.insert(9596, 196);
        unicode_map.insert(9594, 196);
        unicode_map.insert(9590, 196);
        unicode_map.insert(9592, 196);
        unicode_map.insert(9588, 196);
        unicode_map.insert(9476, 196);
        unicode_map.insert(9477, 196);
        unicode_map.insert(9480, 196);
        unicode_map.insert(9481, 196);
        unicode_map.insert(9548, 196);
        unicode_map.insert(9549, 196);
        unicode_map.insert(9552, 196);
        unicode_map.insert(9532, 197);
        unicode_map.insert(9547, 197);
        unicode_map.insert(9546, 197);
        unicode_map.insert(9545, 197);
        unicode_map.insert(9544, 197);
        unicode_map.insert(9543, 197);
        unicode_map.insert(9542, 197);
        unicode_map.insert(9541, 197);
        unicode_map.insert(9540, 197);
        unicode_map.insert(9539, 197);
        unicode_map.insert(9538, 197);
        unicode_map.insert(9537, 197);
        unicode_map.insert(9536, 197);
        unicode_map.insert(9535, 197);
        unicode_map.insert(9534, 197);
        unicode_map.insert(9533, 197);
        unicode_map.insert(9578, 197);
        unicode_map.insert(9579, 197);
        unicode_map.insert(9580, 197);
        unicode_map.insert(9794, 198);
        unicode_map.insert(9824, 199);
        unicode_map.insert(9827, 200);
        unicode_map.insert(164, 201);
        unicode_map.insert(166, 202);
        unicode_map.insert(168, 203);
        unicode_map.insert(175, 204);
        unicode_map.insert(713, 204);
        unicode_map.insert(179, 205);
        unicode_map.insert(180, 206);
        unicode_map.insert(184, 207);
        unicode_map.insert(185, 208);
        unicode_map.insert(190, 209);
        unicode_map.insert(192, 210);
        unicode_map.insert(193, 211);
        unicode_map.insert(194, 212);
        unicode_map.insert(195, 213);
        unicode_map.insert(200, 214);
        unicode_map.insert(202, 215);
        unicode_map.insert(203, 216);
        unicode_map.insert(1025, 216);
        unicode_map.insert(9496, 217);
        unicode_map.insert(9499, 217);
        unicode_map.insert(9498, 217);
        unicode_map.insert(9497, 217);
        unicode_map.insert(9563, 217);
        unicode_map.insert(9564, 217);
        unicode_map.insert(9565, 217);
        unicode_map.insert(9583, 217);
        unicode_map.insert(9484, 218);
        unicode_map.insert(9487, 218);
        unicode_map.insert(9486, 218);
        unicode_map.insert(9485, 218);
        unicode_map.insert(9554, 218);
        unicode_map.insert(9555, 218);
        unicode_map.insert(9556, 218);
        unicode_map.insert(9581, 218);
        unicode_map.insert(9608, 219);
        unicode_map.insert(204, 220);
        unicode_map.insert(205, 221);
        unicode_map.insert(206, 222);
        unicode_map.insert(207, 223);
        unicode_map.insert(1031, 223);
        unicode_map.insert(208, 224);
        unicode_map.insert(272, 224);
        unicode_map.insert(223, 225);
        unicode_map.insert(915, 226);
        unicode_map.insert(1043, 226);
        unicode_map.insert(960, 227);
        unicode_map.insert(210, 228);
        unicode_map.insert(211, 229);
        unicode_map.insert(181, 230);
        unicode_map.insert(956, 230);
        unicode_map.insert(212, 231);
        unicode_map.insert(934, 232);
        unicode_map.insert(1060, 232);
        unicode_map.insert(213, 233);
        unicode_map.insert(937, 234);
        unicode_map.insert(8486, 234);
        unicode_map.insert(215, 235);
        unicode_map.insert(8734, 236);
        unicode_map.insert(217, 237);
        unicode_map.insert(218, 238);
        unicode_map.insert(8745, 239);
        unicode_map.insert(8801, 240);
        unicode_map.insert(177, 241);
        unicode_map.insert(8805, 242);
        unicode_map.insert(8804, 243);
        unicode_map.insert(8992, 244);
        unicode_map.insert(8993, 245);
        unicode_map.insert(247, 246);
        unicode_map.insert(8776, 247);
        unicode_map.insert(176, 248);
        unicode_map.insert(8729, 249);
        unicode_map.insert(183, 250);
        unicode_map.insert(8730, 251);
        unicode_map.insert(8319, 252);
        unicode_map.insert(178, 253);
        unicode_map.insert(9632, 254);
        unicode_map.insert(9646, 254);
        unicode_map.insert(219, 255);
        unicode_map.insert(221, 256);
        unicode_map.insert(222, 257);
        unicode_map.insert(227, 258);
        unicode_map.insert(240, 259);
        unicode_map.insert(245, 260);
        unicode_map.insert(248, 261);
        unicode_map.insert(253, 262);
        unicode_map.insert(254, 263);
        unicode_map.insert(286, 264);
        unicode_map.insert(287, 265);
        unicode_map.insert(304, 266);
        unicode_map.insert(305, 267);
        unicode_map.insert(338, 268);
        unicode_map.insert(339, 269);
        unicode_map.insert(350, 270);
        unicode_map.insert(351, 271);
        unicode_map.insert(352, 272);
        unicode_map.insert(353, 273);
        unicode_map.insert(376, 274);
        unicode_map.insert(381, 275);
        unicode_map.insert(382, 276);
        unicode_map.insert(710, 277);
        unicode_map.insert(732, 278);
        unicode_map.insert(8212, 279);
        unicode_map.insert(8213, 279);
        unicode_map.insert(8216, 280);
        unicode_map.insert(8217, 281);
        unicode_map.insert(8218, 282);
        unicode_map.insert(8220, 283);
        unicode_map.insert(8221, 284);
        unicode_map.insert(8222, 285);
        unicode_map.insert(8224, 286);
        unicode_map.insert(8225, 287);
        unicode_map.insert(8230, 288);
        unicode_map.insert(8240, 289);
        unicode_map.insert(8249, 290);
        unicode_map.insert(8250, 291);
        unicode_map.insert(8364, 292);
        unicode_map.insert(258, 293);
        unicode_map.insert(1232, 293);
        unicode_map.insert(259, 294);
        unicode_map.insert(1233, 294);
        unicode_map.insert(262, 295);
        unicode_map.insert(263, 296);
        unicode_map.insert(268, 297);
        unicode_map.insert(269, 298);
        unicode_map.insert(270, 299);
        unicode_map.insert(271, 300);
        unicode_map.insert(281, 301);
        unicode_map.insert(282, 302);
        unicode_map.insert(283, 303);
        unicode_map.insert(313, 304);
        unicode_map.insert(314, 305);
        unicode_map.insert(317, 306);
        unicode_map.insert(318, 307);
        unicode_map.insert(321, 308);
        unicode_map.insert(322, 309);
        unicode_map.insert(323, 310);
        unicode_map.insert(324, 311);
        unicode_map.insert(327, 312);
        unicode_map.insert(328, 313);
        unicode_map.insert(336, 314);
        unicode_map.insert(337, 315);
        unicode_map.insert(340, 316);
        unicode_map.insert(341, 317);
        unicode_map.insert(344, 318);
        unicode_map.insert(345, 319);
        unicode_map.insert(346, 320);
        unicode_map.insert(347, 321);
        unicode_map.insert(354, 322);
        unicode_map.insert(355, 323);
        unicode_map.insert(356, 324);
        unicode_map.insert(357, 325);
        unicode_map.insert(366, 326);
        unicode_map.insert(367, 327);
        unicode_map.insert(368, 328);
        unicode_map.insert(369, 329);
        unicode_map.insert(377, 330);
        unicode_map.insert(378, 331);
        unicode_map.insert(379, 332);
        unicode_map.insert(380, 333);
        unicode_map.insert(536, 334);
        unicode_map.insert(537, 335);
        unicode_map.insert(538, 336);
        unicode_map.insert(539, 337);
        unicode_map.insert(711, 338);
        unicode_map.insert(728, 339);
        unicode_map.insert(729, 340);
        unicode_map.insert(731, 341);
        unicode_map.insert(733, 342);
        unicode_map.insert(264, 343);
        unicode_map.insert(265, 344);
        unicode_map.insert(266, 345);
        unicode_map.insert(267, 346);
        unicode_map.insert(284, 347);
        unicode_map.insert(285, 348);
        unicode_map.insert(288, 349);
        unicode_map.insert(289, 350);
        unicode_map.insert(292, 351);
        unicode_map.insert(293, 352);
        unicode_map.insert(295, 353);
        unicode_map.insert(308, 354);
        unicode_map.insert(309, 355);
        unicode_map.insert(348, 356);
        unicode_map.insert(349, 357);
        unicode_map.insert(364, 358);
        unicode_map.insert(365, 359);
        unicode_map.insert(372, 360);
        unicode_map.insert(373, 361);
        unicode_map.insert(374, 362);
        unicode_map.insert(375, 363);
        unicode_map.insert(7744, 364);
        unicode_map.insert(7745, 365);
        unicode_map.insert(256, 366);
        unicode_map.insert(257, 367);
        unicode_map.insert(274, 368);
        unicode_map.insert(275, 369);
        unicode_map.insert(278, 370);
        unicode_map.insert(279, 371);
        unicode_map.insert(290, 372);
        unicode_map.insert(291, 373);
        unicode_map.insert(298, 374);
        unicode_map.insert(299, 375);
        unicode_map.insert(302, 376);
        unicode_map.insert(303, 377);
        unicode_map.insert(310, 378);
        unicode_map.insert(311, 379);
        unicode_map.insert(315, 380);
        unicode_map.insert(316, 381);
        unicode_map.insert(325, 382);
        unicode_map.insert(326, 383);
        unicode_map.insert(332, 384);
        unicode_map.insert(333, 385);
        unicode_map.insert(342, 386);
        unicode_map.insert(343, 387);
        unicode_map.insert(362, 388);
        unicode_map.insert(363, 389);
        unicode_map.insert(370, 390);
        unicode_map.insert(1024, 391);
        unicode_map.insert(1026, 392);
        unicode_map.insert(1027, 393);
        unicode_map.insert(1028, 394);
        unicode_map.insert(1033, 395);
        unicode_map.insert(1034, 396);
        unicode_map.insert(1035, 397);
        unicode_map.insert(1036, 398);
        unicode_map.insert(1037, 399);
        unicode_map.insert(1038, 400);
        unicode_map.insert(1039, 401);
        unicode_map.insert(1041, 402);
        unicode_map.insert(1044, 403);
        unicode_map.insert(1046, 404);
        unicode_map.insert(1047, 405);
        unicode_map.insert(1048, 406);
        unicode_map.insert(1049, 407);
        unicode_map.insert(1051, 408);
        unicode_map.insert(928, 409);
        unicode_map.insert(1055, 409);
        unicode_map.insert(1059, 410);
        unicode_map.insert(1063, 411);
        unicode_map.insert(1064, 412);
        unicode_map.insert(1066, 413);
        unicode_map.insert(1067, 414);
        unicode_map.insert(1068, 415);
        unicode_map.insert(1069, 416);
        unicode_map.insert(1070, 417);
        unicode_map.insert(1071, 418);
        unicode_map.insert(1073, 419);
        unicode_map.insert(1074, 420);
        unicode_map.insert(1075, 421);
        unicode_map.insert(1076, 422);
        unicode_map.insert(1078, 423);
        unicode_map.insert(1079, 424);
        unicode_map.insert(1080, 425);
        unicode_map.insert(1081, 426);
        unicode_map.insert(1082, 427);
        unicode_map.insert(1083, 428);
        unicode_map.insert(1084, 429);
        unicode_map.insert(1085, 430);
        unicode_map.insert(1087, 431);
        unicode_map.insert(1090, 432);
        unicode_map.insert(1092, 433);
        unicode_map.insert(1095, 434);
        unicode_map.insert(1096, 435);
        unicode_map.insert(1098, 436);
        unicode_map.insert(1099, 437);
        unicode_map.insert(1100, 438);
        unicode_map.insert(1101, 439);
        unicode_map.insert(1102, 440);
        unicode_map.insert(1103, 441);
        unicode_map.insert(1104, 442);
        unicode_map.insert(1106, 443);
        unicode_map.insert(1107, 444);
        unicode_map.insert(1108, 445);
        unicode_map.insert(1113, 446);
        unicode_map.insert(1114, 447);
        unicode_map.insert(1115, 448);
        unicode_map.insert(1116, 449);
        unicode_map.insert(1117, 450);
        unicode_map.insert(1118, 451);
        unicode_map.insert(1119, 452);
        unicode_map.insert(1168, 453);
        unicode_map.insert(1169, 454);
        unicode_map.insert(8470, 455);
        unicode_map.insert(1170, 456);
        unicode_map.insert(1171, 457);
        unicode_map.insert(1172, 458);
        unicode_map.insert(1173, 459);
        unicode_map.insert(1176, 460);
        unicode_map.insert(1177, 461);
        unicode_map.insert(1180, 462);
        unicode_map.insert(1181, 463);
        unicode_map.insert(1184, 464);
        unicode_map.insert(1185, 465);
        unicode_map.insert(1194, 466);
        unicode_map.insert(1195, 467);
        unicode_map.insert(1199, 468);
        unicode_map.insert(1208, 469);
        unicode_map.insert(1209, 470);
        unicode_map.insert(1210, 471);
        unicode_map.insert(1211, 472);
        unicode_map.insert(1234, 473);
        unicode_map.insert(1235, 474);
        unicode_map.insert(1238, 475);
        unicode_map.insert(1239, 476);
        unicode_map.insert(1240, 477);
        unicode_map.insert(1241, 478);
        unicode_map.insert(1250, 479);
        unicode_map.insert(1251, 480);
        unicode_map.insert(1254, 481);
        unicode_map.insert(1255, 482);
        unicode_map.insert(1256, 483);
        unicode_map.insert(1257, 484);
        unicode_map.insert(1262, 485);
        unicode_map.insert(1263, 486);
        unicode_map.insert(1264, 487);
        unicode_map.insert(1265, 488);
        unicode_map.insert(1266, 489);
        unicode_map.insert(1267, 490);
        unicode_map.insert(1272, 491);
        unicode_map.insert(1273, 492);
        unicode_map.insert(8366, 493);
        unicode_map.insert(8800, 494);
        unicode_map.insert(8712, 495);
        unicode_map.insert(8709, 496);
        unicode_map.insert(8214, 497);
        unicode_map.insert(8215, 498);
        unicode_map.insert(8242, 499);
        unicode_map.insert(8243, 500);
        unicode_map.insert(8629, 501);
        unicode_map.insert(8656, 502);
        unicode_map.insert(8657, 503);
        unicode_map.insert(8658, 504);
        unicode_map.insert(8659, 505);
        unicode_map.insert(8661, 506);
        unicode_map.insert(8710, 507);
        unicode_map.insert(916, 507);
        unicode_map.insert(8743, 508);
        unicode_map.insert(8744, 509);
        unicode_map.insert(8746, 510);
        unicode_map.insert(8962, 511);

        Font::new("uni3_terminus16", WIDTH, HEIGHT, &GLYPTHS, 512, unicode_map)
    };
}
