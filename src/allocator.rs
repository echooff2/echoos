use alloc::alloc::{GlobalAlloc, Layout};
use fixed_size_block_paging::FixedSizeBlockPagingAllocator;
use core::ops::{Deref, DerefMut};
use core::ptr::null_mut;
use spin::Mutex;
use x86_64::{
    structures::paging::{
        mapper::MapToError, FrameAllocator, Mapper, Page, PageTableFlags, Size4KiB,
    },
    VirtAddr,
};

pub mod fixed_size_block;
pub mod fixed_size_block_paging;

pub const HEAP_START: usize = 0x_4444_4444_0000;
pub const HEAP_SIZE: usize = 25 * 1024 * 1024; // 25MiB

#[global_allocator]
pub static ALLOCATOR: Locked<FixedSizeBlockPagingAllocator> = Locked::new(FixedSizeBlockPagingAllocator::new());

/// this code must run before apic initialization
pub unsafe fn init_heap(
) -> Result<(), MapToError<Size4KiB>> {
    // let page_range = {
    //     let heap_start = VirtAddr::new(HEAP_START as u64);
    //     let heap_end = heap_start + HEAP_SIZE - 1u64;
    //     let heap_start_page = Page::containing_address(heap_start);
    //     let heap_end_page = Page::containing_address(heap_end);
    //     Page::range_inclusive(heap_start_page, heap_end_page)
    // };
    //
    // let mut frame_allocator_lock = FRAME_ALLOCATOR.lock();
    // let frame_allocator = frame_allocator_lock.as_mut().expect("frame allocator must be initialized before init heap");
    // let mut mapper_lock = PAGE_TABLE.lock();
    // let mapper = mapper_lock.as_mut().expect("PAGE_TABLE must be initialized before init heap");
    //
    // for page in page_range {
    //     let frame = frame_allocator
    //         .allocate_frame()
    //         .ok_or(MapToError::FrameAllocationFailed)?;
    //
    //     let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE;
    //
    //     unsafe {
    //         mapper.map_to(page, frame, flags, frame_allocator)?.flush();
    //     };
    // }
    //
    // unsafe {
    //     ALLOCATOR.lock().init(HEAP_START, HEAP_SIZE);
    // }
    //
    // initialization is not needed for FixedSizeBlockPagingAllocator - left the code in case I
    // need to go back to previous allocator

    Ok(())
}

pub struct Locked<T> {
    inner: Mutex<T>,
}

impl<T> Locked<T> {
    pub const fn new(inner: T) -> Self {
        Self {
            inner: Mutex::new(inner),
        }
    }
}

impl<T> Deref for Locked<T> {
    type Target = Mutex<T>;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T> DerefMut for Locked<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

pub struct Dummy;

unsafe impl GlobalAlloc for Dummy {
    unsafe fn alloc(&self, _layout: Layout) -> *mut u8 {
        null_mut()
    }

    unsafe fn dealloc(&self, _ptr: *mut u8, _layout: Layout) {
        panic!("dealloc should be never called");
    }
}
