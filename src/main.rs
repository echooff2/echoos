#![no_std]
#![no_main]
#![feature(custom_test_frameworks, asm_const, sync_unsafe_cell)]
#![test_runner(echoos::test_runner)]
#![deny(unsafe_op_in_unsafe_fn)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;

#[macro_use]
extern crate echoos;

use ::acpi::madt::Madt;
use ::acpi::sdt::Signature;
use ::acpi::InterruptModel;
// use bootloader::{entry_point, BootInfo};
use bootloader::{entry_point, BootInfo};
use echoos::rand::rdrand;
use echoos::xhci::trb::command_trb::NoOpCommandTRB;
use echoos::xhci::Xhci;
use core::arch::asm;
use core::fmt::write;
use core::panic::PanicInfo;
use core::ptr::{read_volatile, write_volatile};
use echoos::acpi;
use echoos::acpi::ACPI_TABLE;
use echoos::apic;
use echoos::apic::APIC;
use echoos::display::fonts::uni3_terminus16::UNI3_TERMINUS16;
use echoos::display::text_display;
use echoos::hlt_loop;
use echoos::hpet::Hpet;
use echoos::memory::{self, find_free_p3_pages_to_fit, find_free_page, FRAME_ALLOCATOR, PAGE_TABLE};
use echoos::pci::{BarSpace, BarType, Pci, PciDevice, PciDeviceHeader, PciHeaderType};
use x2apic::lapic::xapic_base;
use x86_64::structures::paging::{Page, PageSize};
use x86_64::structures::paging::PageTableFlags;
use x86_64::structures::paging::PhysFrame;
use x86_64::structures::paging::Size4KiB;
use x86_64::structures::paging::Mapper;
use x86_64::PhysAddr;
use x86_64::VirtAddr;


entry_point!(kernel_main);

fn kernel_main(boot_info: &'static mut BootInfo) -> ! {
    let phys_mem_offset = VirtAddr::new(
        boot_info
        .physical_memory_offset
        .into_option()
        .expect("couldn't get physical memory offset"),
    );

    echoos::init(phys_mem_offset);

    unsafe { memory::init(phys_mem_offset) };
    unsafe { memory::BootInfoFrameAllocator::init(&boot_info.memory_regions) };

    unsafe {
        echoos::allocator::init_heap().expect("heap initialization failed");
    }

    serial_println!("heap initialized");

    text_display::init(
        boot_info.framebuffer.as_mut().unwrap(),
        text_display::Color::WHITE.to_rgb_color(),
        text_display::Color::BLACK.to_rgb_color(),
        &UNI3_TERMINUS16,
    );

    serial_println!("text display initialized");



    // phys_mem_offset = ...
    // echoos::init()
    // mapper = ...
    // frame_allocator = ...
    // init_heap()
    // text_display::init()
    
    // here was text_display::init

    let cpuid = raw_cpuid::CpuId::new();

    if cpuid.get_feature_info().expect("cpuid doesn't have features info").has_x2apic() == false {
        println!("warning: x2apic not supported");
    }

    assert!(rdrand::check_support() == true, "rdrand instruction is not supported");

    text_display::clear();

    println!("phys_mem_offset: {:#X}", phys_mem_offset);

    acpi::init(
        phys_mem_offset,
        *boot_info
            .rsdp_addr
            .as_ref()
            .expect("System not supported, RSDP not found"),
    );

    println!("ACPI initialized");
    println!(
        "ACPI revision: {}",
        acpi::ACPI_TABLE.read().as_ref().unwrap().revision
    );

    let madt = unsafe {
        ACPI_TABLE
            .read()
            .as_ref()
            .unwrap()
            .get_sdt::<Madt>(Signature::MADT)
            .expect("couldn't get madt")
            .unwrap()
    };

    let apic = match madt
        .parse_interrupt_model()
        .expect("couldn't get interrupt_model")
        .0
    {
        InterruptModel::Apic(apic) => apic,
        _ => panic!("other interrupts models that apic are not supported"),
    };

    serial_println!("Hello serial!");

    println!("mem offset: {:#x}", phys_mem_offset.as_u64());
    // unsafe {
    //     println!("msr[0x1b] = {:#x}", x86_64::registers::model_specific::Msr::new(0x1b).read());
    //     // serial_println!("msr[0x1b] = {}", *((apic.local_apic_address + phys_mem_offset.as_u64()) as *const u8));
    //     println!("msr[0x1b] = {:#x}", (apic.local_apic_address + phys_mem_offset.as_u64()));
    //     // println!("msr[0x1b] = {:#x}", *((apic.local_apic_address + phys_mem_offset.as_u64()) as *const u8));
    //     println!("msr[0x1b] = {:#x}", apic.local_apic_address);
    //     println!("msr[0x1b] = {:#x}", *(apic.local_apic_address as *const u8));
    // }

    println!("xapic base: {:#x}", unsafe { x2apic::lapic::xapic_base() });
    println!("logged");
    serial_println!("serial logged");

    // let apic_base_read = unsafe { *((apic.local_apic_address + phys_mem_offset.as_u64()) as *const u8) };
    // println!("apic_base_read: {}", apic_base_read);

    // let's try map the xapic base to someting for test
    let xapic_base_page: Page<Size4KiB> =
        Page::from_start_address(VirtAddr::new(0x8000)).expect("xapic base page faulty");
    // let xapic_base_page_start = Page::containing_address(VirtAddr::new(0x8000));
    // let xapic_base_page_end = Page::containing_address(VirtAddr::new(0x9000));
    // let xapic_base_page = Page::range_inclusive(xapic_base_page_start, xapic_base_page_end);
    let xapic_base_frame = PhysFrame::containing_address(unsafe { PhysAddr::new(xapic_base()) });
    println!(
        "xapic base frame addr start: {:#x}",
        xapic_base_frame.start_address().as_u64()
    );
    println!(
        "xapic base frame addr end: {:#x}",
        xapic_base_frame.start_address().as_u64() + xapic_base_frame.size()
    );
    let xapic_base_flags =
        PageTableFlags::WRITABLE | PageTableFlags::PRESENT | PageTableFlags::NO_CACHE;

    unsafe {
        let mut frame_alloc_lock = FRAME_ALLOCATOR.lock();
        let frame_allocator = frame_alloc_lock.as_mut().unwrap();
        let mut mapper_lock = PAGE_TABLE.lock();
        let mapper = mapper_lock.as_mut().unwrap();
        mapper
            .map_to(
                xapic_base_page,
                xapic_base_frame,
                xapic_base_flags,
                frame_allocator,
            )
            .expect("couldn't map xapic base addr to vmemory")
            .flush()
    };

    println!("mapped page!");

    let hpet_info = acpi::get_hpet_table().expect("ACPI not initialized or hpet not supported");
    unsafe {
        Hpet::init(hpet_info).expect("hpet is required");
    };

    unsafe {
        apic::init(apic, phys_mem_offset);
    }

    // TODO check if pci is supported
    let mut pci = unsafe { Pci::new() };
    for bus in pci.busses() {
        println!("bus {} initialized", bus.bus_number());
    }

    let xhci_pci = pci.find_function_take(0xC, 0x3, 0x30).expect("xhci controller not found");
    let xhci_pci = xhci_pci.upgrade();
    match xhci_pci {
        PciDevice::GeneralDevice(pci) => {
            // TODO should this be here? probably not
            let xhci_bar = pci.bars[0].memory_map().expect("couldn't memory map bar's pages");
            println!("is aligned: {}", xhci_bar.is_aligned(Size4KiB::SIZE));
            let xhci = unsafe { Xhci::new(pci, xhci_bar)};
            let mut xhci = unsafe { xhci.reset(xhci_bar) };
            unsafe { xhci.init() };
            xhci.runtime_registers.irs[0].event_ring.dequeue_ptr.read();
            println!("is_empty: {}", xhci.runtime_registers.irs[0].event_ring.is_empty_cached());
            println!("ip: {}", xhci.runtime_registers.irs[0].ip());
            println!("xhci is enabled: {}", xhci.is_running());
            let command_ring = xhci.command_ring.as_mut().unwrap();
            command_ring.enqueue_trb(NoOpCommandTRB::new()).expect("couldn't enqueue trb");
            unsafe { xhci.doorbell_registers.ring(0, 0, 0) };
            // while true {
                xhci.runtime_registers.irs[0].event_ring.dequeue_ptr.read();
                println!("dequeue_ptr: {:?}", xhci.runtime_registers.irs[0].event_ring.dequeue_ptr);
                println!("is_empty: {}", xhci.runtime_registers.irs[0].event_ring.is_empty_cached());
                println!("ip: {}", xhci.runtime_registers.irs[0].ip());
                // test hch - hchalted, host system error HSE
                // hce
                // on real hardware i get not only hce but alsop hch and HSE
                // could that be because real hardware bothered sending TRB Error trb and tried
                // informing me of this using msix but the write onto msix address failed and hse
                // was asserted? REF: xhci docs page 201 Note: A Host System Error ...
                println!(
                    "hce: {}, hch: {}, hse: {}", 
                    xhci.ops_registers.usb_sts.hce(),
                    xhci.ops_registers.usb_sts.hch(),
                    xhci.ops_registers.usb_sts.hse(),
                );
            // }
            xhci.debug_print();

            // println!("xhci: {:?}", xhci);
        },
        _ => panic!("xhci should be general device")
    }

    // println!("{:?}", pci);
    
    // let mut root_pci = PciDeviceHeader::new(0, 0, 0).unwrap();
    // println!("is multi function: {}", root_pci.is_multi_function);
    // println!("header_type: {:?}", root_pci.header_type);
    // match root_pci.header_type {
    //     PciHeaderType::GeneralDevice => {
    //         let device = root_pci.into_general_device().expect("root_pci is checked to be GeneralDevice");
    //         println!(
    //             "subsystem_id: {:?}, interrupt_pin: {:?}",
    //             device.subsystem_id, device.interrupt_pin
    //         );
    //         for device_num in 0..32 {
    //             let device = PciDeviceHeader::new(0, device_num, 0);
    //
    //             if let Ok(mut device) = device {
    //                 if device.vendor_id != 0xffff {
    //                     // device exists
    //                     println!(
    //                         "device {} on bus 0 exists, class: {:X}, subclass: {:X}, prog_if: {:X} vendor: {}, device: {}, is multi func: {}, header_type: {:?}",
    //                         device_num,
    //                         device.class_code,
    //                         device.subclass,
    //                         device.prog_if,
    //                         device.vendor_id,
    //                         device.device_id,
    //                         device.is_multi_function,
    //                         device.header_type
    //                     );
    //
    //                     if device.class_code == 0x0C
    //                         && device.subclass == 0x03
    //                         && device.prog_if == 0x30
    //                     {
    //                         if let PciHeaderType::GeneralDevice = device.header_type {
    //                             let gen_device = device.into_general_device().expect("device is checked to be GeneralDevice");
    //                             // TODO disable both I/O and memory decode in the command byte
    //                             println!("usb bar0: {:?}", gen_device.bars[0]);
    //                             println!(
    //                                 "this bar uses memory space: {}",
    //                                 gen_device.bars[0].get_space() == BarSpace::MemoryMapped
    //                             );
    //                             let bar0_type = gen_device.bars[0].get_type();
    //                             // assert_ne!(bar0_type, 1, "pci bar address size of 16 bit is not supported");
    //                             println!("bar type: {:?} - {}", 
    //                                 bar0_type,
    //                                 if bar0_type == BarType::Bits64 { "64bit" } else { "32bit" }
    //                             );
    //
    //                             println!("xhci bar is prefetchable: {}", gen_device.bars[0].is_prefechable());
    //                             // assert_eq!(bar0_type, BarType::Bits32, "for now only 32bit bars are supported");
    //                             assert_eq!(gen_device.bars[0].get_space(), BarSpace::MemoryMapped, "this has to be to use memory instead of io");
    //                             println!("bar's type: {:?}", gen_device.bars[0].get_type());
    //                             let bar0_addr = (gen_device.bars[0].get_address()) as u64;
    //                             let bar0_phys_addr = PhysAddr::new(bar0_addr);
    //                             let bar0_frame: PhysFrame<Size4KiB> =
    //                                 PhysFrame::containing_address(bar0_phys_addr);
    //                             println!("bar0_addr: {:#X}", bar0_addr);
    //                             // let memory_mapped_bar = bar_virt_addr.clone();
    //                             // let memory_mapped_bar = VirtAddr::new(0xDEADBEEF00);
    //                             // let memory_mapped_bar = VirtAddr::new((phys_mem_offset.as_u64() - 0xffff) & (!0xff));
    //                             // let memory_mapped_bar =
    //                             //     VirtAddr::new(phys_mem_offset.as_u64() + bar0_addr);
    //                             // println!("mapped address: {:#X}", memory_mapped_bar.as_u64());
    //                             // let bar0_virt_addr: VirtAddr = VirtAddr::new(0x80000);
    //                             // let bar0_virt_addr: VirtAddr = todo!();
    //                             // 
    //                             // is page_offset guaranteed to be 0? TODO check docs to check that
    //                             // I believe it is 16 byte aligned? that is what osdev wiki says
    //                             // anyway, i think official docs says qword aligned so idk
    //                             let page_offset = bar0_frame.start_address().as_u64() - bar0_phys_addr.as_u64();
    //                             assert_eq!(page_offset, 0, "for now page_offset must be 0 TODO handle it better");
    //                             let bar0_page = find_free_page();
    //                             // TODO map these pages
    //                             let p2_pages = find_free_p3_pages_to_fit(gen_device.bars[0].size());
    //                             let bar0_virt_addr = p2_pages[0].start_address() + page_offset;
    //                             let bar0_page_flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE | PageTableFlags::NO_CACHE;
    //                             memory::map_p3s_to(&p2_pages, bar0_page_flags, bar0_phys_addr, gen_device.bars[0].size(), &mut mapper, &mut frame_allocator);
    //                             println!(
    //                                 "mapped whole bar region of size: {} to virtual memory virt address: {:#X}",
    //                                 gen_device.bars[0].size(),
    //                                 p2_pages[0].start_address().as_u64()
    //                             );
    //
    //                             // let bar0_virt_addr = bar0_page.start_address() + page_offset;
    //                             println!("test: {:#X}", unsafe {
    //                                 *phys_mem_offset.as_ptr::<u64>()
    //                             });
    //                             // why after this it is working? is it because i created some space
    //                             // on frame or maybe because without it you cannot read it even
    //                             // though i think it is mapped at phys_mem_offset. Is the data of
    //                             // *bar0 real?
    //
    //                             // let's see if bar0_addr is in MemoryRegions from bootloader
    //                             // it is not
    //                             println!("finding regions!");
    //                             for region in boot_info.memory_regions.iter() {
    //                                 if bar0_addr >= region.start && bar0_addr <= region.end {
    //                                     println!("found region!, {region:?}");
    //                                 }
    //                             }
    //                             println!("ended finding");
    //
    //                             // memory::create_test_bar0_mapping(bar_virt_addr.as_u64(), page, &mut mapper, &mut frame_allocator);
    //                             // let bar0_page_flags = PageTableFlags::PRESENT
    //                             //     | PageTableFlags::WRITABLE
    //                             //     | PageTableFlags::NO_CACHE;
    //                             //
    //                             // unsafe {
    //                             //     mapper.map_to(
    //                             //         bar0_page,
    //                             //         bar0_frame,
    //                             //         bar0_page_flags,
    //                             //         &mut frame_allocator,
    //                             //     )
    //                             // };
    //                             // let phys_frame_bar = unsafe { echoos::memory::translate_addr(bar_virt_addr, phys_mem_offset) };
    //                             // println!("bar's phys_frame: {:?}", phys_frame_bar);
    //                             // let phys_frame_bar2 = unsafe { echoos::memory::translate_addr(memory_mapped_bar, phys_mem_offset) };
    //                             // println!("bar's phys_frame2: {:?}", phys_frame_bar2);
    //
    //                             // let bar0 = (phys_mem_offset.as_u64() + (gen_device.bars[0] & 0xFFFFFFF0) as u64) as *mut u32;
    //                             // let test_bar0 = phys_mem_offset + ((gen_device.bars[0] & 0xFFFFFFF0) as u64);
    //                             let bar0: *mut u32 = bar0_virt_addr.as_mut_ptr();
    //                             let test_bar0 = bar0_virt_addr;
    //                             println!("test_bar's table: {:?}", test_bar0.p4_index());
    //
    //                             let mut xhci = unsafe { Xhci::new(bar0_virt_addr) };
    //                             unsafe { xhci.init() };
    //                             // unsafe {
    //                             //     let bar0_v = *(bar0.get_val());
    //                             //     println!("usb *bar: {:#X?}", bar0_v);
    //                             //     println!(
    //                             //         "some_data: cap: {:#X?}, hciv: {:#X?}",
    //                             //         bar0_v & 0xff,
    //                             //         (bar0_v >> 8) & 0xffff
    //                             //     );
    //                             //     let hcs_params: u32 = *((bar0 as usize + 4) as *mut u32);
    //                             //     println!("n_ports={}", hcs_params & 0b1111);
    //                             // }
    //                             // println!("after unsafe print");
    //                             
    //                             // assert!(true, "here is it");
    //                             // let bar_u32_val = unsafe { *bar0 };
    //                             // println!("hci_version: {:#X}", (bar_u32_val >> 16) as u16);
    //                             // println!("bar_u32_val = {}", bar_u32_val);
    //                             // let caplength = unsafe { *bar0 & 0xff };
    //                             // println!("cap_length = {caplength}");
    //                             // let hci_ptr: *const u16 = (bar0_virt_addr.as_u64() + 2) as *const u16;
    //                             // println!("hci version: {:#X}", unsafe { *hci_ptr });
    //                             // let hcs_params1: *const u32 = (bar0_virt_addr.as_u64() + 4) as *const u32;
    //                             // println!("hcs_params1: {}", unsafe { *hcs_params1 });
    //                             // println!("hcs_params1_ptr = {}", hcs_params1 as usize);
    //                             // let max_device_slots = (unsafe { *hcs_params1 }) & 0xff;
    //                             // println!("max device slots: {max_device_slots}");
    //                             // let max_ports = (unsafe { *hcs_params1 }) >> 24;
    //                             // println!("max ports: {}", max_ports);
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     }
    //     _ => {}
    // }

    println!(
        "I don't think this is working: apic enabled: {}",
        unsafe { x86_64::registers::model_specific::Msr::new(0x1b).read() >> 11 } & 1
    ); // this most likely doesn't work, when i comment out apic::init still return 1

    // let mut a = volatile::WriteOnly::new(2);

    // println!("waiting!");
    // for i in 0..902020_usize {
    //     a.write(2);
    // }

    unsafe {
        let timer_apic = (0x8000 + 0x00320) as *const u32;
        println!("timer_apic bits: {:#b}", *timer_apic);
    }

    unsafe {
        let to_print = APIC.lock().assume_init_ref().timer_current();
        println!("error flasgs: {:?}", to_print);
    }

    unsafe {
        x86_64::software_interrupt!(34);
    }

    // println!("FIRST LINE\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nd");
    println!("Hello world!");
    // without_interrupts(|| TEXT_DISPLAY.lock().as_mut().unwrap().clear());

    // fn stack_overflow() {
    //     stack_overflow();
    //     volatile::Volatile::new(0).read();
    // }
    // stack_overflow();

    println!("Should print");

    #[cfg(test)]
    test_main();

    hlt_loop();
}

#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    use core::fmt::Write;
    use x86_64::instructions::interrupts::without_interrupts;

    // when adding printing do not use println!(), instead use writeln!() and ignore result of it
    // so no panic happens in panic handler
    without_interrupts(|| {
        let mut text_display_lock = unsafe { text_display::get_text_display().lock() };
        let text_display = text_display_lock.as_mut();

        if let Some(text_display) = text_display {
            let _ = writeln!(text_display, "\nPANIC!: {}", info);
        }
    });

    hlt_loop();
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    echoos::test_panic_handler(info)
}

#[test_case]
fn display_test() {
    println!("test");
}
