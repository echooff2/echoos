use core::arch::x86_64::_rdrand64_step;

use lazy_static::lazy_static;
use raw_cpuid::CpuId;

use crate::{println, serial_println};

lazy_static!(
    pub static ref RDRAND: Rdrand = {
        match Rdrand::new() {
            Some(v) => v,
            None => {
                serial_println!("rdrand not supported");
                panic!("rdrand not supported");
            }
        }
    };
);

pub struct Rdrand(());

impl Rdrand {
    pub fn new() -> Option<Self> {
        match check_support() {
            true => Some(Rdrand(())),
            false => None,
        }
    }

    pub fn get_u64(&self) -> u64 {
        unsafe { get_u64() }
    }
}

// returns true if supported
pub fn check_support() -> bool {
    let cpuid = CpuId::new();
    let features = cpuid.get_feature_info();

    if let Some(features) = features {
        return features.has_rdrand();
    }

    false
}

/// SAFETY: make sure check_support returns true before use
pub unsafe fn get_u64() -> u64 {
    let mut val = 0_u64;

    while unsafe { _rdrand64_step(&mut val) } == 0 {}

    val
}
