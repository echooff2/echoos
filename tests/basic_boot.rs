#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(echoos::test_runner)]
#![reexport_test_harness_main = "test_main"]

use core::panic::PanicInfo;

#[no_mangle]
pub extern "C" fn _start() -> ! {
    test_main();

    unreachable!()
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    echoos::test_panic_handler(info)
}

#[test_case]
fn test_assert() {
    assert_eq!(1, 1);
}
